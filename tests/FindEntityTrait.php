<?php

namespace App\Tests;

use App\Entity\Coresponsable;
use App\Entity\Formulaire;
use App\Entity\Organisation;
use App\Entity\Service;
use App\Entity\Soustraitant;
use App\Entity\Typage;
use App\Entity\User;

trait FindEntityTrait
{
    public function getOneEntityBy(string $entityClass, array $criteria)
    {
        if (!$this->entityManager) {
            throw new \Exception('entityManager is not defined');
        }
        $repository = $this->entityManager->getRepository($entityClass);

        return $repository->findOneBy($criteria);
    }

    /**
     * @throws \Exception
     */
    public function getOneUserBy(array $criteria): ?User
    {
        return $this->getOneEntityBy(User::class, $criteria);
    }

    /**
     * @throws \Exception
     */
    public function getOneTypageBy(array $criteria): ?Typage
    {
        return $this->getOneEntityBy(Typage::class, $criteria);
    }

    /**
     * @throws \Exception
     */
    public function getOneServiceBy(array $criteria): ?Service
    {
        return $this->getOneEntityBy(Service::class, $criteria);
    }

    /**
     * @throws \Exception
     */
    public function getOneSoustraitantBy(array $criteria): ?Soustraitant
    {
        return $this->getOneEntityBy(Soustraitant::class, $criteria);
    }

    /**
     * @throws \Exception
     */
    public function getOneCoresponsableBy(array $criteria): ?Coresponsable
    {
        return $this->getOneEntityBy(Coresponsable::class, $criteria);
    }

    /**
     * @throws \Exception
     */
    public function getOneOrganisationBy(array $criteria): ?Organisation
    {
        return $this->getOneEntityBy(Organisation::class, $criteria);
    }

    /**
     * @throws \Exception
     */
    public function getOneFormulaireBy(array $criteria): ?Formulaire
    {
        return $this->getOneEntityBy(Formulaire::class, $criteria);
    }
}
