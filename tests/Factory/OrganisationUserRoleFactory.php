<?php

namespace App\Tests\Factory;

use App\Entity\OrganisationUserRole;
use App\Repository\OrganisationUserRoleRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<OrganisationUserRole>
 *
 * @method static OrganisationUserRole|Proxy createOne(array $attributes = [])
 * @method static OrganisationUserRole[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static OrganisationUserRole|Proxy find(object|array|mixed $criteria)
 * @method static OrganisationUserRole|Proxy findOrCreate(array $attributes)
 * @method static OrganisationUserRole|Proxy first(string $sortedField = 'id')
 * @method static OrganisationUserRole|Proxy last(string $sortedField = 'id')
 * @method static OrganisationUserRole|Proxy random(array $attributes = [])
 * @method static OrganisationUserRole|Proxy randomOrCreate(array $attributes = [])
 * @method static OrganisationUserRole[]|Proxy[] all()
 * @method static OrganisationUserRole[]|Proxy[] findBy(array $attributes)
 * @method static OrganisationUserRole[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static OrganisationUserRole[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static OrganisationUserRoleRepository|RepositoryProxy repository()
 * @method OrganisationUserRole|Proxy create(array|callable $attributes = [])
 */
final class OrganisationUserRoleFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(OrganisationUserRole $organisationUserRole): void {})
        ;
    }

    protected static function getClass(): string
    {
        return OrganisationUserRole::class;
    }
}
