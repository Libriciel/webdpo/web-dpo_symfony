<?php

namespace App\Tests\Factory;

use App\Entity\Coresponsable;
use App\Repository\CoresponsableRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Coresponsable>
 *
 * @method static Coresponsable|Proxy createOne(array $attributes = [])
 * @method static Coresponsable[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Coresponsable|Proxy find(object|array|mixed $criteria)
 * @method static Coresponsable|Proxy findOrCreate(array $attributes)
 * @method static Coresponsable|Proxy first(string $sortedField = 'id')
 * @method static Coresponsable|Proxy last(string $sortedField = 'id')
 * @method static Coresponsable|Proxy random(array $attributes = [])
 * @method static Coresponsable|Proxy randomOrCreate(array $attributes = [])
 * @method static Coresponsable[]|Proxy[] all()
 * @method static Coresponsable[]|Proxy[] findBy(array $attributes)
 * @method static Coresponsable[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Coresponsable[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static CoresponsableRepository|RepositoryProxy repository()
 * @method Coresponsable|Proxy create(array|callable $attributes = [])
 */
final class CoresponsableFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'raisonsociale' => self::faker()->text(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Coresponsable $coresponsable): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Coresponsable::class;
    }
}
