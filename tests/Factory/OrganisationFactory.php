<?php

namespace App\Tests\Factory;

use App\Entity\Organisation;
use App\Repository\OrganisationRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Organisation>
 *
 * @method static Organisation|Proxy createOne(array $attributes = [])
 * @method static Organisation[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Organisation|Proxy find(object|array|mixed $criteria)
 * @method static Organisation|Proxy findOrCreate(array $attributes)
 * @method static Organisation|Proxy first(string $sortedField = 'id')
 * @method static Organisation|Proxy last(string $sortedField = 'id')
 * @method static Organisation|Proxy random(array $attributes = [])
 * @method static Organisation|Proxy randomOrCreate(array $attributes = [])
 * @method static Organisation[]|Proxy[] all()
 * @method static Organisation[]|Proxy[] findBy(array $attributes)
 * @method static Organisation[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Organisation[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static OrganisationRepository|RepositoryProxy repository()
 * @method Organisation|Proxy create(array|callable $attributes = [])
 */
final class OrganisationFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'raisonsociale' => self::faker()->text(),
            'phone' => self::faker()->text(),
            'address' => self::faker()->text(),
            'email' => self::faker()->text(),
            'siret' => self::faker()->text(),
            'ape' => self::faker()->text(),
            'rgpd' => self::faker()->boolean(),
            'registrePrefix' => self::faker()->text(),
            'registreNumber' => self::faker()->randomNumber(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Organisation $organisation): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Organisation::class;
    }
}
