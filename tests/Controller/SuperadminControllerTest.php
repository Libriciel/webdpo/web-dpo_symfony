<?php

namespace App\Tests\Controller;

use App\DataFixtures\OrganisationFixtures;
use App\DataFixtures\RoleFixtures;
use App\DataFixtures\UserFixtures;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SuperadminControllerTest extends WebTestCase
{
    use FindEntityTrait;
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            OrganisationFixtures::class,
            UserFixtures::class,
            RoleFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testSuperadminIndex()
    {
        $this->loginAsSuperadminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/superadmin');
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Liste des superadmin de l\'application")');
        $this->assertCount(1, $item);
    }

    public function testSuperadminAdd()
    {
        $this->loginAsSuperadminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/superadmin/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajout d\'un superadmin")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user[username]'] = 'adminsuper';
        $form['user[civility]'] = 1;
        $form['user[firstName]'] = 'Admin';
        $form['user[lastName]'] = 'SUPER';
        $form['user[email]'] = 'admin.super@test.fr';
        $form['user[notification]'] = 1;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("user.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneUserBy(['username' => 'adminsuper']));
    }

    public function testSuperadminAddAndRemove()
    {
        $this->loginAsSuperadminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/superadmin/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajout d\'un superadmin")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user[username]'] = 'superadmindelete';
        $form['user[civility]'] = 1;
        $form['user[firstName]'] = 'Superadmin';
        $form['user[lastName]'] = 'DELETE';
        $form['user[email]'] = 'superadmin.delete@test.fr';
        $form['user[notification]'] = 1;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("user.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneUserBy(['username' => 'superadmindelete']));

        $superadminDelete = $this->getOneUserBy(['username' => 'superadmindelete']);

        $this->client->request(Request::METHOD_DELETE, '/superadmin/delete/' . $superadminDelete->getId());
        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Le superadmin a bien été supprimé")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneUserBy(['id' => $superadminDelete->getId()]));
    }

    public function testSuperadminEdit()
    {
        $this->loginAsSuperadminLibriciel();

        $user = $this->getOneUserBy(['username' => 'superadmin1']);

        $crawler = $this->client->request(Request::METHOD_GET, '/superadmin/edit/' . $user->getId());
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modification d\'un superadmin")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user[civility]'] = 'Mme.';
        $form['user[firstName]'] = 'Jeanne';
        $form['user[lastName]'] = 'POTO';
        $form['user[notification]'] = 0;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("user.success_message.save")');
        $this->assertCount(1, $successMsg);

        $userAfterEdit = $user;
        $this->entityManager->refresh($userAfterEdit);
        $this->assertNotEmpty($userAfterEdit);
    }

    public function testSuperadminDeleteMyself()
    {
        $this->loginAsSuperadminLibriciel();

        $user = $this->getOneUserBy(['username' => 'superadmin']);
        $this->client->request(Request::METHOD_DELETE, '/superadmin/delete/' . $user->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Impossible de supprimer son propre utilisateur")');
        $this->assertCount(1, $successMsg);
    }
}
