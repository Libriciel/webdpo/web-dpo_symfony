<?php

namespace App\Tests\Controller;

use App\DataFixtures\OrganisationFixtures;
use App\DataFixtures\RoleFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class UserControllerTest extends WebTestCase
{
    use FindEntityTrait;
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            OrganisationFixtures::class,
            UserFixtures::class,
            RoleFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testIndex()
    {
        $this->loginAsAdminLibriciel();
        $crawler = $this->client->request(Request::METHOD_GET, '/user');
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Liste des utilisateurs de l\'application")');
        $this->assertCount(1, $item);
    }

    public function testAdd()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/user/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajouter un utilisateur")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user[username]'] = 'g.bleu';
        $form['user[civility]'] = 1;
        $form['user[firstName]'] = 'Gaston';
        $form['user[lastName]'] = 'BLEU';
        $form['user[email]'] = 'gaston.bleu@test.fr';
        $form['user[notification]'] = 1;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("user.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneUserBy(['username' => 'g.bleu']));
    }

    public function testEdit()
    {
        $this->loginAsAdminLibriciel();

        $user = $this->getOneUserBy(['username' => 'admin1']);

        $crawler = $this->client->request(Request::METHOD_GET, '/user/edit/' . $user->getId());
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modifier un utilisateur")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user[civility]'] = 'M.';
        $form['user[firstName]'] = 'Jean-Paul';
        $form['user[lastName]'] = 'LUNE';
        $form['user[notification]'] = 0;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("user.success_message.save")');
        $this->assertCount(1, $successMsg);

        $userAfterEdit = $user;
        $this->entityManager->refresh($userAfterEdit);
        $this->assertNotEmpty($userAfterEdit);
    }

    public function testShow()
    {
        $this->loginAsAdminLibriciel();

        $user = $this->getOneUserBy(['username' => 'admin1']);

        $crawler = $this->client->request(Request::METHOD_GET, '/user/show/' . $user->getId());
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Visualisation d\'un utilisateur")');
        $this->assertCount(1, $item);
    }


    public function testToggle()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user = $this->getOneUserBy(['username' => 'admin1']);
        $this->client->request(Request::METHOD_POST, '/user/toggle', ['toggle' => [$user->getId()]]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Les utilisateurs ont bien été modifiés")');
        $this->assertCount(1, $successMsg);
    }

    public function testToggleMultiple()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user1 = $this->getOneUserBy(['username' => 'admin1']);
        $user2 = $this->getOneUserBy(['username' => 'admin2']);
        $user3 = $this->getOneUserBy(['username' => 'admin3']);
        $this->client->request(Request::METHOD_POST, '/user/toggle', [
            'toggle' => [
                $user1->getId(),
                $user2->getId(),
                $user3->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Les utilisateurs ont bien été modifiés")');
        $this->assertCount(1, $successMsg);
    }

    public function testToggleMyself()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user = $this->getOneUserBy(['username' => 'admin_libriciel']);
        $this->client->request(Request::METHOD_POST, '/user/toggle', ['toggle' => [$user->getId()]]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Impossible de modifié son propre utilisateur")');
        $this->assertCount(1, $successMsg);
    }

    public function testDelete()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user = $this->getOneUserBy(['username' => 'delete1']);
        $this->client->request(Request::METHOD_DELETE, '/user/delete/' . $user->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("L\'utilisateur a bien été supprimé")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneUserBy(['username' => 'delete1']));
    }

    public function testDeleteMyself()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user = $this->getOneUserBy(['username' => 'admin_libriciel']);
        $this->client->request(Request::METHOD_DELETE, '/user/delete/' . $user->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Impossible de supprimer son propre utilisateur")');
        $this->assertCount(1, $successMsg);
    }

    public function testDeleteUserActif()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user = $this->getOneUserBy(['username' => 'admin1']);
        $this->client->request(Request::METHOD_DELETE, '/user/delete/' . $user->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Impossible de supprimer un utilisateur ACTIF")');
        $this->assertCount(1, $successMsg);
    }

    public function testDeleteMultiple()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user1 = $this->getOneUserBy(['username' => 'delete1']);
        $user2 = $this->getOneUserBy(['username' => 'delete2']);
        $user3 = $this->getOneUserBy(['username' => 'delete3']);

        $this->client->request(Request::METHOD_DELETE, '/user/deletemultiple', [
            'delete' => [
                $user1->getId(),
                $user2->getId(),
                $user3->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("user.success_message.delete")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneUserBy(['username' => 'delete1']));
        $this->assertEmpty($this->getOneUserBy(['username' => 'delete2']));
        $this->assertEmpty($this->getOneUserBy(['username' => 'delete3']));
    }

    public function testDeleteMultipleMyself()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user1 = $this->getOneUserBy(['username' => 'delete1']);
        $user2 = $this->getOneUserBy(['username' => 'delete2']);
        $user3 = $this->getOneUserBy(['username' => 'delete3']);
        $user_login = $this->getOneUserBy(['username' => 'admin_libriciel']);

        $this->client->request(Request::METHOD_DELETE, '/user/deletemultiple', [
            'delete' => [
                $user1->getId(),
                $user2->getId(),
                $user3->getId(),
                $user_login->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Impossible de supprimer son propre utilisateur")');
        $this->assertCount(1, $successMsg);
    }

    public function testPreference()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/user/preference');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modifier mes informations personnelles")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user_preference[civility]'] = 'M.';
        $form['user_preference[firstName]'] = 'Paul';
        $form['user_preference[lastName]'] = 'JAUNE';
        $form['user_preference[email]'] = 'paul.jaune@test.fr';
        $form['user_preference[cellphone]'] = '06 20 20 24 20';
        $form['user_preference[phone]'] = '04 67 67 88 67';
        $form['user_preference[notification]'] = 1;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $this->client->followRedirect();
        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Vos informations personnelles, ont bien été enregistré")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneUserBy(['username' => 'admin_libriciel']));
    }

    public function testPreferenceWithUsername()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/user/preference');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modifier mes informations personnelles")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['user_preference[username]'] = 'p.jaune';
        $form['user_preference[civility]'] = 'M.';
        $form['user_preference[firstName]'] = 'Paul';
        $form['user_preference[lastName]'] = 'JAUNE';
        $form['user_preference[email]'] = 'paul.jaune@test.fr';
        $form['user_preference[cellphone]'] = '06 07 08 09 10';
        $form['user_preference[phone]'] = '04 67 01 02 03';
        $form['user_preference[notification]'] = 1;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $this->client->followRedirect();
        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Vos informations personnelles, ont bien été enregistré")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneUserBy(['username' => 'admin_libriciel']));
    }

    public function testReloadPassword()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user = $this->getOneUserBy(['username' => 'admin1']);
        $this->client->request(Request::METHOD_POST, '/user/reaload_password', [
            'reload' => [
                $user->getId()
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter(
            'html:contains("Un e-mail de réinitialisation du mot de passe a été envoyé aux utilisateurs")'
        );
        $this->assertCount(1, $successMsg);
    }

    public function testReloadPasswordNotExisteUser()
    {
        $this->loginAsAdminLibriciel();

        /** @var User $user */
        $user = $this->getOneUserBy(['username' => 'admin1']);
        $this->client->request(Request::METHOD_POST, '/user/reaload_password', [
            'reload' => [
                '8279656B-250E-99ZZ-BE1E-572256E18612'
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter(
            'html:contains("L\'utilisateur n\'a pas été trouvé")'
        );
        $this->assertCount(1, $successMsg);
    }

    public function testReloadPasswordMultiple()
    {
        $this->loginAsAdminLibriciel();

        $user1 = $this->getOneUserBy(['username' => 'admin1']);
        $user2 = $this->getOneUserBy(['username' => 'admin2']);
        $user3 = $this->getOneUserBy(['username' => 'admin3']);
        $this->client->request(Request::METHOD_POST, '/user/reaload_password', [
            'reload' => [
                $user1->getId(),
                $user2->getId(),
                $user3->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter(
            'html:contains("Un e-mail de réinitialisation du mot de passe a été envoyé aux utilisateurs")'
        );
        $this->assertCount(1, $successMsg);
    }
}
