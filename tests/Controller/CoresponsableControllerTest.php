<?php

namespace App\Tests\Controller;

use App\DataFixtures\CoresponsableFixtures;
use App\DataFixtures\OrganisationFixtures;
use App\DataFixtures\RoleFixtures;
use App\DataFixtures\UserFixtures;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use App\Tests\Story\OrganisationStory;
use App\Tests\Story\UserStory;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class CoresponsableControllerTest extends WebTestCase
{
    use Factories, ResetDatabase;
    use FindEntityTrait;
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            OrganisationFixtures::class,
            UserFixtures::class,
            RoleFixtures::class,
            CoresponsableFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testIndex()
    {
        $this->loginAsAdminLibriciel();
        $crawler = $this->client->request(Request::METHOD_GET, '/coresponsable');
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Liste des co-responsable de l\'application")');
        $this->assertCount(1, $item);
    }

//    public function testIndex()
//    {
//        UserStory::load();
//        OrganisationStory::load();
//        $this->loginAsAdminLibriciel();
//        $crawler = $this->client->request(Request::METHOD_GET, '/coresponsable');
//        $this->assertResponseStatusCodeSame(200);
//
//        $item = $crawler->filter('html:contains("Liste des co-responsable de l\'application")');
//        $this->assertCount(1, $item);
//    }

    public function testAdd()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/coresponsable/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajouter un co-responsable")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['coresponsable[raisonsociale]'] = 'OVH';
        $form['coresponsable[phone]'] = '04 75 51 45 23';
        $form['coresponsable[fax]'] = '04 63 20 15 22';
        $form['coresponsable[email]'] = 'contact-ovh@test.fr';
        $form['coresponsable[address]'] = '2 RUE KELLERMANN 59100 ROUBAIX';
        $form['coresponsable[siren]'] = '424 761 419';
        $form['coresponsable[siret]'] = '424 761 419 00045';
        $form['coresponsable[ape]'] = '6311Z';
        $form['coresponsable[representant][civility]'] = 'Mme.';
        $form['coresponsable[representant][firstName]'] = 'Angélique';
        $form['coresponsable[representant][lastName]'] = 'ORANGE';
        $form['coresponsable[representant][function]'] = 'Maire';
        $form['coresponsable[representant][email]'] = 'a.orange@maire-ville.test';
        $form['coresponsable[delegue][civility]'] = 'M.';
        $form['coresponsable[delegue][firstName]'] = 'Jean';
        $form['coresponsable[delegue][lastName]'] = 'OPOT';
        $form['coresponsable[delegue][email]'] = 'dpo.mairie@test.fr';
        $form['coresponsable[delegue][cellphone]'] = '06 54 78 51 03';
        $form['coresponsable[delegue][phone]'] = '04 65 56 54 52';
        $form['coresponsable[delegue][numberCnil]'] = 'DPO-456461';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("coresponsable.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneCoresponsableBy(['raisonsociale' => 'OVH']));
    }

    public function testEdit()
    {
        $this->loginAsAdminLibriciel();

        $coresponsable = $this->getOneCoresponsableBy(['raisonsociale' => 'EDITION']);

        $crawler = $this->client->request(Request::METHOD_GET, '/coresponsable/edit/' . $coresponsable->getId());
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modifier un co-responsable")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['coresponsable[raisonsociale]'] = 'LEGO SAS';
        $form['coresponsable[siren]'] = '806 220 216';
        $form['coresponsable[siret]'] = '806 220 216 00055';
        $form['coresponsable[representant][civility]'] = 'M.';
        $form['coresponsable[representant][firstName]'] = 'Victor';
        $form['coresponsable[representant][lastName]'] = 'SAEYS';
        $form['coresponsable[representant][function]'] = 'PDG';
        $form['coresponsable[representant][email]'] = 'v.saeys@lego.test';
        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("coresponsable.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneCoresponsableBy(['raisonsociale' => 'EDITION']));
        $this->assertNotEmpty($this->getOneCoresponsableBy(['raisonsociale' => 'LEGO SAS']));
    }

    public function testNotEdit()
    {
        $this->loginAsAdminLibriciel();

        $coresponsable = $this->getOneCoresponsableBy(['raisonsociale' => 'NOT CAN\'T EDIT']);

        $this->client->request(Request::METHOD_GET, '/coresponsable/edit/' . $coresponsable->getId());
        $this->assertResponseStatusCodeSame(302);
    }

    public function testDelete()
    {
        $this->loginAsAdminLibriciel();

        $coresponsable = $this->getOneCoresponsableBy(['raisonsociale' => 'DELETE1']);

        $this->client->request(Request::METHOD_DELETE, '/coresponsable/delete/' . $coresponsable->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Le co-responsable a bien été supprimé")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneCoresponsableBy(['raisonsociale' => 'DELETE']));
    }

    public function testDeleteMultiple()
    {
        $this->loginAsAdminLibriciel();

        $coresponsable1 = $this->getOneCoresponsableBy(['raisonsociale' => 'DELETE1']);
        $coresponsable2 = $this->getOneCoresponsableBy(['raisonsociale' => 'DELETE2']);
        $coresponsable3 = $this->getOneCoresponsableBy(['raisonsociale' => 'DELETE3']);

        $this->client->request(Request::METHOD_DELETE, '/coresponsable/deletemultiple', [
            'delete' => [
                $coresponsable1->getId(),
                $coresponsable2->getId(),
                $coresponsable3->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("coresponsable.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneUserBy(['username' => 'DELETE1']));
        $this->assertEmpty($this->getOneUserBy(['username' => 'DELETE2']));
        $this->assertEmpty($this->getOneUserBy(['username' => 'DELETE3']));
    }

    public function testNotDelete()
    {
        $this->loginAsAdminLibriciel();

        $coresponsable = $this->getOneCoresponsableBy(['raisonsociale' => 'NOT CAN\'T EDIT']);

        $this->client->request(Request::METHOD_DELETE, '/coresponsable/delete/' . $coresponsable->getId());
        $this->assertResponseStatusCodeSame(302);
    }

//    public function testAssociation()
//    {
//        $this->loginAsAdminLibriciel();
//
//        $soustraitant = $this->getOneSoustraitantBy(['raisonsociale' => 'ASSOCIATION']);
//        $organisation = $this->getOneOrganisationBy(['raisonsociale' => 'Libriciel SCOP']);
//
//        $crawler = $this->client->request(Request::METHOD_GET, '/soustraitant');
//        $this->assertResponseStatusCodeSame(200);
//        $item = $crawler->filter('html:contains("Associer un ou plusieurs sous-traitant à une ou plusieurs entité(s)")');
//        $this->assertCount(1, $item);
//
//        $form = $crawler->selectButton('Associer')->form();
//
////        $form['multi_organisation_user[organisation][]'] = [$organisation->getId()];
////        $form['multi_organisation_user[fields][]'] = [$soustraitant->getId()];
//
//        $form['multi_organisation_user[organisation][]'] = [$organisation];
//        $form['multi_organisation_user[fields][]'] = [$soustraitant];
//
//        $this->client->submit($form);
//    }

    public function testDissociation()
    {
        $this->loginAsAdminLibriciel();

        $coresponsable = $this->getOneCoresponsableBy(['raisonsociale' => 'DISSOCIATION1']);

        $this->client->request(Request::METHOD_POST, '/coresponsable/dissociation/' . $coresponsable->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("coresponsable.success_message.save")');
        $this->assertCount(1, $successMsg);

        $coresponsableAfterDissociation = $coresponsable;
        $this->entityManager->refresh($coresponsableAfterDissociation);
        $this->assertNotEmpty($coresponsableAfterDissociation);
        $this->assertEmpty($coresponsableAfterDissociation->getOrganisations());
    }

    public function testDissociationMultiple()
    {
        $this->loginAsAdminLibriciel();

        $coresponsable1 = $this->getOneCoresponsableBy(['raisonsociale' => 'DISSOCIATION1']);
        $coresponsable2 = $this->getOneCoresponsableBy(['raisonsociale' => 'DISSOCIATION2']);
        $coresponsable3 = $this->getOneCoresponsableBy(['raisonsociale' => 'DISSOCIATION3']);
        $coresponsable4 = $this->getOneCoresponsableBy(['raisonsociale' => 'DISSOCIATION4']);
        $coresponsable5 = $this->getOneCoresponsableBy(['raisonsociale' => 'DISSOCIATION5']);

        $this->client->request(Request::METHOD_POST, '/coresponsable/dissociationmultiple', [
            'dissociation' => [
                $coresponsable1->getId(),
                $coresponsable2->getId(),
                $coresponsable3->getId(),
                $coresponsable4->getId(),
                $coresponsable5->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("coresponsable.success_message.save")');
        $this->assertCount(1, $successMsg);
    }
}
