<?php

namespace App\Tests\Controller;

use App\DataFixtures\OrganisationFixtures;
use App\DataFixtures\RoleFixtures;
use App\DataFixtures\ServiceFixtures;
use App\DataFixtures\UserFixtures;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ServiceControllerTest extends WebTestCase
{
    use FindEntityTrait;
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            OrganisationFixtures::class,
            UserFixtures::class,
            RoleFixtures::class,
            ServiceFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testIndex()
    {
        $this->loginAsAdminLibriciel();
        $crawler = $this->client->request(Request::METHOD_GET, '/service');
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Liste des services de l\'entité")');
        $this->assertCount(1, $item);
    }

    public function testAdd()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/service/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajouter un service")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['service[name]'] = 'Communication';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("service.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneServiceBy(['name' => 'Communication']));
    }

    public function testEdit()
    {
        $this->loginAsAdminLibriciel();

        $service = $this->getOneServiceBy(['name' => 'EDIT']);

        $crawler = $this->client->request(Request::METHOD_GET, '/service/edit/' . $service->getId());
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modifier un service")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['service[name]'] = 'MODIFIER';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("service.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneServiceBy(['name' => 'EDIT']));
        $this->assertNotEmpty($this->getOneServiceBy(['name' => 'MODIFIER']));
    }

    public function testDelete()
    {
        $this->loginAsAdminLibriciel();

        $service = $this->getOneServiceBy(['name' => 'EDIT']);

        $this->client->request(Request::METHOD_DELETE, '/service/delete/' . $service->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("service.success_message.delete")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneServiceBy(['name' => 'EDIT']));
    }
}
