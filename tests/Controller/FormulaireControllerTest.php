<?php

namespace App\Tests\Controller;

use App\DataFixtures\FormulaireFixtures;
use App\DataFixtures\OrganisationFixtures;
use App\DataFixtures\RoleFixtures;
use App\DataFixtures\UserFixtures;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class FormulaireControllerTest extends WebTestCase
{
    use Factories, ResetDatabase;
    use FindEntityTrait;
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            OrganisationFixtures::class,
            UserFixtures::class,
            RoleFixtures::class,
            FormulaireFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testIndex()
    {
        $this->loginAsAdminLibriciel();
        $crawler = $this->client->request(Request::METHOD_GET, '/formulaire');
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Liste des formulaires")');
        $this->assertCount(1, $item);
    }

    public function testAdd()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/formulaire/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajouter un formulaire")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['formulaire[name]'] = 'Formulaire de déclaration';
        $form['formulaire[description]'] = 'Tous les traitements doivent être déclaré via ce formulaire.';
        $form['formulaire[registre]'] = 'IsActivite';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Modification du formulaire")');
        $this->assertCount(1, $item);

        $this->assertNotEmpty($this->getOneFormulaireBy(['name' => 'Formulaire de déclaration']));
    }

    public function testEdit()
    {
        $this->loginAsAdminLibriciel();

        $formulaire = $this->getOneFormulaireBy(['name' => 'EDIT']);

        $crawler = $this->client->request(Request::METHOD_GET, '/formulaire/edit/' . $formulaire->getId());
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modification du formulaire")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['formulaire[name]'] = 'Formulaire après EDIT';
        $form['formulaire[description]'] = 'EDIT';
        $form['formulaire[options][usePia]'] = true;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("formulaire.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneFormulaireBy(['name' => 'EDIT']));
        $this->assertNotEmpty($this->getOneFormulaireBy(['name' => 'Formulaire après EDIT']));
    }

    public function testEditArchived()
    {
        $this->loginAsAdminLibriciel();

        $formulaire = $this->getOneFormulaireBy(['name' => 'ARCHIVED']);

        $crawler = $this->client->request(Request::METHOD_GET, '/formulaire/edit/' . $formulaire->getId());
        $this->assertResponseStatusCodeSame(500);
        $item = $crawler->filter('html:contains("Impossible de modifier le formulaire archivée")');
        $this->assertCount(1, $item);
    }

    public function testEditEntityNotCreative()
    {
        $this->loginAsAdminLibriciel();

        $formulaire = $this->getOneFormulaireBy(['name' => 'FOR NOT CREATIVE ENTITY']);

        $crawler = $this->client->request(Request::METHOD_GET, '/formulaire/edit/' . $formulaire->getId());
        $this->assertResponseStatusCodeSame(500);
        $item = $crawler->filter('html:contains("Impossible de modifier le formulaire d\'une autre entité")');
        $this->assertCount(1, $item);
    }

    public function testToggle()
    {
        $this->loginAsAdminLibriciel();

        $formulaireActif = $this->getOneFormulaireBy(['name' => 'ACTIF']);
        $formulaireNotActif = $this->getOneFormulaireBy(['name' => 'NOT ACTIF']);

        $this->client->request(Request::METHOD_POST, '/formulaire/toggle', [
            'toggle' => [
                $formulaireActif->getId(),
                $formulaireNotActif->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Les formulaires ont bien été modifiés")');
        $this->assertCount(1, $successMsg);
    }

//    public function testAssociation()
//    {
//        $this->loginAsAdminLibriciel();
//
//        $formulaire = $this->getOneFormulaireBy(['name' => 'ASSOCIATION']);
//        $organisation = $this->getOneOrganisationBy(['raisonsociale' => 'Libriciel SCOP']);
//
//        $crawler = $this->client->request(Request::METHOD_POST, '/formulaire/association', [
//            'multi_organisation_user' => [
//                'organisation' => [
//                    json_encode($organisation)
//                ],
//                'fields' => [
//                    $formulaire->getId()
//                ]
//            ]
//        ]);
//
//        $crawler = $this->client->followRedirect();
//        $this->assertResponseStatusCodeSame(200);
//-----------------------------------------------------------------
//        $successMsg = $crawler->filter('html:contains("formulaire.success_message.save")');
//        $this->assertCount(1, $successMsg);

//        $this->loginAsAdminLibriciel();
//
//        $formulaire = $this->getOneFormulaireBy(['name' => 'ASSOCIATION']);
//        $organisation = $this->getOneOrganisationBy(['raisonsociale' => 'Libriciel SCOP']);
//
//        $crawler = $this->client->request(Request::METHOD_GET, '/formulaire');
//        $this->assertResponseStatusCodeSame(200);
//        $item = $crawler->filter('html:contains("Associer un ou plusieurs formulaire(s) à une ou plusieurs entité(s)")');
//        $this->assertCount(1, $item);
//
//        $form = $crawler->selectButton('Associer')->form();
//
//        $form['multi_organisation_user[organisation][]'] = json_encode($organisation);
//        $form['multi_organisation_user[fields][]'] = $formulaire->getId();
//
//        $this->client->submit($form);
//
//        $this->assertTrue($this->client->getResponse()->isRedirect());
//
//        $crawler = $this->client->followRedirect();
//        $this->assertResponseStatusCodeSame(200);
//
//        $successMsg = $crawler->filter('html:contains("formulaire.success_message.save")');
//        $this->assertCount(1, $successMsg);
//    }

    public function testDissociation()
    {
        $this->loginAsAdminLibriciel();

        $formulaire = $this->getOneFormulaireBy(['name' => 'DISSOCIATION_1']);

        $this->client->request(Request::METHOD_POST, '/formulaire/dissociation/' . $formulaire->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("formulaire.success_message.save")');
        $this->assertCount(1, $successMsg);

        $formulaireAfterDissociation = $formulaire;
        $this->entityManager->refresh($formulaireAfterDissociation);
        $this->assertNotEmpty($formulaireAfterDissociation);
        $this->assertEmpty($formulaireAfterDissociation->getOrganisations());
    }

    public function testDissociationMultiple()
    {
        $this->loginAsAdminLibriciel();

        $formulaire1 = $this->getOneFormulaireBy(['name' => 'DISSOCIATION_1']);
        $formulaire2 = $this->getOneFormulaireBy(['name' => 'DISSOCIATION_2']);
        $formulaire3 = $this->getOneFormulaireBy(['name' => 'DISSOCIATION_3']);
        $formulaire4 = $this->getOneFormulaireBy(['name' => 'DISSOCIATION_4']);
        $formulaire5 = $this->getOneFormulaireBy(['name' => 'DISSOCIATION_5']);

        $this->client->request(Request::METHOD_POST, '/formulaire/dissociationmultiple', [
            'dissociation' => [
                $formulaire1->getId(),
                $formulaire2->getId(),
                $formulaire3->getId(),
                $formulaire4->getId(),
                $formulaire5->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("formulaire.success_message.save")');
        $this->assertCount(1, $successMsg);
    }

    public function testDelete()
    {
        $this->loginAsAdminLibriciel();

        $formulaire = $this->getOneFormulaireBy(['name' => 'DELETE_1']);

        $this->client->request(Request::METHOD_DELETE, '/formulaire/delete/' . $formulaire->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("formulaire.success_message.delete")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneFormulaireBy(['name' => 'DELETE_1']));
    }

    public function testDeleteMultiple()
    {
        $this->loginAsAdminLibriciel();

        $formulaire1 = $this->getOneFormulaireBy(['name' => 'DELETE_1']);
        $formulaire2 = $this->getOneFormulaireBy(['name' => 'DELETE_2']);
        $formulaire3 = $this->getOneFormulaireBy(['name' => 'DELETE_3']);
        $formulaire4 = $this->getOneFormulaireBy(['name' => 'DELETE_4']);
        $formulaire5 = $this->getOneFormulaireBy(['name' => 'DELETE_5']);

        $this->client->request(Request::METHOD_DELETE, '/formulaire/deletemultiple', [
            'delete' => [
                $formulaire1->getId(),
                $formulaire2->getId(),
                $formulaire3->getId(),
                $formulaire4->getId(),
                $formulaire5->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("formulaire.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneFormulaireBy(['name' => 'DELETE_1']));
        $this->assertEmpty($this->getOneFormulaireBy(['name' => 'DELETE_2']));
        $this->assertEmpty($this->getOneFormulaireBy(['name' => 'DELETE_3']));
        $this->assertEmpty($this->getOneFormulaireBy(['name' => 'DELETE_4']));
        $this->assertEmpty($this->getOneFormulaireBy(['name' => 'DELETE_5']));
    }

    public function testNotDelete()
    {
        $this->loginAsAdminLibriciel();

        $formulaire = $this->getOneFormulaireBy(['name' => 'DISSOCIATION_1']);

        $this->client->request(Request::METHOD_DELETE, '/formulaire/delete/' . $formulaire->getId());
        $this->assertResponseStatusCodeSame(302);
    }

    public function testArchived()
    {
        $this->loginAsAdminLibriciel();

        $formulaireNotArchived = $this->getOneFormulaireBy(['name' => 'NOT ARCHIVED']);
        $this->client->request(Request::METHOD_POST, '/formulaire/archived', [
            'archived' => [
                $formulaireNotArchived->getId()
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("Les formulaires ont bien été archivés")');
        $this->assertCount(1, $successMsg);
    }
}
