<?php

namespace App\Tests\Controller;

use App\DataFixtures\OrganisationFixtures;
use App\DataFixtures\RoleFixtures;
use App\DataFixtures\SoustraitantFixtures;
use App\DataFixtures\UserFixtures;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SoustraitantControllerTest extends WebTestCase
{
    use FindEntityTrait;
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            OrganisationFixtures::class,
            UserFixtures::class,
            RoleFixtures::class,
            SoustraitantFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testIndex()
    {
        $this->loginAsAdminLibriciel();
        $crawler = $this->client->request(Request::METHOD_GET, '/soustraitant');
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Liste des sous-traitant de l\'application")');
        $this->assertCount(1, $item);
    }

    public function testAdd()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/soustraitant/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajouter un soustraitant")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['soustraitant[raisonsociale]'] = 'Jouet Club';
        $form['soustraitant[representant][civility]'] = 'Mme.';
        $form['soustraitant[representant][firstName]'] = 'Corinne';
        $form['soustraitant[representant][lastName]'] = 'VERT';
        $form['soustraitant[representant][function]'] = 'Maire';
        $form['soustraitant[representant][email]'] = 'c.vert@maire-ville.test';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("soustraitant.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneSoustraitantBy(['raisonsociale' => 'Jouet Club']));
    }

    public function testEdit()
    {
        $this->loginAsAdminLibriciel();

        $soustraitant = $this->getOneSoustraitantBy(['raisonsociale' => 'TOTO']);

        $crawler = $this->client->request(Request::METHOD_GET, '/soustraitant/edit/' . $soustraitant->getId());
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Modifier un soustraitant")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['soustraitant[raisonsociale]'] = 'FERRARI FRANCE SARL';
        $form['soustraitant[representant][civility]'] = 'M.';
        $form['soustraitant[representant][firstName]'] = 'Jean-Michel';
        $form['soustraitant[representant][lastName]'] = 'BLEU';
        $form['soustraitant[representant][function]'] = 'PDG';
        $form['soustraitant[representant][email]'] = 'jm.bleu@ferrari.test';
        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("soustraitant.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneSoustraitantBy(['raisonsociale' => 'TOTO']));
        $this->assertNotEmpty($this->getOneSoustraitantBy(['raisonsociale' => 'FERRARI FRANCE SARL']));
    }

    public function testNotEdit()
    {
        $this->loginAsAdminLibriciel();

        $soustraitant = $this->getOneSoustraitantBy(['raisonsociale' => 'NOT CAN\'T EDIT']);

        $this->client->request(Request::METHOD_GET, '/soustraitant/edit/' . $soustraitant->getId());
        $this->assertResponseStatusCodeSame(302);
    }

    public function testDelete()
    {
        $this->loginAsAdminLibriciel();

        $soustraitant = $this->getOneSoustraitantBy(['raisonsociale' => 'DELETE']);

        $this->client->request(Request::METHOD_DELETE, '/soustraitant/delete/' . $soustraitant->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Le sous-traitant a bien été supprimé")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneSoustraitantBy(['raisonsociale' => 'DELETE']));
    }

    public function testNotDelete()
    {
        $this->loginAsAdminLibriciel();

        $soustraitant = $this->getOneSoustraitantBy(['raisonsociale' => 'NOT CAN\'T EDIT']);

        $this->client->request(Request::METHOD_DELETE, '/soustraitant/delete/' . $soustraitant->getId());
        $this->assertResponseStatusCodeSame(302);
    }

//    public function testAssociation()
//    {
//        $this->loginAsAdminLibriciel();
//
//        $soustraitant = $this->getOneSoustraitantBy(['raisonsociale' => 'ASSOCIATION']);
//        $organisation = $this->getOneOrganisationBy(['raisonsociale' => 'Libriciel SCOP']);
//
//        $crawler = $this->client->request(Request::METHOD_GET, '/soustraitant');
//        $this->assertResponseStatusCodeSame(200);
//        $item = $crawler->filter('html:contains("Associer un ou plusieurs sous-traitant à une ou plusieurs entité(s)")');
//        $this->assertCount(1, $item);
//
//        $form = $crawler->selectButton('Associer')->form();
//
////        $form['multi_organisation_user[organisation][]'] = [$organisation->getId()];
////        $form['multi_organisation_user[fields][]'] = [$soustraitant->getId()];
//
//        $form['multi_organisation_user[organisation][]'] = [$organisation];
//        $form['multi_organisation_user[fields][]'] = [$soustraitant];
//
//        $this->client->submit($form);
//    }

    public function testDissociation()
    {
        $this->loginAsAdminLibriciel();

        $soustraitant = $this->getOneSoustraitantBy(['raisonsociale' => 'DISSOCIATION']);

        $this->client->request(Request::METHOD_POST, '/soustraitant/dissociation/' . $soustraitant->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("soustraitant.success_message.save")');
        $this->assertCount(1, $successMsg);

        $soustraitantAfterDissociation = $soustraitant;
        $this->entityManager->refresh($soustraitantAfterDissociation);
        $this->assertNotEmpty($soustraitantAfterDissociation);
        $this->assertEmpty($soustraitantAfterDissociation->getOrganisations());
    }
}
