<?php

namespace App\Tests\Controller;

use App\DataFixtures\OrganisationFixtures;
use App\DataFixtures\RoleFixtures;
use App\DataFixtures\TypageFixtures;
use App\DataFixtures\UserFixtures;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class TypageControllerTest extends WebTestCase
{
    use FindEntityTrait;
    use LoginTrait;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            OrganisationFixtures::class,
            UserFixtures::class,
            RoleFixtures::class,
            TypageFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testIndex()
    {
        $this->loginAsAdminLibriciel();
        $crawler = $this->client->request(Request::METHOD_GET, '/typage');
        $this->assertResponseStatusCodeSame(200);

        $item = $crawler->filter('html:contains("Liste des types d\'annexes")');
        $this->assertCount(1, $item);
    }

    public function testAdd()
    {
        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/typage/add');
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajouter un type d\'annexe")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['typage[name]'] = 'Annexe RGPD';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("typage.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertNotEmpty($this->getOneTypageBy(['name' => 'Annexe RGPD']));
    }

    public function testEdit()
    {
        $this->loginAsAdminLibriciel();

        $typageBeforeUpdate = $this->getOneTypageBy(['name' => 'EDIT']);

        $crawler = $this->client->request(Request::METHOD_GET, '/typage/edit/' . $typageBeforeUpdate->getId());
        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Ajouter un type d\'annexe")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['typage[name]'] = 'Annexe édition';

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("typage.success_message.save")');
        $this->assertCount(1, $successMsg);

        $typageAfterUpdate = $typageBeforeUpdate;
        $this->entityManager->refresh($typageAfterUpdate);
        $this->assertNotEmpty($typageAfterUpdate);

        //        if ($typageAfterUpdate->getId() !== $typageBeforeUpdate->getId()) {
        //            $this->expectErrorMessage("Error save edit ID");
        //        }
        //
        //        if ($typageAfterUpdate->getCreatedByOrganisation() !== $typageBeforeUpdate->getCreatedByOrganisation()) {
        //            $this->expectErrorMessage("Error save edit CreatedByOrganisation");
        //        }
        //
        //        if ($typageAfterUpdate->getName() !== $form['typage']['name']->getValue()) {
        //            $this->expectErrorMessage("Error save name");
        //        }
    }

    public function testNotEdit()
    {
        $this->loginAsAdminLibriciel();

        $typage = $this->getOneTypageBy(['name' => 'NOT CAN\'T EDIT']);

        $this->client->request(Request::METHOD_GET, '/typage/edit/' . $typage->getId());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Impossible de modifier le type d\'annexe")');
        $this->assertCount(1, $successMsg);
    }

    public function testDelete()
    {
        $this->loginAsAdminLibriciel();

        $typage = $this->getOneTypageBy(['name' => 'DELETE_1']);

        $this->client->request(Request::METHOD_DELETE, '/typage/delete/' . $typage->getId());
        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("typage.success_message.delete")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneTypageBy(['id' => $typage->getId()]));
    }

    public function testDeleteMultiple()
    {
        $this->loginAsAdminLibriciel();

        $typage1 = $this->getOneTypageBy(['name' => 'DELETE_1']);
        $typage2 = $this->getOneTypageBy(['name' => 'DELETE_2']);
        $typage3 = $this->getOneTypageBy(['name' => 'DELETE_3']);
        $typage4 = $this->getOneTypageBy(['name' => 'DELETE_4']);
        $typage5 = $this->getOneTypageBy(['name' => 'DELETE_5']);

        $this->client->request(Request::METHOD_DELETE, '/typage/deletemultiple', [
            'delete' => [
                $typage1->getId(),
                $typage2->getId(),
                $typage3->getId(),
                $typage4->getId(),
                $typage5->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("typage.success_message.save")');
        $this->assertCount(1, $successMsg);

        $this->assertEmpty($this->getOneTypageBy(['name' => 'DELETE_1']));
        $this->assertEmpty($this->getOneTypageBy(['name' => 'DELETE_2']));
        $this->assertEmpty($this->getOneTypageBy(['name' => 'DELETE_3']));
        $this->assertEmpty($this->getOneTypageBy(['name' => 'DELETE_4']));
        $this->assertEmpty($this->getOneTypageBy(['name' => 'DELETE_5']));
    }

    public function testDissociation()
    {
        $this->loginAsAdminLibriciel();

        $typage = $this->getOneTypageBy(['name' => 'DISSOCIATION_1']);

        $this->client->request(Request::METHOD_POST, '/typage/dissociation/' . $typage->getId());
        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $successMsg = $crawler->filter('html:contains("typage.success_message.save")');
        $this->assertCount(1, $successMsg);

        $typageAfterUpdate = $typage;
        $this->entityManager->refresh($typageAfterUpdate);
        $this->assertNotEmpty($typageAfterUpdate);

        if ($typageAfterUpdate->getId() !== $typage->getId()) {
            $this->expectErrorMessage("Error save ID");
        }

        if ($typageAfterUpdate->getCreatedByOrganisation() !== $typage->getCreatedByOrganisation()) {
            $this->expectErrorMessage("Error save CreatedByOrganisation");
        }

        if ($typageAfterUpdate->getName() !== $typage->getName()) {
            $this->expectErrorMessage("Error save name");
        }

        if (!empty($typageAfterUpdate->getOrganisations()->toArray())) {
            $this->expectErrorMessage("Error dissociation organisation");
        }
    }

    public function testDissociationMultiple()
    {
        $this->loginAsAdminLibriciel();

        $typage1 = $this->getOneTypageBy(['name' => 'DISSOCIATION_1']);
        $typage2 = $this->getOneTypageBy(['name' => 'DISSOCIATION_2']);
        $typage3 = $this->getOneTypageBy(['name' => 'DISSOCIATION_3']);
        $typage4 = $this->getOneTypageBy(['name' => 'DISSOCIATION_4']);
        $typage5 = $this->getOneTypageBy(['name' => 'DISSOCIATION_5']);

        $this->client->request(Request::METHOD_POST, '/typage/dissociationmultiple', [
            'dissociation' => [
                $typage1->getId(),
                $typage2->getId(),
                $typage3->getId(),
                $typage4->getId(),
                $typage5->getId(),
            ]
        ]);

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("typage.success_message.save")');
        $this->assertCount(1, $successMsg);
    }
}
