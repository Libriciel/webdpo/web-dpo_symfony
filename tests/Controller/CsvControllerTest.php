<?php

namespace App\Tests\Controller;

use App\DataFixtures\RoleFixtures;
use App\DataFixtures\ServiceFixtures;
use App\DataFixtures\TypeFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Type;
use App\Tests\FindEntityTrait;
use App\Tests\LoginTrait;
use Doctrine\Persistence\ObjectManager;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class CsvControllerTest extends WebTestCase
{
    use FindEntityTrait;
    use LoginTrait;

    private ?KernelBrowser $client;
    /**
     * @var ObjectManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();

        $databaseTool->loadFixtures([
            UserFixtures::class,
            RoleFixtures::class,
            ServiceFixtures::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->client = null;
        $this->entityManager->close();
    }

    public function testImportServices()
    {
        $csvFile = new UploadedFile(__DIR__ . '/../resources/services_success.csv', 'services_success.csv');
        $this->assertNotEmpty($csvFile);

        $this->loginAsAdminLibriciel();

        $crawler = $this->client->request(Request::METHOD_GET, '/csv/importServices');

        $this->assertResponseStatusCodeSame(200);
        $item = $crawler->filter('html:contains("Importer des services via csv")');
        $this->assertCount(1, $item);

        $form = $crawler->selectButton('Enregistrer')->form();

        $form['csv[csv]'] = $csvFile;

        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $crawler = $this->client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $successMsg = $crawler->filter('html:contains("Fichier csv importé avec succès")');
        $this->assertCount(1, $successMsg);

        //        $this->assertNotEmpty($this->getOneServiceBy(['name' => 'DSI']));
        //        $this->assertNotEmpty($this->getOneServiceBy(['name' => 'DRH']));
        //        $this->assertNotEmpty($this->getOneServiceBy(['name' => 'Informatique']));
        //        $this->assertEmpty($this->getOneServiceBy(['name' => '']));
        //        $this->assertNotEmpty($this->getOneServiceBy(['name' => 'Com']));
        //        $this->assertNotEmpty($this->getOneServiceBy(['name' => 'Compta']));
    }
}
