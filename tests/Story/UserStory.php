<?php

namespace App\Tests\Story;

use App\Tests\Factory\UserFactory;
use Zenstruck\Foundry\Story;

final class UserStory extends Story
{
    public const PASSWORD = '$2y$13$zfBa/5vb4mhrRAjqZC6e4OEKyV2Wxpdbc4aUo873CW0pb30PqPefS';

    public function build(): void
    {
        $this->add('superAdmin', UserFactory::new([
            'username' => 'superadmin',
            'password' => self::PASSWORD,
            'civility' => 'M.',
            'firstName' => 'Super',
            'lastName' => 'ADMIN',
            'email' => 'superadmin@webdpo.test',
            'isActive' => true
        ]));

        $this->add('adminLibriciel', UserFactory::new([
            'username' => 'admin_libriciel',
            'password' => self::PASSWORD,
            'civility' => 'M.',
            'firstName' => 'Jean',
            'lastName' => 'ADMIN',
            'email' => 'j.admin@libriciel.test',
            'isActive' => true,
            'organisation' => OrganisationStory::libriciel()
        ]));
    }
}
