<?php

namespace App\Tests\Story;

use App\Tests\Factory\OrganisationFactory;
use Zenstruck\Foundry\Story;

final class OrganisationStory extends Story
{
    public function build(): void
    {
        $this->add('libriciel', OrganisationFactory::new([
            'name' => 'Libriciel'
        ]));
    }
}
