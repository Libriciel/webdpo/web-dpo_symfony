CREATE file ".env.local"

sudo ./start_dev.sh -f

docker-compose -f docker-compose.dev.yml build
docker-compose -f docker-compose.dev.yml up -V

docker exec -it fpm_webdpo bash

bin/console doctrine:fixtures:load
