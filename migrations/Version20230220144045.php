<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230220144045 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_organisation DROP CONSTRAINT FK_35AD45257294869C');
        $this->addSql('ALTER TABLE article_organisation ADD CONSTRAINT FK_35AD45257294869C FOREIGN KEY (article_id) REFERENCES article (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE coresponsable DROP CONSTRAINT FK_DF5EBF806C4A52F0');
        $this->addSql('ALTER TABLE coresponsable DROP CONSTRAINT FK_DF5EBF80C283956F');
        $this->addSql('ALTER TABLE coresponsable ADD CONSTRAINT FK_DF5EBF806C4A52F0 FOREIGN KEY (representant_id) REFERENCES "representant" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE coresponsable ADD CONSTRAINT FK_DF5EBF80C283956F FOREIGN KEY (delegue_id) REFERENCES "delegue" (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX uniq_51c962525f37a13b RENAME TO IDX_FORGETTOKEN_TOKEN');
        $this->addSql('ALTER INDEX uniq_5bdd01a85e237e06 RENAME TO IDX_FORMULAIRE_NAME');
        $this->addSql('ALTER TABLE formulaire_organisation DROP CONSTRAINT FK_EB3779FE5053569B');
        $this->addSql('ALTER TABLE formulaire_organisation ADD CONSTRAINT FK_EB3779FE5053569B FOREIGN KEY (formulaire_id) REFERENCES "formulaire" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_party_name_organisation RENAME TO IDX_SERVICE_NAME_ORGANISATION');
        $this->addSql('ALTER INDEX uniq_375ab422176c17b6 RENAME TO IDX_SOUSTRAITANT_RAISONSOCIALE');
        $this->addSql('ALTER INDEX uniq_375ab422444f97dd RENAME TO IDX_SOUSTRAITANT_PHONE');
        $this->addSql('ALTER INDEX uniq_375ab4229123cd68 RENAME TO IDX_SOUSTRAITANT_FAX');
        $this->addSql('ALTER INDEX uniq_375ab422e7927c74 RENAME TO IDX_SOUSTRAITANT_EMAIL');
        $this->addSql('ALTER INDEX uniq_375ab42226e94372 RENAME TO IDX_SOUSTRAITANT_SIRET');
        $this->addSql('ALTER INDEX uniq_375ab422e48e9a13 RENAME TO IDX_SOUSTRAITANT_LOGO');
        $this->addSql('ALTER TABLE traitement ALTER valide SET NOT NULL');
        $this->addSql('ALTER INDEX uniq_2a356d27f55ae19e RENAME TO IDX_TRAITEMENT_NUMERO');
        $this->addSql('ALTER INDEX uniq_e0fd05f65e237e06 RENAME TO IDX_TYPAGE_NAME');
        $this->addSql('ALTER TABLE typage_organisation DROP CONSTRAINT FK_EBD786596973B056');
        $this->addSql('ALTER TABLE typage_organisation ADD CONSTRAINT FK_EBD786596973B056 FOREIGN KEY (typage_id) REFERENCES "typage" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ALTER civility DROP NOT NULL');
        $this->addSql('ALTER INDEX uniq_8d93d649f85e0677 RENAME TO IDX_USER_USERNAME');
        $this->addSql('ALTER INDEX uniq_8d93d649e7927c74 RENAME TO IDX_USER_EMAIL');
        $this->addSql('ALTER INDEX uniq_8d93d649aaa18e60 RENAME TO IDX_USER_CELLPHONE');
        $this->addSql('ALTER INDEX uniq_8d93d649444f97dd RENAME TO IDX_USER_PHONE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "traitement" ALTER valide DROP NOT NULL');
        $this->addSql('ALTER INDEX idx_traitement_numero RENAME TO uniq_2a356d27f55ae19e');
        $this->addSql('ALTER INDEX idx_service_name_organisation RENAME TO idx_party_name_organisation');
        $this->addSql('ALTER TABLE typage_organisation DROP CONSTRAINT fk_ebd786596973b056');
        $this->addSql('ALTER TABLE typage_organisation ADD CONSTRAINT fk_ebd786596973b056 FOREIGN KEY (typage_id) REFERENCES typage (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_soustraitant_logo RENAME TO uniq_375ab422e48e9a13');
        $this->addSql('ALTER INDEX idx_soustraitant_siret RENAME TO uniq_375ab42226e94372');
        $this->addSql('ALTER INDEX idx_soustraitant_email RENAME TO uniq_375ab422e7927c74');
        $this->addSql('ALTER INDEX idx_soustraitant_fax RENAME TO uniq_375ab4229123cd68');
        $this->addSql('ALTER INDEX idx_soustraitant_phone RENAME TO uniq_375ab422444f97dd');
        $this->addSql('ALTER INDEX idx_soustraitant_raisonsociale RENAME TO uniq_375ab422176c17b6');
        $this->addSql('ALTER TABLE formulaire_organisation DROP CONSTRAINT fk_eb3779fe5053569b');
        $this->addSql('ALTER TABLE formulaire_organisation ADD CONSTRAINT fk_eb3779fe5053569b FOREIGN KEY (formulaire_id) REFERENCES formulaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_typage_name RENAME TO uniq_e0fd05f65e237e06');
        $this->addSql('ALTER TABLE article_organisation DROP CONSTRAINT fk_35ad45257294869c');
        $this->addSql('ALTER TABLE article_organisation ADD CONSTRAINT fk_35ad45257294869c FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_formulaire_name RENAME TO uniq_5bdd01a85e237e06');
        $this->addSql('ALTER INDEX idx_forgettoken_token RENAME TO uniq_51c962525f37a13b');
        $this->addSql('ALTER TABLE "coresponsable" DROP CONSTRAINT fk_df5ebf806c4a52f0');
        $this->addSql('ALTER TABLE "coresponsable" DROP CONSTRAINT fk_df5ebf80c283956f');
        $this->addSql('ALTER TABLE "coresponsable" ADD CONSTRAINT fk_df5ebf806c4a52f0 FOREIGN KEY (representant_id) REFERENCES representant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "coresponsable" ADD CONSTRAINT fk_df5ebf80c283956f FOREIGN KEY (delegue_id) REFERENCES delegue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ALTER civility SET NOT NULL');
        $this->addSql('ALTER INDEX idx_user_phone RENAME TO uniq_8d93d649444f97dd');
        $this->addSql('ALTER INDEX idx_user_cellphone RENAME TO uniq_8d93d649aaa18e60');
        $this->addSql('ALTER INDEX idx_user_email RENAME TO uniq_8d93d649e7927c74');
        $this->addSql('ALTER INDEX idx_user_username RENAME TO uniq_8d93d649f85e0677');
    }
}
