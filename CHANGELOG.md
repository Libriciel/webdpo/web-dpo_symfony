# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.
Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

[3.0.0] - XX-XX-XXXX
=====

### Ajouts

### Evolutions

### Corrections

### Suppressions
