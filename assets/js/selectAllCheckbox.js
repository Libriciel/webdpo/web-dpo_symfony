import $ from 'jquery';

$(document).ready(function () {
    let jsCheckbox = '.js-checkbox',
        classChecked = 'trChecked',
        checkboxID = 'allCheckbox',
        possibleActions = getPossibleActions(jsCheckbox);

    $(jsCheckbox).each(function () {
        this.checked = false;
    });

    $(jsCheckbox).click(function () {
        if (this.checked) {
            $(this).closest('tr').addClass(classChecked);
        } else {
            $(this).closest('tr').removeClass(classChecked);
        }

        checkUseAction(possibleActions);
    });

    $(jsCheckbox).change(function () {
        $('#' + checkboxID).prop('checked', $(jsCheckbox).not('#' + checkboxID).not(':disabled').not(':checked').length === 0);
    });

    selectAllCheckbox(checkboxID, classChecked, possibleActions);
});

function getPossibleActions(jsCheckbox)
{
    let possibleActions = [];

    $(jsCheckbox+":first").each(function() {
        $.each(this.attributes, function() {
            // this.attributes is not a plain object, but an array
            // of attribute nodes, which contain both the name and value
            if (this.specified) {
                if (this.name.startsWith("data-") === true) {
                    let attr = this.name;
                    possibleActions.push(attr.substr(attr.indexOf("-") + 1));
                }
            }
        });
    });

    return possibleActions;
}

function selectAllCheckbox(checkboxID, classChecked, possibleActions)
{
    $("#" + checkboxID).change(function () {
        $("." + checkboxID).not(':disabled').prop('checked', $(this).prop('checked'));

        if ($(this).is(':checked')) {
            $('.' + checkboxID).closest('tr').addClass(classChecked);
        } else {
            $('.' + checkboxID).closest('tr').removeClass(classChecked);
        }

        checkUseAction(possibleActions);
    });
}

function checkUseAction(possibleActions)
{
    let checkboxsChecked = $('.js-checkbox:checkbox:checked');

    $(possibleActions).each(function () {
        let action = this,
            allValue = [],
            btnActionId = 'btn_'+action;

        $.map(checkboxsChecked, function (val) {
            allValue.push($(val).attr('data-'+action));
        });

        const isEqualToTrue = (currentValue) => currentValue === "true";

        if (allValue.length !== 0) {
            useBtn(allValue.every(isEqualToTrue), btnActionId);
        } else {
            useBtn(false, btnActionId);
        }
    });
}
/**
 * Activation ou désactivation du bouton d'action
 *
 * @param value
 * @param btnId
 */
function useBtn(value, btnId)
{
    if (value === true) {
        $("#" + btnId).removeAttr('disabled');
    } else {
        $("#" + btnId).attr('disabled', true);
    }
}
