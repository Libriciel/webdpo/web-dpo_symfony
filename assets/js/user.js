import $ from 'jquery';

$(document).ready(function () {

    const addItemRole = (organisation_id, organisation_raisonsociale) => {
        let rolesCollectionHolder = document.querySelector('#roles');
        let itemRole = document.createElement("div");
        itemRole.classList.add("col-md-12");

        itemRole.innerHTML = rolesCollectionHolder.dataset.prototype.replace(/__name__/g, organisation_id);
        itemRole.innerHTML = itemRole.innerHTML.replace('__labelRole__', "Profil de l'utilisateur dans l'entité : " + organisation_raisonsociale);
        rolesCollectionHolder.appendChild(itemRole);

        $('#user_organisationUserRoles_'+organisation_id+'_organisation').val(organisation_id);
    }

    let elementUserOrganisations = $("#user_organisations");

    $(elementUserOrganisations).on('select2:select', function(e) {
        let organisation_id = e.params.data.id,
            organisation_raisonsociale = e.params.data.text;

        addItemRole(organisation_id, organisation_raisonsociale);
    });

    $(elementUserOrganisations).on('select2:unselect', function(e) {
        console.log('#user_organisationUserRoles_'+e.params.data.id);
        document.querySelector('#user_organisationUserRoles_'+e.params.data.id).remove();
    });
});
