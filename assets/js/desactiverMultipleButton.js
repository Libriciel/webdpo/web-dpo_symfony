import $ from 'jquery';

document.addEventListener('DOMContentLoaded', function() {
    let idBtnDesactiver = $('.js-btn-desactiver').data('idbtndesactiver'),
        idFormDesactiver = $('.js-btn-desactiver').data('idformdesactiver');

    $('.js-checkbox').click(function () {
        let userIdDesactiver = $(this).prop('value'),
            valueToggleDesactiver = $(this).attr('data-' + idBtnDesactiver.substr(idBtnDesactiver.indexOf("_") + 1));

        if (this.checked) {
            if (valueToggleDesactiver === 'true') {
                valueToggleDesactiver = 1;
            } else {
                valueToggleDesactiver = 0;
            }

            let hidden = '<input type="hidden" name="toggle[' + userIdDesactiver + ']" value="' + valueToggleDesactiver + '" />';
            $("#" + idFormDesactiver).append(hidden);
        } else {
            $("#" + idFormDesactiver).find('input[name="toggle['+userIdDesactiver+']"]').remove();
        }
    });

    $("#allCheckbox").change(function () {
        $(".allCheckbox").not(':disabled').prop('checked', $(this).prop('checked'));

        if ($(this).is(':checked')) {
            $('.allCheckbox').each(function () {
                let userIdDesactiver = $(this).prop('value'),
                    valueToggleDesactiver = $(this).attr('data-' + idBtnDesactiver.substr(idBtnDesactiver.indexOf("_") + 1));

                if (valueToggleDesactiver === 'true') {
                    valueToggleDesactiver = 1;
                } else {
                    valueToggleDesactiver = 0;
                }

                let hidden = '<input type="hidden" name="toggle[' + userIdDesactiver + ']" value="' + valueToggleDesactiver + '" />';
                $("#" + idFormDesactiver).append(hidden);
            });
        } else {
            $('.allCheckbox').each(function () {
                let userIdDesactiver = $(this).prop('value');
                $("#" + idFormDesactiver).find('input[name="toggle['+userIdDesactiver+']"]').remove();
            });
        }
    });
});
