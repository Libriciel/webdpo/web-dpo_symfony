import $ from "jquery";
import tinymce from "tinymce";

import "tinymce/themes/silver/theme.min";
import "tinymce/icons/default/icons.min"

import "tinymce-i18n/langs/fr_FR";

import "tinymce/plugins/lists";
import "tinymce/plugins/link";
import "tinymce/plugins/autolink";
import "tinymce/plugins/table";
import "tinymce/plugins/image";
import "tinymce/plugins/wordcount";
import "tinymce/plugins/preview";
import "tinymce/plugins/hr";
import "tinymce/plugins/searchreplace";

$(document).ready(function () {
    tinymce.init({
        selector: '.tinymceField',
        skin: false,
        content_css: false,
        // browser_spellcheck: true,
        // mode : 'textareas',
        // editor_selector: 'startTinyMCE',
        language_url : 'fr_FR',
        language: 'fr_FR',
        setup: function(editor) {
            editor.on('Change Keyup', function () {
                editor.save();
                //tinyMCE.triggerSave()
            });
        },
        plugins: [
            'lists',
            'link',
            'autolink',
            'table',
            'image',
            'wordcount',
            'preview',
            'hr',
            'searchreplace',
        ],
        toolbar: "styleselect fontsizeselect | bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link unlink | codesample image media | preview",
    });
});
