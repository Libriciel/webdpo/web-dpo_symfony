import $ from "jquery";

import '../form_buider/create_form';
import '../form_buider/save_form';

import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/droppable';
import 'jquery-ui/ui/widgets/resizable';

$(document).ready(function () {
    let typeCreateForm = 'formulaire';

    createForm(typeCreateForm);

    $('#save_test').click(function () {
        saveForm(typeCreateForm);
    });

    // $('#form-container-formulaire').resizable({
    //     handles: 's'
    // });
    $(".form-container").resizable({
        handles: "s"
    });

    $('form').on('submit', function(event) {
        saveForm(typeCreateForm);
    });
});