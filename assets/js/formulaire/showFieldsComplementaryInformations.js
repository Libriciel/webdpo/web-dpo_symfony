import $ from 'jquery';

$(document).ready(function () {
    let fieldUseSousFinalite = $('#formulaire_options_useSousFinalite');
    showOrHide(fieldUseSousFinalite.val(), $('#infoSupSousFinalite'));
    fieldUseSousFinalite.change(function () {
        showOrHide($(this).val(), $('#infoSupSousFinalite'));
    });

    let fieldUseBaseLegale = $('#formulaire_options_useBaseLegale');
    showOrHide(fieldUseBaseLegale.val(), $('#infoSupBaseLegale'));
    fieldUseBaseLegale.change(function () {
        showOrHide($(this).val(), $('#infoSupBaseLegale'));
    });

    let fieldUseDecisionAutomatisee = $('#formulaire_options_useDecisionAutomatisee');
    showOrHide(fieldUseDecisionAutomatisee.val(), $('#infoSupDecisionAutomatisee'));
    fieldUseDecisionAutomatisee.change(function () {
        showOrHide($(this).val(), $('#infoSupDecisionAutomatisee'));
    });

    let fieldUseTransfertHorsUe = $('#formulaire_options_useTransfertHorsUe');
    showOrHide(fieldUseTransfertHorsUe.val(), $('#infoSupTransfertHorsUe'));
    fieldUseTransfertHorsUe.change(function () {
        showOrHide($(this).val(), $('#infoSupTransfertHorsUe'));
    });

    let fieldUseDonneesSensible = $('#formulaire_options_useDonneesSensible');
    showOrHide(fieldUseDonneesSensible.val(), $('#infoSupDonneesSensibles'));
    fieldUseDonneesSensible.change(function () {
        showOrHide($(this).val(), $('#infoSupDonneesSensibles'));
    });
});

function showOrHide(val, field)
{
    if (val == true) {
        $(field).show();
    } else {
        $(field).hide();
    }
}