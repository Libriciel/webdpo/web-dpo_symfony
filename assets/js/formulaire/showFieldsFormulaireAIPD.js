import $ from 'jquery';

$(document).ready(function () {
    let field = $('#formulaire_options_usePia');

    showOrHideAIPD(field.val(), $('#infoSupFieldsAIPD'));
    field.change(function () {
        showOrHideAIPD($(this).val(), $('#infoSupFieldsAIPD'));
    });
});


function showOrHideAIPD(val, field)
{
    if (val == true) {
        $(field).show();
    } else {
        $(field).hide();
    }
}