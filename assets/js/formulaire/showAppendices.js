import $ from 'jquery';

$(document).ready(function () {
    let fieldUseAllExtensionFiles = $('#formulaire_options_useAllExtensionFiles');

    displayAlertDanger(fieldUseAllExtensionFiles.val());
    fieldUseAllExtensionFiles.change(function () {
        displayAlertDanger($(this).val());
    });

});

function displayAlertDanger(val){
    if (val == true) {
        $('#infoUseAllExtension').show();
    } else {
        $('#infoUseAllExtension').hide();
    }
}