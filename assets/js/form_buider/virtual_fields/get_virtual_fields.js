import $ from "jquery";

import './fields/virtual_simple_field';
import './fields/virtual_checkbox';
import './fields/virtual_radio';
import './fields/virtual_select';
import './fields/virtual_multi_select';

window.getVirtualFields = function(idContainer, fixmeOffset) {
    // if ($(idContainer).hasClass('ui-selected')) {
    //     $(this).removeClass('ui-selected');
    //     $(this).addClass('no-drop');
    // }
    let dynamicFields = [];

    $(idContainer).find('.draggable').each(function(idxDroppable, droppable) {
        let objFieldValues = {},
            objFieldDetails = {},
            fieldType = null,
            fieldLine = null,
            fieldColumn = null;

        fieldLine = Math.round($(droppable).position().top / fixmeOffset + 1);

        let positionLeftField = $(this).position().left;
        if (positionLeftField <= 0) {
            fieldColumn = 1; // colomun de left
        } else {
            fieldColumn = 2; // colomun de right
        }

        if ($(droppable).hasClass('small-text')) {
            fieldType = 'input';
            objFieldDetails = getVirtualSimplefield($(this));

        } else if ($(droppable).hasClass('long-text')) {
            fieldType = 'textarea';
            objFieldDetails = getVirtualSimplefield($(this), fieldType);

        } else if ($(droppable).hasClass('date')) {
            fieldType = 'date';
            objFieldDetails = getVirtualSimplefield($(this));

        }  else if ($(droppable).hasClass('checkboxes')) {
            fieldType = 'checkboxes';
            objFieldDetails = getVirtualCheckbox($(this));

        } else if ($(droppable).hasClass('radios')) {
            fieldType = 'radios';
            objFieldDetails = getVirtualRadio($(this));

        } else if ($(droppable).hasClass('deroulant')) {
            fieldType = 'deroulant';
            objFieldDetails = getVirtualSelect($(this));

        } else if ($(droppable).hasClass('multi-select')) {
            fieldType = 'multi-select';
            objFieldDetails = getVirtualMultiSelect($(this));

        } else if ($(droppable).hasClass('title')) {
            fieldType = 'title';
            objFieldDetails['content'] = $(this).find('h1').html();

        } else if ($(droppable).hasClass('texte')) {
            fieldType = 'texte';
            objFieldDetails['content'] = $(this).find('h5').html();

        } else if ($(droppable).hasClass('help')) {
            fieldType = 'help';
            objFieldDetails['content'] = $(this).find('.messager').html();

        }

        // let dataVirtualConditions = $(droppable).attr('data-virtual-conditions');
        // if (dataVirtualConditions) {
        //     objFieldDetails['conditions'] = dataVirtualConditions
        // } else {
        //     objFieldDetails['conditions'] = ''
        // }

        objFieldValues = {
            'type' : fieldType,
            'ligne' : fieldLine,
            'colonne' : fieldColumn,
            'details' : JSON.stringify(objFieldDetails)
        };

        dynamicFields.push(objFieldValues);
    });

    return dynamicFields;
};