import $ from "jquery";

window.getVirtualFieldIsRequired = function(object) {
    let required = false;

    if ($(object).attr('data-virtual-required') === 'fieldRequired') {
        required = true;
    }

    return required;
}