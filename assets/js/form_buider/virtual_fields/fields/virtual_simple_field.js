import $ from "jquery";

import './get_virtual_field_is_required';

window.getVirtualSimplefield = function(object, fieldType = 'input') {
    let objField = {},
        idField = $(object).find(fieldType).attr('id');

    objField['name'] = idField;
    objField['label'] = $(object).find('label').text();
    objField['placeholder'] = $(object).find(fieldType).attr('placeholder');
    objField['default'] = $('#'+idField).val();
    objField['required'] = getVirtualFieldIsRequired(object);

    return objField;
}