import $ from "jquery";

import './get_virtual_field_is_required';

window.getVirtualMultiSelect = function(object) {
    let objField = {},
        options = [],
        valDefault = [],
        idField = $(object).find('select').attr('id');

    objField['name'] = idField;
    objField['label'] = $(object).find('label').text();
    objField['required'] = getVirtualFieldIsRequired(object);

    $(object).find('option[name="'+idField+'"]').each(function () {
        if ($(this).text()) {
            options.push($(this).text());
        }
    });
    objField['options'] = options;

    $(object).find('option[name="'+idField+'"]:checked').each(function () {
        valDefault.push(this.index);
    });
    objField['default'] = valDefault;

    return objField;
}