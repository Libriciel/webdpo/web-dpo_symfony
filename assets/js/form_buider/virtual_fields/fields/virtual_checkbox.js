import $ from "jquery";

import './get_virtual_field_is_required';

window.getVirtualCheckbox = function(object) {
    let objField = {},
        options = [];

    objField['name'] = $(object).find('div[id]').attr('id');
    objField['label'] = $(object).find('legend').text();
    objField['required'] = getVirtualFieldIsRequired(object);

    $(object).find('input[type="checkbox"]').each(function () {
        options.push($(this).next('label').text());
    });
    objField['options'] = options;

    return objField;
}