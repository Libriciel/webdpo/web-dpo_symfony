import $ from "jquery";

import './get_virtual_field_is_required';

window.getVirtualSelect = function(object) {
    let objField = {},
        options = [],
        // valDefault = '',
        idField = $(object).find('select').attr('id');

    objField['name'] = idField;
    objField['label'] = $(object).find('label').text();
    objField['required'] = getVirtualFieldIsRequired(object);

    $(object).find('option[name="'+idField+'"]').each(function () {
        if ($(this).text()) {
            options.push($(this).text());
        }
    });
    objField['options'] = options;

    // $(object).find('option[name="'+idField+'"]:checked').each(function () {
    //     if ($(this).value !== '') {
    //         valDefault = $(this).value;
    //     }
    // });
    // objField['default'] = valDefault;
    objField['default'] = $(object).find('option[name="'+idField+'"]:checked').value;

    return objField;
}