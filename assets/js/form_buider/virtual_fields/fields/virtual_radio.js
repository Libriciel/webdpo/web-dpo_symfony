import $ from "jquery";

import './get_virtual_field_is_required';

window.getVirtualRadio = function(object) {
    let objField = {},
        options = [];

    objField['name'] = $(object).find('div[id]').attr('id');
    objField['label'] = $(object).find('legend').text();
    objField['required'] = getVirtualFieldIsRequired(object);

    $(object).find('input[type="radio"]').each(function () {
        options.push($(this).next('label').text());
    });
    objField['options'] = options;

    objField['default'] = $(object).find('input[type="radio"]:checked').next('label').text();

    // $(object).find('input[type="radio"]:checked').each(function () {
    //     if ($(this).value !== '') {
    //         valDefault = $(this).text();
    //     }
    // });
    // objField['default'] = valDefault;

    return objField;
}