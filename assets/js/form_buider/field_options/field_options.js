window.fieldOptionOptions = function(
    idOptionFieldOptions,
    listOptions,
) {
    return '<div class="col-md-3">' +
        '<div class="form-group">' +
            '<label for="'+idOptionFieldOptions+'">' +
                'Liste des options (une option par ligne)' +
                '<span class="requis"> *</span>' +
            '</label>' +
            '<textarea class="form-control" id="'+idOptionFieldOptions+'">' + listOptions + '</textarea>' +
        '</div>' +
    '</div>';
};