import $ from "jquery";

import './field_title';
import './field_textarea_html';

window.groupOptionsHtmlFields = function(
    object,
    idOptionFieldHtmlContents
) {
    // Création des champs d'option en fonction des parametres
    let options = '';

    if ($(object).hasClass('title')) {
        options = '<div class="row">' +
            fieldOptionTitle(idOptionFieldHtmlContents, $(object).find('h1').html()) +
        '</div>';
    }

    if ($(object).is('.texte, .help')) {
        options = '<div class="row">' +
            fieldOptionTextareaHtml(idOptionFieldHtmlContents, $(object).text()) +
        '</div>';
    }

    return options;
};