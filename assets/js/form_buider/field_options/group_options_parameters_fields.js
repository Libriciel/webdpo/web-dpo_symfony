import $ from "jquery";

import './field_label';
import './field_name';
import './field_placeholder';
import './field_options';
import './field_required';

window.groupOptionsParametersFields = function(
    object,
    idOptionFieldVariableName,
    idOptionFieldLabel,
    idOptionFieldPlaceholder,
    idOptionFieldOptions,
    idOptionFieldRequired,
) {
    // Champ "Nom de variable" dans les options du champ
    let valueOptionFieldVariableName = '',
        disabledOptionFieldVariableName = false;

    // Champ "Nom du champ" dans les options du champ
    let valueOptionFieldLabel = $(object).find('label').text();

    // "Champ obligatoire" dans les options du champ
    let fieldRequiredChecked = '',
        disabledOptionFieldRequired = false;

    let findTypeFieldSelected = '',
        fieldPlaceholderOrOptions = '';

    if ($(object).is('.small-text, .long-text, .date')) {
        // Champ "Aide à la saisie" dans les options du champ uniquement pour les petit et grand champ texte ainsi que pour le champ date
        let valueOptionFieldPlaceholder = null,
            readonlyOptionFieldPlaceholder = '';

        if ($(object).is('.small-text, .date')) {
            findTypeFieldSelected = 'input';

            if ($(object).hasClass('date')) {
                readonlyOptionFieldPlaceholder = "readonly='readonly'";
            }
        }

        if ($(object).hasClass('long-text')) {
            findTypeFieldSelected = 'textarea';
        }

        valueOptionFieldPlaceholder = $(object).find(findTypeFieldSelected).attr('placeholder');

        fieldPlaceholderOrOptions = fieldOptionPlaceholder(
            idOptionFieldPlaceholder,
            valueOptionFieldPlaceholder,
            readonlyOptionFieldPlaceholder
        );

        valueOptionFieldVariableName = $(object).find(findTypeFieldSelected).attr('id');

    } else if ($(object).is('.checkboxes, .radios, .deroulant, .multi-select')) {
        let listOptions = '';

        if ($(object).is('.checkboxes, .radios')) {
            valueOptionFieldLabel = $(object).find('legend').text();
            findTypeFieldSelected = 'input';

            if ($(object).is('.radios')) {
                fieldRequiredChecked = 'checked="true"';
                disabledOptionFieldRequired = true;
            }

            $(object).find(findTypeFieldSelected).each(function () {
                if (listOptions === '') {
                    listOptions = listOptions + $(this).next('label').text();
                } else {
                    listOptions = listOptions + '\n' + $(this).next('label').text();
                }
            });

            valueOptionFieldVariableName = $(object).find('div').attr('id');
        }

        if ($(object).is('.deroulant, .multi-select')) {
            findTypeFieldSelected = 'select';

            $(object).find(findTypeFieldSelected).each(function () {
                if (listOptions === '') {
                    listOptions = listOptions + $(this).text();
                } else {
                    listOptions = listOptions + '\n' + $(this).text();
                }
            });

            valueOptionFieldVariableName = $(object).find(findTypeFieldSelected).attr('id');
        }

        fieldPlaceholderOrOptions = fieldOptionOptions(
            idOptionFieldOptions,
            listOptions
        );
    } else {
        throw new Error("Aucun champ");
    }

    if (valueOptionFieldVariableName.startsWith('field-generated-') === true) {
        valueOptionFieldVariableName = '';
    } else {
        disabledOptionFieldVariableName = false;
    }

    // champ obligatoire
    if ($(object).attr('data-virtual-required') === 'fieldRequired') {
        fieldRequiredChecked = 'checked="true"';
    }

    // let displayConditions = '',
    //     conditions = '';
    //
    // // Ecriture des conditions dans les options du champs
    // conditions = $(object).attr('data-virtual-conditions');
    // if (conditions) {
    //     displayConditions = writeConditionsInOption(conditions);
    // }

    // Création des champs d'option en fonction des parametres
    let options = '<div class="row">' +
        fieldOptionLabel(idOptionFieldVariableName, valueOptionFieldVariableName, disabledOptionFieldVariableName) +
        fieldOptionName(idOptionFieldLabel, valueOptionFieldLabel) +
        fieldPlaceholderOrOptions +
        fieldOptionRequired(idOptionFieldRequired, fieldRequiredChecked, disabledOptionFieldRequired) +
    '</div>';

    return options;
};