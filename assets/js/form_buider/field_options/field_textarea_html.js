window.fieldOptionTextareaHtml = function(
    idOptionField,
    valueOptionField
) {
    return '<div class="col">' +
        '<div class="form-group">' +
            '<label class="required" for="'+idOptionField+'">' +
                'Contenu' +
            '</label>' +
            '<textarea class="form-control tinymceField" id="'+idOptionField+'">' +
                valueOptionField +
            '</textarea>' +
        '</div>' +
    '</div>';
};