window.groupButtonsOptionsField = function(
    idBtnCloser,
    idBtnApplicable
) {
    return '<div class="text-center">'+
        '<button type="button" class="btn btn-outline-danger btn-sm" id="'+idBtnCloser+'">' +
            '<i class="fa fa-trash"></i>' +
        '</button>' +
        '<button type="button" class="btn btn-outline-success btn-sm" id="'+idBtnApplicable+'">' +
            '<i class="fa fa-check"></i>' +
            'Appliquer' +
        '</button>' +
    '</div>';
};