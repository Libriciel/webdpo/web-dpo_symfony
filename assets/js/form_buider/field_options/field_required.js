window.fieldOptionRequired = function(
    idOptionFieldRequired,
    fieldRequiredChecked,
    disabled = false
) {

    let classDisabled = '';
    if (disabled === true) {
        classDisabled = 'disabled="disabled"'
    }

    return '<div class="col-3">' +
        '<fieldset class="form-group">'+
            '<div class="form-check">'+
                '<input type="checkbox" id="'+idOptionFieldRequired+'" '+fieldRequiredChecked+' class="form-check-input" '+classDisabled+'>'+
                '<label class="form-check-label" for="'+idOptionFieldRequired+'">'+
                    'Champ obligatoire'+
                '</label>'+
            '</div>'+
        '</fieldset>'+
    '</div>';
};