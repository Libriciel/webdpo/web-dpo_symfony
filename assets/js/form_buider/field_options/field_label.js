window.fieldOptionLabel = function(
    champId,
    attrNameNomDeVariable,
    disabled = false
) {
    let disabledField = '';
    if (disabled === true) {
        disabledField = 'disabled=""'
    }

    return '<div class="col-3">' +
        '<div class="form-group">' +
            '<label for="'+champId+'">' +
                'Nom de variable' +
                '<span class="requis"> *</span>' +
            '</label>' +
            '<input type="text" class="form-control" id="'+champId+'" placeholder="Nom UNIQUE" value="'+attrNameNomDeVariable+'" '+disabledField+'>' +
        '</div>' +
    '</div>';
};