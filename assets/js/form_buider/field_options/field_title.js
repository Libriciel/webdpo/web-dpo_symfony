window.fieldOptionTitle = function(
    idOptionFieldTitle,
    valueOptionFieldTitle
) {
    return '<div class="col">' +
        '<div class="form-group">' +
            '<label class="required" for="'+idOptionFieldTitle+'">' +
                'Contenu' +
            '</label>' +
            '<input type="text" class="form-control" name="'+idOptionFieldTitle+'" id="'+idOptionFieldTitle+'" value="'+valueOptionFieldTitle+'">' +
        '</div>' +
    '</div>';
};