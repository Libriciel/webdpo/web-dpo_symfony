import $ from "jquery";

import './group_options_parameters_fields';
import './group_options_html_fields';
import './buttonsOptionsField';

window.groupOptionsFields = function(
    object,
    idContainer,
    idOptionFieldVariableName,
    idOptionFieldLabel,
    idOptionFieldPlaceholder,
    idOptionFieldOptions,
    idOptionFieldRequired,
    idOptionFieldHtmlContents,
    idBtnApplicable,
    idBtnCloser
) {
    let options = null;

    if ($(object).is('.small-text, .long-text, .date, .checkboxes, .radios, .deroulant, .multi-select, .title, .texte, .help')) {
        let fields = '';

        if ($(object).is('.small-text, .long-text, .date, .checkboxes, .radios, .deroulant, .multi-select')) {
            fields = groupOptionsParametersFields(
                object,
                idOptionFieldVariableName,
                idOptionFieldLabel,
                idOptionFieldPlaceholder,
                idOptionFieldOptions,
                idOptionFieldRequired
            );
        }

        if ($(object).is('.title, .texte, .help')) {
            fields = groupOptionsHtmlFields(
                object,
                idOptionFieldHtmlContents
            );
        }

        options = $('' +
            fields +
            groupButtonsOptionsField(idBtnCloser, idBtnApplicable)
        );
    }

    return options;
};