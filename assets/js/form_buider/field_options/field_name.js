window.fieldOptionName = function(
    idOptionFieldLabel,
    valueOptionFieldLabel
) {
    return '<div class="col-3">' +
        '<div class="form-group">' +
            '<label for="'+idOptionFieldLabel+'">' +
                'Nom du champ' +
                '<span class="requis"> *</span>' +
            '</label>' +
            // '<input type="text" class="form-control labelForm" id="'+idOptionFieldLabel+'" placeholder="Label du champ" value="'+$(idContainer).find('.ui-selected').find('.labeler').html()+'">' +
            '<input type="text" class="form-control labelForm" id="'+idOptionFieldLabel+'" placeholder="Label du champ" value="'+valueOptionFieldLabel+'">' +
        '</div>' +
    '</div>';
};