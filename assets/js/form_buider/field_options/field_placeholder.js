window.fieldOptionPlaceholder = function(
    idOptionFieldPlaceholder,
    attrPlaceholderAideSaisie,
    readonlyOptionFieldPlaceholder
) {
    return '<div class="col-3">' +
        '<div class="form-group">' +
            '<label for="'+idOptionFieldPlaceholder+'">' +
                'Aide à la saisie' +
            '</label>' +
            '<input type="text" class="form-control placeholderForm"'+readonlyOptionFieldPlaceholder+' id="'+idOptionFieldPlaceholder+'" value="'+attrPlaceholderAideSaisie+'">' +
        '</div>' +
    '</div>';
};