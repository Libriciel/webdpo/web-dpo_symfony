import $ from "jquery";

import './virtual_fields/get_virtual_fields';

window.saveForm = function(typeCreateForm, fixmeOffset = 45) {
    let idContainer = '#form-container-'+typeCreateForm;

    let dynamicFields = getVirtualFields(idContainer, fixmeOffset);
    console.log(dynamicFields);

    $('#formulaire_tests').val(JSON.stringify({
        typeCreateForm: dynamicFields
    }));

    // $.each(dynamicFields, function(idx, value) {
    //     $('#formulaire_tests').val(value);
    // });

    // $.each(dynamicFields, function(idx, hidden) {
    //     $('#formulaire_tests').append(hidden);
    //     // $('#formulaire_tests').append(hidden.val());
    //     // $('form').append(hidden);
    // });
};