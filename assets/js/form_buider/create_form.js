import $ from "jquery";

import './fields/input';
import './fields/textarea';
import './fields/date';
import './fields/checkbox';
import './fields/radio';
import './fields/select';
import './fields/multi_select';
import './fields/title';
import './fields/help';
import './fields/text';

import './displayDetails';

import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/droppable';
import 'jquery-ui/ui/widgets/resizable';
import './scroll';
import {French} from "flatpickr/dist/l10n/fr";
import flatpickr from "flatpickr";

window.createForm = function(typeCreateForm, fixmeOffset = 45) {
    let idContainer = '#form-container-'+typeCreateForm,
        fieldOptions = '#field-options-'+typeCreateForm,
        // idBtnApplicable = 'applicable-'+typeCreateForm,
        // idBtnCloser = 'closer-'+typeCreateForm,
        // idBtnGroup = 'btn-group-'+typeCreateForm,
        idBtnInput = 'btn-input-'+typeCreateForm;

    //-------------------------------------------------------------------------
    // let fixmeOffset = 45;
    // $.fn.extend({
    //     snapToGrid: function() {
    //         return $(this).each(function() {
    //             let size = Math.round($(this).closest('.form-container').width() / 2);
    //             $(this).draggable({
    //                 containment: 'parent',
    //                 grid: [size, fixmeOffset],
    //                 snapTolerance: fixmeOffset
    //             });
    //         });
    //     }
    // });
    //
    // $(window).resize(function() {
    //     $(idContainer)
    //         .find('.draggable')
    //         .snapToGrid();
    //
    //     // resizeFormContainer();
    // });

    //--------------------------------------------------------------------------
    let draggableVerticalOffset = function () {
        let last = null,
            lastPosition = 0,
            lastHeight = 0,
            currentPosition,
            currentHeight,
            children = $(idContainer).children('.draggable'),
            top = 0;

        $(children).each(function () {
            currentPosition = Math.round($(this).offset().top);
            currentHeight = Math.round($(this).height());

            if ((currentPosition + currentHeight) >= (lastPosition + lastHeight)) {
                last = $(this);
                lastPosition = currentPosition;
                lastHeight = currentHeight;
            }
        });

        if ((lastPosition + lastHeight) > 0) {
            top = (lastPosition + lastHeight - $(idContainer).offset().top);
        }

        return top;
    };
    //--------------------------------------------------------------------------

    /**
     * Création des éléments du formulaire lors du clic sur un bouton.
     * Pour rajouter des boutons il faut les rajouter sur l'élément HTML
     * et rajouter un case dans le switch
     */
    $('.'+idBtnInput).click(function () {
        let id = $(this).attr('id');

        $(idContainer).find('.ui-selected').addClass('no-drop');
        $(idContainer).find('.ui-selected').removeClass('ui-selected');

        let top = draggableVerticalOffset();
        let newElement = null;

        switch (id) {
            case 'btn-small-text-'+typeCreateForm:
                newElement = inputField(top);
                break;

            case 'btn-long-text-'+typeCreateForm:
                newElement = textareaField(top);
                break;

            case 'btn-date-'+typeCreateForm:
                newElement = dateField(top);
                break;

            case 'btn-checkbox-'+typeCreateForm:
                newElement = checkboxField(top);
                break;

            case 'btn-radio-'+typeCreateForm:
                newElement = radioField(top);
                break;

            case 'btn-deroulant-'+typeCreateForm:
                newElement = selectField(top);
                break;

            case 'btn-multi-select-'+typeCreateForm:
                newElement = multiSelectField(top);
                break;

            case 'btn-title-'+typeCreateForm:
                newElement = titleField(top);
                break;

            case 'btn-help-'+typeCreateForm:
                newElement = helpField(top);
                break;

            case 'btn-texte-'+typeCreateForm:
                newElement = textField(top);
                break;

            default:
                break;
        }

        $(idContainer).append(newElement);

        // hideDetails();
        // displayDetails($(idContainer).find('.ui-selected'));
        // refresh();
        // resizeFormContainer();

        // repositionFieldOnGrid(newElement);
        // $(newElement).snapToGrid();

        hideDetails(fieldOptions);
        displayDetails($(idContainer).find('.ui-selected'), idContainer, fieldOptions, typeCreateForm);
        refresh();

        let config = {
            enableTime: false,
            dateFormat: "d/m/Y",
            "locale": French,
            weekNumbers: true,
        };
        flatpickr($('input[type=datetime-local]'), config);

        $('.select2-multi').select2({
            width: '100%',
            allowClear: true,
            multiple: true,
            placeholder: "Sélectionnez une ou plusieurs valeurs",
            language: "fr-FR"
        });
    });

    refresh();

    function refresh() {
        let size = Math.round($(idContainer).width() / 2);
        let draggableClass = $(idContainer).find('div.draggable.ui-selected');

        draggableClass.unbind('click');
        $(idContainer).unbind('click');


        $(draggableClass).draggable({
            containment: "parent",
            opacity: 0.70,
            // grid: [size, 50],
            grid: [size, fixmeOffset],
            // snapTolerance: 50,
            cancel: '.no-drop',
        });

        draggableClass.click(function () {
            hideDetails(fieldOptions);

            if ($(this).hasClass('ui-selected')) {
                $(this).removeClass('ui-selected');
                $(this).addClass('no-drop');
            } else {
                $(idContainer).find('.ui-selected').addClass('no-drop').removeClass('ui-selected');
                $(this).addClass('ui-selected');
                $(this).removeClass('no-drop');
                displayDetails($(this), idContainer, fieldOptions, typeCreateForm);
            }
        });
    }
};