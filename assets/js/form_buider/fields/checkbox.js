import $ from "jquery";

window.checkboxField = function(
    top = 0,
    // id = 'checkbox-field-' + Math.random(),
    id = 'field-generated-' + Math.random(),
    label = 'Cases à cocher',
    required = false
) {
    let newElement = $(
        '<div class="col-6 draggable checkboxes ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<fieldset class="form-group">'+
                '<legend class="col-form-label">'+
                    label+
                '</legend>'+
                '<div id="'+id+'">'+
                    '<div class="form-check">'+
                        '<input type="checkbox" id="checkbox_no_option" class="form-check-input">'+
                            '<label class="form-check-label" for="checkbox_no_option">'+
                                'Aucune option'+
                            '</label>'+
                    '</div>'+
                '</div>'+
            '</fieldset>'+
        '</div>'
    );

    return newElement;
};