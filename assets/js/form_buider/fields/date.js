import $ from "jquery";

window.dateField = function(
    top = 0,
    // id = 'date-field-' + Math.random(),
    id = 'field-generated-' + Math.random(),
    label = 'Champ date',
    required = false
) {
    let newElement = $(
        '<div class="col-6 draggable date ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<div class="form-group">'+
                '<label for="'+id+'" class="control-label">'+
                    label+
                '</label>'+
            '<input type="datetime-local" id="'+id+'" class="form-control" placeholder="jj/mm/aaaa">'+
            '</div>'+
        '</div>'
    );

    return newElement;
};