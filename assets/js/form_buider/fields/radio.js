import $ from "jquery";

window.radioField = function(
    top = 0,
    // id = 'radio-field-' + Math.random(),
    id = 'field-generated-' + Math.random(),
    label = 'Choix unique',
    required = false
) {
    let newElement = $(
        '<div class="col-6 draggable radios ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<fieldset class="form-group">'+
                '<legend class="col-form-label">' +
                    label +
                    '<span class="requis"> *</span>' +
                '</legend>'+
                '<div id="'+id+'">'+
                    '<div class="form-check">'+
                        '<input type="radio" id="radio_no_option" class="form-check-input">'+
                        '<label class="form-check-label" for="radio_no_option">'+
                            'Aucune option'+
                        '</label>'+
                    '</div>'+
                '</div>'+
            '</fieldset>'+
        '</div>'
    );

    return newElement;
};