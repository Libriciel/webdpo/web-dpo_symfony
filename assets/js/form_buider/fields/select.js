import $ from "jquery";

window.selectField = function(
    top = 0,
    // id = 'select-field-' + Math.random(),
    id = 'field-generated-' + Math.random(),
    label = 'Menu déroulant',
    required = false
) {
    let newElement = $(
        '<div class="col-6 draggable deroulant ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<div class="form-group">'+
                '<label for="'+id+'" class="control-label">'+
                    label+
                '</label>'+
                '<select id="'+id+'" class="form-control">'+
                    '<option value="">' +
                        'Aucune option' +
                    '</option>'+
                '</select>'+
            '</div>'+
        '</div>'
    );

    return newElement;
};