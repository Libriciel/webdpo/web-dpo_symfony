import $ from "jquery";

window.textareaField = function(
    top = 0,
    // id = 'textarea-field-' + Math.random(),
    id = 'field-generated-' + Math.random(),
    label = 'Grand champ texte',
    placeholder = 'Aide à la saisie',
    required = false
) {
    let newElement = $(
        // '<div class="col-6 draggable long-text ui-selected" style="left : 0px; top : '+top+'px;">'+
        '<div class="col-6 draggable long-text ui-selected">'+
            '<div class="form-group">'+
                '<label for="'+id+'" class="control-label">'+
                    label+
                '</label>'+
            '<textarea id="'+id+'" class="form-control" placeholder="'+placeholder+'"></textarea>'+
            '</div>'+
        '</div>'
    );

    return newElement;
};