import $ from "jquery";

window.helpField = function(
    top = 0,
    label = 'Champ d\'information',
) {
    let newElement = $(
        '<div class="col-6 draggable help ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<div class="form-group">' +
                '<div class="alert alert-info">' +
                    '<div class="text-center">' +
                        '<i class="fa fa-info-circle fa-2x"></i>' +
                    '</div>' +
                    '<div class="messager">' +
                        label +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    );

    return newElement;
};