import $ from "jquery";

window.inputField = function(
    top = 0,
    // id = 'input-field-' + Math.random(),
    id = 'field-generated-' + Math.random(),
    label = 'Petit champ texte',
    placeholder = 'Aide à la saisie',
    required = false
) {
    let classRequired = '';
    if (required === true) {
        classRequired = 'required';
    }

    let newElement = $(
        '<div class="col-6 draggable small-text ui-selected" style="left : 0px; top : '+top+'px; opacity: 1;">'+
        // '<div class="col-6 draggable small-text ui-selected">'+
            '<div class="form-group">'+
                '<label for="'+id+'" class="control-label '+classRequired+'">'+
                    label+
                '</label>'+
                '<input id="'+id+'" type="text" class="form-control" placeholder="'+placeholder+'">'+
            '</div>'+
        '</div>'
    );

    return newElement;
};