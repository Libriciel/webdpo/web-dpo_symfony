import $ from "jquery";

window.titleField = function(
    top = 0,
    label = 'Titre de catégorie',
) {
    let newElement = $(
        '<div class="col-6 draggable title text-center ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<div class="form-group">'+
                '<h1>' +
                    label +
                '</h1>' +
            '</div>'+
        '</div>'
    );

    return newElement;
};