import $ from "jquery";

window.multiSelectField = function(
    top = 0,
    // id = 'multi-select-field-' + Math.random(),
    id = 'field-generated-' + Math.random(),
    label = 'Menu multi-select',
    required = false
) {
    let newElement = $(
        '<div class="col-6 draggable multi-select ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<div class="form-group">'+
                '<label for="'+id+'" class="control-label">'+
                    label +
                '</label>'+
                '<select id="'+id+'" class="form-control select2-multi">'+
                    '<option value="">' +
                        'Aucune option' +
                    '</option>'+
                '</select>'+
            '</div>'+
        '</div>'
    );

    return newElement;
};