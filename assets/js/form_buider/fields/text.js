import $ from "jquery";

window.textField = function(
    top = 0,
    label = 'Votre texte',
) {
    let newElement = $(
        '<div class="col-6 draggable texte ui-selected" style="left : 0px; top : '+top+'px;">'+
            '<div class="form-group">'+
                label +
            '</div>'+
        '</div>'
    );

    return newElement;
};