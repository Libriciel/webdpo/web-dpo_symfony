import $ from "jquery";

import './field_options/group_options_fields'
import './apply_options_field_on_field'

/**
 * Options du champ
 *
 * Affiche le formulaire avec les détails à remplir
 * sur chaque champ ajouté lors de la séléction
 *
 * Définition du nom de variable, nom du champ, aide à la saisie,
 * options du champ, champ obligatoire
 *
 * @param {type} object
 * @param idContainer
 * @param fieldOptions
 * @param typeCreateForm
 * @returns {undefined}
 */
window.displayDetails = function(object, idContainer, fieldOptions, typeCreateForm)
{
    let idOptionFieldVariableName = 'field-option-name-'+typeCreateForm,
        idOptionFieldLabel = 'field-option-label-'+typeCreateForm,
        idOptionFieldPlaceholder = 'field-option-placeholder-'+typeCreateForm,
        idOptionFieldOptions = 'field-option-options-'+typeCreateForm,
        idOptionFieldRequired = 'field-option-required-'+typeCreateForm,
        idOptionFieldHtmlContents = 'field-option-html-contents-'+typeCreateForm,
        idBtnApplicable = 'applicable-'+typeCreateForm,
        idBtnCloser = 'closer-'+typeCreateForm,
        options = '';

    options = groupOptionsFields(
        object,
        idContainer,
        idOptionFieldVariableName,
        idOptionFieldLabel,
        idOptionFieldPlaceholder,
        idOptionFieldOptions,
        idOptionFieldRequired,
        idOptionFieldHtmlContents,
        idBtnApplicable,
        idBtnCloser
    );

    // Afficher les options en fontion du champ en question
    $(fieldOptions).append(options);

    $('#' + idOptionFieldVariableName).keyup(function () {
        let value = $(this).val().toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '_').replace(/[^a-zA-Z 0-9_]+/g, '');
        $(this).val(value);
    });

    // On applique les modifications du champ en question au clic sur le boutton "Appliquer"
    $('#'+idBtnApplicable).click(function () {
        let fieldId = $(object).find('[id]').attr('id');
        let conditionsTextuelle = '';

        // $(idContainer).find('[data-virtual-conditions]').not(".ui-selected").each(function (k, dataVitualContions) {
        //     let conditionsJson = $(dataVitualContions).attr('data-virtual-conditions');
        //     if (conditionsJson) {
        //         conditions = JSON.parse(conditionsJson);
        //         if (typeof conditions == 'object') {
        //             $.each(conditions, function (uuid, condition) {
        //                 if (fieldId === condition['ifTheField']) {
        //                     conditionsTextuelle = conditionsTextuelle + writeConditionsInOption(conditionsJson, false);
        //                 }
        //             });
        //         }
        //     }
        // });

        if (conditionsTextuelle !== '') {
            // $('#allConditionsDelete').html(conditionsTextuelle);
            //
            // $('#modalWarningDeleteConditions').modal('show');
            //
            // // Si on click sur "Supprimer"
            // $('#deleteCondition').click(function () {
            //     // Suppression des conditions en référence avec le champ qui est modifier
            //     // On supprimer les conditions sur les autres champs si le champ en question fait partie de la condition
            //     $(idContainer).find('[data-virtual-conditions]').not(".ui-selected").each(function (k, dataVitualContions) {
            //         let conditionsJson = $(dataVitualContions).attr('data-virtual-conditions');
            //         if (conditionsJson) {
            //             conditions = JSON.parse(conditionsJson);
            //             if (typeof conditions == 'object') {
            //                 $.each(conditions, function (uuid, condition) {
            //                     if (fieldId === condition['ifTheField']) {
            //                         delete conditions[uuid];
            //                     }
            //                 });
            //
            //                 $(dataVitualContions).attr('data-virtual-conditions', '');
            //                 $(dataVitualContions).attr('data-virtual-conditions', JSON.stringify(conditions));
            //             }
            //         }
            //     });
            //
            //     applyOptionsFieldOnField();
            //
            //     $('#modalWarningDeleteConditions').modal('hide');
            // });
            //
            // // Si on click sur "Annuler"
            // $('#modalWarningDeleteConditions').on('hidden.bs.modal', function () {
            //     $('#modalWarningDeleteConditions').modal('hide');
            //
            //     $(idContainer).find('.ui-selected').removeClass('ui-selected');
            //     hideDetails(fieldOptions);
            // });
        } else {
            applyOptionsFieldOnField(
                object,
                idContainer,
                typeCreateForm,
                idOptionFieldVariableName,
                idOptionFieldLabel,
                idOptionFieldPlaceholder,
                idOptionFieldHtmlContents,
                idOptionFieldOptions,
                idOptionFieldRequired
            );
        }
    });

    // On supprime le champ en question au clic sur le bouton "Poubelle"
    $('#'+idBtnCloser).click(function () {
        // let fieldRemoveId = $(idContainer).find('.ui-selected').find('[id]').attr('id');
        //
        // On supprimer les conditions sur les autres champs si le champ en question fait partie de la condition
        // $(idContainer).find('[data-virtual-conditions]').not(".ui-selected").each(function (k, dataVitualContions) {
        //     let conditionsJson = $(dataVitualContions).attr('data-virtual-conditions');
        //     if (conditionsJson) {
        //         conditions = JSON.parse(conditionsJson);
        //         if (typeof conditions == 'object') {
        //             $.each(conditions, function (uuid, condition) {
        //                 $.each(condition, function (key, value) {
        //                     if ($.inArray(key, ['ifTheField', 'thenTheField']) !== -1) {
        //                         if (fieldRemoveId === value) {
        //                             delete conditions[uuid];
        //                         }
        //                     }
        //                 });
        //             });
        //
        //             $(dataVitualContions).attr('data-virtual-conditions', '');
        //             $(dataVitualContions).attr('data-virtual-conditions', JSON.stringify(conditions));
        //         }
        //     }
        // });

        $(object).remove();
        hideDetails(fieldOptions);
    });

    // On supprime la condition crée sur le champ
    $('.removeCondition').click(function () {
        let condition_id = $(this).attr('id');

        let conditions = JSON.parse($(idContainer).find('.ui-selected').attr('data-virtual-conditions'));
        if (conditions) {
            $.each(conditions, function (key) {
                if (condition_id === key) {
                    delete conditions[key];
                    return false;
                }
            });
        }

        $(idContainer).find('.ui-selected').attr('data-virtual-conditions', JSON.stringify(conditions));
        $('#'+condition_id).parent().remove();
    });
}

window.hideDetails = function (fieldOptions)
{
    $(fieldOptions+">div").remove();
}