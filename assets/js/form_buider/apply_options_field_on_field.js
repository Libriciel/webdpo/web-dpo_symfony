import $ from "jquery";

/**
 * On vérifie et on applique les "Options du champ" sur le champ
 */
window.applyOptionsFieldOnField = function(
    object,
    idContainer,
    typeCreateForm,
    idOptionFieldVariableName,
    idOptionFieldLabel,
    idOptionFieldPlaceholder,
    idOptionFieldHtmlContents,
    idOptionFieldOptions,
    idOptionFieldRequired
){
    let findTypeFieldSelected = '';

    if ($(object).is('.small-text, .long-text, .date, .checkboxes, .radios, .deroulant, .multi-select')) {
        let valueFieldOptionName = $('#'+idOptionFieldVariableName).val();
        let valueFieldOptionLabel = $('#'+idOptionFieldLabel).val();
        let valueFieldOptionPlaceholder = '';
        let valueFieldOptionOptions = '';
        let valueFieldOptionRequired = $('#'+idOptionFieldRequired).is(":checked");

        try {
            checkNewNameField(valueFieldOptionName, idContainer);

            checkNewLabelField(valueFieldOptionLabel);

            if ($(object).is('.small-text, .long-text, .date')) {
                valueFieldOptionPlaceholder = $('#'+idOptionFieldPlaceholder).val();

                if ($(object).is('.small-text, .date')) {
                    findTypeFieldSelected = 'input';
                }

                if ($(object).hasClass('long-text')) {
                    findTypeFieldSelected = 'textarea';
                }

                $(object).each(function (){
                    $(this).find(findTypeFieldSelected).attr('id', valueFieldOptionName);
                    $(this).find(findTypeFieldSelected).attr('placeholder', valueFieldOptionPlaceholder);

                    $(this).find('label').attr('for', valueFieldOptionName);
                    $(this).find('label').text(valueFieldOptionLabel);

                    setFieldIfRequired($(this), valueFieldOptionRequired);
                })
            }

            if ($(object).is('.checkboxes, .radios, .deroulant, .multi-select')) {
                valueFieldOptionOptions = $('#'+idOptionFieldOptions).val().split('\n');
                checkNewOptionsField(valueFieldOptionOptions);

                if ($(object).is('.checkboxes, .radios')) {
                    let typeInput = 'checkbox';
                    if ($(object).hasClass('radios')) {
                       typeInput = 'radio';
                    }

                    $(object).each(function () {
                        $(this).find('div[id]').attr('id', valueFieldOptionName);
                        $(this).find('legend').text(valueFieldOptionLabel);

                        let objet = '';
                        $.each(valueFieldOptionOptions, function (index, option) {
                            objet = objet + '<div class="form-check">' +
                                '<input class="form-check-input" type="'+typeInput+'" name="'+valueFieldOptionName+'" value="" id="'+valueFieldOptionName+index+'">' +
                                '<label class="form-check-label" for="'+valueFieldOptionName+index+'">' +
                                    option +
                                '</label>' +
                            '</div>';
                        });
                        $(this).find('#'+valueFieldOptionName).html($(objet));

                        setFieldIfRequired($(this), valueFieldOptionRequired, 'legend');
                    });
                }

                if ($(object).is('.deroulant, .multi-select')) {
                    findTypeFieldSelected = 'select';

                    let objet = '';
                    if ($(object).is('.deroulant')) {
                        objet = '<option name="'+valueFieldOptionName+'" value=""></option>';
                    }

                    $(object).each(function () {
                        $(this).find(findTypeFieldSelected).attr('id', valueFieldOptionName);
                        $(this).find('label').attr('for', valueFieldOptionName);
                        $(this).find('label').text(valueFieldOptionLabel);

                        $.each(valueFieldOptionOptions, function (index, option) {
                            objet = objet + '<option id="'+valueFieldOptionName+index+'" name="'+valueFieldOptionName+'" value="'+index+'">' +
                                option +
                            '</option>';
                        });
                        $(this).find('#'+valueFieldOptionName).html($(objet));

                        setFieldIfRequired($(this), valueFieldOptionRequired);
                    });
                }
            }

            $('#'+idOptionFieldVariableName).prop("disabled", true);

        } catch (e) {
            alert(e);
            return false;
        }

    } else if ($(object).is('.title, .texte, .help')) {
        try {
            let valueFieldOptionHtmlContents =  $('#'+idOptionFieldHtmlContents).val();
            checkNewContenuField(valueFieldOptionHtmlContents);

            // Attribution les valeurs au champ "Titre de catégorie"
            if ($(object).hasClass('title')) {
                $(object).find('h1').html(valueFieldOptionHtmlContents);
            }

            // Attribution les valeurs au champ "Label"
            if ($(object).hasClass('texte')) {
                // let breakTag = (newContenuField || typeof newContenuField === 'undefined') ? '<br ' + '/>' : '<br>'
                // let text = (newContenuField + '').replace(/(\r\n|\n\r|\r|\n)/g, breakTag + '$1');
                $(object).html(valueFieldOptionHtmlContents);
            }

            // Attribution les valeurs au champ "Champ d'information"
            if ($(object).hasClass('help')) {
                $(object).find('.messager').html(valueFieldOptionHtmlContents);
            }

        } catch (e) {
            alert(e);
            return false;
        }
    }
}

function checkNewNameField(nameField, idContainer)
{
    // On vérifie que le "name" du champ n'est pas vide
    if (!nameField) {
        throw new Error("Le nom de la variable est vide !");
        // alert("Le nom de la variable est vide !");
        // event.preventDefault();
        // return false;
    }

    // On vérifie que le "name" du champ ne soit pas égale à "undefined"
    if (nameField === 'undefined') {
        throw new Error("Le nom de la variable est ne peut pas être 'undefined' !");
    }

    // On vérifie que le "name" du champ n'a pas de caractère spéciaux, espace, tiré
    let transformNameField = nameField.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/\s/g, '_').replace(/[^a-zA-Z 0-9_]+/g, '');
    if (nameField !== transformNameField) {
        throw new Error("Le nom de la variable ne respecte pas les règles de nommage.\nIl est interdit d'utiliser des majuscules, des caractères spéciaux (*,%,$,/,etc.), des espaces, des tirets (-)");
    }

    // On vérifie dans le "container" qu'il n'héxiste pas un autre champ avec le même "name"
    let candidates = $(idContainer+' *[name=' + nameField + ']');
    let number = $(candidates).length;

    $(candidates).each(function (idx, candidate) {
        if ($(candidate).closest('.ui-selected').length > 0) {
            number--;
        }
    });

    if (number > 0) {
        throw new Error('Un champ possède déjà ce nom de variable !');
    }

    if ($('#'+nameField).filter(function () {return $(this).closest('.ui-selected').length === 0; }).length !== 0) {
        throw new Error('Un champ possède déjà ce nom de variable !');
    }
}

function checkNewLabelField(labelField)
{
    if (!labelField) {
        throw new Error("Le nom du champ ne peut pas être vide !");
    }
}

function setFieldIfRequired(element, required, tag = 'label')
{
    $(element).find(tag).removeClass('required');
    $(element).attr('data-virtual-required', 'fieldNotRequired');

    if (required === true) {
        $(element).find(tag).addClass('required');
        $(element).attr('data-virtual-required', 'fieldRequired');
    }

    // $(element).find(tag).find('span.requis').remove();
    // $(element).attr('data-virtual-required', 'fieldNotRequired');
    //
    // if (required === true) {
    //     $(element).find(tag).append('<span class="requis"> *</span>');
    //     $(element).attr('data-virtual-required', 'fieldRequired');
    // }
}

function checkNewOptionsField(optionsField)
{
    $.each(optionsField, function (key, option) {
        if (!option) {
            throw new Error("Le nom du champ ne peut pas être vide !");
        }
    });
}

function checkNewContenuField(contenuField)
{
    if (!contenuField) {
        throw new Error("Le contenu du champ ne peut pas être vide !");
    }
}