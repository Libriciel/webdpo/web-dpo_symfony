import $ from 'jquery';

document.addEventListener('DOMContentLoaded', function() {
    let idBtnActiver = $('.js-btn-activer').data('idbtnactiver'),
        idFormActiver = $('.js-btn-activer').data('idformactiver');

    $('.js-checkbox').click(function () {
        let userIdActiver = $(this).prop('value'),
            valueToggleActiver = $(this).attr('data-' + idBtnActiver.substr(idBtnActiver.indexOf("_") + 1));

        if (this.checked) {
            if (valueToggleActiver === 'true') {
                valueToggleActiver = 0;
            } else {
                valueToggleActiver = 1;
            }

            let hidden = '<input type="hidden" name="toggle[' + userIdActiver + ']" value="' + valueToggleActiver + '" />';
            $("#" + idFormActiver).append(hidden);
        } else {
            $("#" + idFormActiver).find('input[name="toggle['+userIdActiver+']"]').remove();
        }
    });

    $("#allCheckbox").change(function () {
        $(".allCheckbox").not(':disabled').prop('checked', $(this).prop('checked'));

        if ($(this).is(':checked')) {
            $('.allCheckbox').each(function () {
                let userIdActiver = $(this).prop('value'),
                    valueToggleActiver = $(this).attr('data-' + idBtnActiver.substr(idBtnActiver.indexOf("_") + 1));

                if (valueToggleActiver === 'true') {
                    valueToggleActiver = 0;
                } else {
                    valueToggleActiver = 1;
                }

                let hidden = '<input type="hidden" name="toggle[' + userIdActiver + ']" value="' + valueToggleActiver + '" />';
                $("#" + idFormActiver).append(hidden);
            });
        } else {
            $('.allCheckbox').each(function () {
                let userIdActiver = $(this).prop('value');
                $("#" + idFormActiver).find('input[name="toggle['+userIdActiver+']"]').remove();
            });
        }
    });
});
