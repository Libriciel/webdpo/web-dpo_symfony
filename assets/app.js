import './styles/app.css';

import $ from 'jquery';
import 'bootstrap';
import 'select2';
import 'jquery-mask-plugin';
import './js/format';
import './ls-sidebar/ls-sidebar';
import './js/ls-file';
import flatpickr from "flatpickr";
import {French} from "flatpickr/dist/l10n/fr"

// start the Stimulus application
// import './bootstrap';

$('document').ready(function () {
    let config = {
        enableTime: false,
        // altFormat: "d/m/y",
        dateFormat: "d/m/Y",
        "locale": French,
    };
    flatpickr($('input[type=datetime-local]'), config);

    // setTimeout(function(){
    //     $("div.alert").remove();
    // }, 3000 ); // 3 secs

    // Mask champs
    $('.maskPhone').mask("00 00 00 00 00", {placeholder: "__ __ __ __ __"});

    $('.maskSiren').mask("000 000 000", {placeholder: "___ ___ ___"});

    $('.maskSiret').mask("000 000 000 00000", {placeholder: "___ ___ ___ _____"});

    $('.maskApe').mask("AAAAA", {placeholder: "_____"});

    $('.select2').select2({
        width: '100%',
        allowClear: true,
        placeholder: "Sélectionnez une valeur",
        language: "fr-FR"
    });

    const addSelectAll = matches => {
        if (matches.length > 0) {
            let data = [{
                id: 'selectAll',
                text: 'Sélectionnez toutes les valeurs',
                matchIds: matches.map(match => match.id)
            }];

            $.each(matches, function (index, value) {
                data.push(value);
            });

            return data;
        }
    };

    const handleSelection = event => {
        if (event.params.data.id === 'selectAll') {
            $('.select2-multi').val(event.params.data.matchIds);
            $('.select2-multi').trigger('change');
        }
    };

    $('.select2-multi').select2({
        width: '100%',
        allowClear: true,
        multiple: true,
        sorter: addSelectAll,
        placeholder: "Sélectionnez une ou plusieurs valeurs",
        language: "fr-FR"
    });
    $('.select2-multi').on('select2:select', handleSelection);

    $('#btn_change_organisation').click(function (){
        $.get( "/organisation/change", function( data ) {
            $( "#form_select_organisation" ).html( data );
        });
    });
});

global.$ = $;
