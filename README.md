# Application web-DPO v3.0.0
[![Minimum PHP Version](https://img.shields.io/badge/php-7.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/licence-AGPL%20v3-blue.svg)](http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)

## Présentation
Vous êtes en présence des sources de l'application **web-DPO** version 3.0.0

Avant toute nouvelle installation, veuillez faire une sauvegarde de la base de données ainsi que la version actuelle des
sources.

Assurez-vous que l'activité sur la plate-forme est réduite au minimum en programmant une interruption de service et
veuillez arrêter les tâches planifiées.

## Nouvelle installation
Veuillez suivre les indications décrites dans la documentation d'installation :

### Pré-requis
- Symfony 6.2.X
- PHP 8.1
- PostgrSQL 14

### Support
Les produits édités par **Libriciel** sont libres sous licence "AGPL v3".

Pour bénéficier du **support** de nos services pour l'installation et/ou la configuration du produit, ou pour bénéficier
de la **maintenance**, merci de contacter un commercial pour ouvrir un compte.

