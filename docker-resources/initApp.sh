#!/bin/bash

echo "Create database with symfony"
./bin/console doctrine:database:create --no-interaction

echo "Migrate database with symfony"
./bin/console doctrine:migrations:migrate --no-interaction

echo "Init database with symfony"
./bin/console initBdd docker-resources/minimum.sql

echo "Clear cache with symfony"
./bin/console cache:clear --no-interaction

/usr/sbin/php-fpm8.1 -F