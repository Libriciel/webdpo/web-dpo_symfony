FROM ubuntu:22.04 as webdpo_fpm

ARG TIMEZONE=UTC
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo $TIMEZONE > /etc/timezone

ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="0" \
    PHP_OPCACHE_MAX_ACCELERATED_FILES="10000" \
    PHP_OPCACHE_MEMORY_CONSUMPTION="192" \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE="10" \
    PHP_MAX_FILE_UPLOADS='600' \
    PHP_UPLOAD_MAX_FILESIZE='200M' \
    PHP_POST_MAX_SIZE='2000M' \
    PHP_MEMORY_LIMIT='2100M' \
    PHP_MAX_INPUT_VAR='5000'  \
    PHP_MAX_EXECUTION_TIME='300' \
    PHP_PM_MAX_CHILDREN='15' \
    PHP_PM_START_SERVERS='3' \
    PHP_PM_MIN_SPARE_SERVERS='2' \
    PHP_PM_MAX_SPARE_SERVERS='4'

RUN apt update -yqq \
    && apt install \
        wget \
        sudo \
        vim \
        curl \
        git \
        zip \
        unzip \
        locales \
        netcat \
        openssl \
        xfonts-75dpi \
        fontconfig \
        xfonts-base \
        poppler-utils \
        qpdf \
        -yqq

RUN apt install php-fpm php-intl php-mbstring php-xml php-zip php-pgsql  -y

RUN apt install postgresql-client-14 -yy

RUN curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - &&\
    sudo apt-get install -y nodejs

# webdpo sources
COPY --chown=www-data:www-data ./ /var/www/html/web-dpo/

WORKDIR /var/www/html/web-dpo

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

RUN cd /var/www/html/web-dpo && \
    composer install --no-cache --no-scripts --no-dev && \
    npm install && \
    npm run build

RUN sed -i "s|/run/php/php8.1-fpm.sock|9000|g" /etc/php/8.1/fpm/pool.d/www.conf
RUN sed -i "s|;clear_env = no|clear_env = no|g" /etc/php/8.1/fpm/pool.d/www.conf

RUN mkdir -p /data/certif_horo
RUN chown -R www-data:www-data /data

COPY ./docker-resources/zz-webdpo.conf /usr/local/etc/php-fpm.d/zz-webdpo.conf
COPY docker-resources/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY docker-resources/zz-php.ini /usr/local/etc/php/conf.d/zz-php.ini

EXPOSE 9000

FROM webdpo_fpm as webdpo_fpm_dev
RUN cd /var/www/html/web-dpo && \
    composer install --no-cache && \
    npm install && \
    npm run dev