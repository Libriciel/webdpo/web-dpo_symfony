<?php

namespace App\Repository;

use App\Entity\Organisation;
use App\Entity\Soustraitant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Soustraitant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Soustraitant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Soustraitant[]    findAll()
 * @method Soustraitant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SoustraitantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Soustraitant::class);
    }

    public function findSoustraitantsAndRepresentant(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.representant', 'r')
            ->leftJoin('s.delegue', 'd')
            ->addSelect('s')
            ->addSelect('r')
            ->addSelect('d');

        return $qb;
    }

    public function getAllSoustraitantsForCurrentOrganisation(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s')
            ->join('s.organisations', 'o')
            ->andWhere('o = :organisation')
            ->setParameter(':organisation', $organisation);

        return $qb;
    }
}
