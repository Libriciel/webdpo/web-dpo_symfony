<?php

namespace App\Repository;

use App\Entity\Organisation;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Organisation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Organisation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Organisation[]    findAll()
 * @method Organisation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganisationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Organisation::class);
    }

    private function _findOrganisationByUser(User $user): QueryBuilder
    {
        $subQuery = $this->createQueryBuilder('td1')
            ->leftJoin('td1.organisationUserRoles', 'our')
            ->andWhere('our.user = :user');

        return $this->createQueryBuilder('o')
            ->andWhere('o.id IN (' . $subQuery->getDQL() . ')')
            ->setParameter(':user', $user->getId())
            ->orderBy('o.raisonsociale');
    }

    public function findOrganisationByUser(User $user): array
    {
        $qb = $this->_findOrganisationByUser($user);

        return $qb->getQuery()->getResult();
    }

    public function findOrganisationByUserQB(User $user): QueryBuilder
    {
        if ($user->getIsSuperadmin() === false) {
            $qb = $this->_findOrganisationByUser($user);
        } else {
            $qb = $this->createQueryBuilder('o')
                ->orderBy('o.raisonsociale');
        }

        return $qb;
    }

    public function findAllStructuresByUser(User $user): array
    {
        $qb = $this->createQueryBuilder('o')
            ->innerJoin('o.organisationUserRoles', 'our')
            ->andWhere('our.user = :user')
            ->setParameter('user', $user)
            ->orderBy('o.raisonsociale', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }
}
