<?php

namespace App\Repository;

use App\Entity\Formulaire;
use App\Entity\Organisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Formulaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Formulaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Formulaire[]    findAll()
 * @method Formulaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormulaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Formulaire::class);
    }

    public function findAllFormulaires(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('f')
//            ->leftJoin('f.options', 'o')
//            ->addSelect('f')
//            ->addSelect('o')
        ;

        return $qb;
    }

    public function getAllFormulairesForCurrentOrganisation(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('f')
            ->join('f.organisations', 'o')
            ->andWhere('o = :organisation')
            ->setParameter(':organisation', $organisation);

        return $qb;
    }
}
