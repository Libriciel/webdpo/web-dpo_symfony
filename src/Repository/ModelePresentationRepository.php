<?php

namespace App\Repository;

use App\Entity\ModelePresentation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModelePresentation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModelePresentation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModelePresentation[]    findAll()
 * @method ModelePresentation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModelePresentationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModelePresentation::class);
    }
}
