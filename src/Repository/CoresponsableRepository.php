<?php

namespace App\Repository;

use App\Entity\Coresponsable;
use App\Entity\Organisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Coresponsable|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coresponsable|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coresponsable[]    findAll()
 * @method Coresponsable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoresponsableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Coresponsable::class);
    }

    public function findCoresponsables(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.representant', 'r')
            ->leftJoin('c.delegue', 'd')
            ->addSelect('c')
            ->addSelect('r')
            ->addSelect('d');

        return $qb;
    }

    public function getAllCoresponsablesForCurrentOrganisation(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.representant', 'r')
            ->leftJoin('c.delegue', 'd')
            ->join('c.organisations', 'o')
            ->andWhere('o = :organisation')
            ->setParameter(':organisation', $organisation)
            ->addSelect('r')
            ->addSelect('d');

        return $qb;
    }
}
