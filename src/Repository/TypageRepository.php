<?php

namespace App\Repository;

use App\Entity\Organisation;
use App\Entity\Typage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Typage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Typage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Typage[]    findAll()
 * @method Typage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Typage::class);
    }

    public function getAllTypages(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('t');

        return $qb;
    }

    public function getAllTypagesForCurrentOrganisation(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('t')
            ->join('t.organisations', 'o')
            ->andWhere('o = :organisation')
            ->setParameter(':organisation', $organisation);

        return $qb;
    }
}
