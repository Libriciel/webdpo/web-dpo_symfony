<?php

namespace App\Repository;

use App\Entity\Organisation;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAllSuperadmin(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.isSuperadmin = true');

        return $qb;
    }

    public function findAllUser(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.isSuperadmin = false');

        return $qb;
    }

    public function findAllUsersForAuthorizedOrganisations(array $organisation)
    {
        $subquery = $this->createQueryBuilder('td2')
            ->leftJoin('td2.organisationUserRoles', 'organisationuserrole')
            ->andWhere('organisationuserrole.organisation IN ( :organisation )');

        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.id IN (' . $subquery->getDQL() . ')')
            ->andWhere('u.isSuperadmin = false')
            ->setParameter(':organisation', $organisation)
        ;

        return $qb;
    }

    public function findAllUsersForOrganisation(Organisation $organisation)
    {
        $subquery = $this->createQueryBuilder('td2')
            ->leftJoin('td2.organisationUserRoles', 'organisationuserrole')
            ->andWhere('organisationuserrole.organisation = :organisation');

        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.id IN (' . $subquery->getDQL() . ')')
            ->andWhere('u.isSuperadmin = false')
            ->setParameter(':organisation', $organisation)
        ;

        return $qb;
    }

    public function findAllUserNotDpo(User $user)
    {
        $subquery = $this->createQueryBuilder('td1')
            ->leftJoin('td1.dpo', 'd')
            ->andWhere('d.user = :user');

        $query = $this->createQueryBuilder('u')
            ->andWhere('u.id NOT IN (' . $subquery->getDQL() . ')')
            ->setParameter(':user', $user->getId());

        return $query->getQuery()->getResult();
    }

    public function findAllQB(): QueryBuilder
    {
        return $this->createQueryBuilder('u');
    }
}
