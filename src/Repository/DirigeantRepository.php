<?php

namespace App\Repository;

use App\Entity\Dirigeant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dirigeant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dirigeant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dirigeant[]    findAll()
 * @method Dirigeant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DirigeantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dirigeant::class);
    }
}
