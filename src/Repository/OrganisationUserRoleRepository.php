<?php

namespace App\Repository;

use App\Entity\OrganisationUserRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganisationUserRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganisationUserRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganisationUserRole[]    findAll()
 * @method OrganisationUserRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganisationUserRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganisationUserRole::class);
    }
}
