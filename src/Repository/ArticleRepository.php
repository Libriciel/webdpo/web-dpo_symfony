<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Organisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findAllArticles(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('a');

        return $qb;
    }

    public function getAllArticlesForCurrentOrganisation(Organisation $organisation): QueryBuilder
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.organisations', 'o')
            ->andWhere('o = :organisation')
            ->setParameter(':organisation', $organisation);

        return $qb;
    }
}
