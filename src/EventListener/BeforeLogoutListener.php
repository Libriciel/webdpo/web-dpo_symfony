<?php

namespace App\EventListener;

use App\Service\User\UserManager;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class BeforeLogoutListener
{
    private UserManager $userManager;

    public function __construct(
        UserManager $userManager
    ) {
        $this->userManager = $userManager;
    }

    public function onSymfonyComponentSecurityHttpEventLogoutEvent(LogoutEvent $logoutEvent): void
    {
        $this->userManager->currentOrganisationLogout($logoutEvent->getToken()->getUser());
    }
}
