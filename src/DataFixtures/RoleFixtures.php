<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public const REFERENCE = 'Role_';

    public function load(ObjectManager $manager)
    {
        $roleSuperAdmin = (new Role())
            ->setName('Superadmin')
            ->addComposite('ROLE_SUPERADMIN')
            ->addComposite('ROLE_MANAGE_ORGANISATIONS')
            ->addComposite('ROLE_MANAGE_USERS')
        ;
        $manager->persist($roleSuperAdmin);
        $this->addReference(self::REFERENCE . 'superadmin', $roleSuperAdmin);

        $roleDpo = (new Role())
            ->setName('DPO')
            ->addComposite('ROLE_DPO')
            ->addComposite('ROLE_MANAGE_ORGANISATIONS')
            ->addComposite('ROLE_MANAGE_USERS')
        ;
        $manager->persist($roleDpo);
        $this->addReference(self::REFERENCE . 'dpo', $roleDpo);

        $roleAdmin = (new Role())
            ->setName('Administrateur')
            ->addComposite('ROLE_ADMIN')
            ->addComposite('ROLE_MANAGE_ORGANISATIONS')
            ->addComposite('ROLE_MANAGE_USERS')
        ;
        $manager->persist($roleAdmin);
        $this->addReference(self::REFERENCE . 'admin', $roleAdmin);

        $roleValideur = (new Role())
            ->setName('Valideur')
            ->addComposite('ROLE_VALIDEUR')
            ->addComposite('ROLE_MANAGE_VALIDATION')
        ;
        $manager->persist($roleValideur);
        $this->addReference(self::REFERENCE . 'valideur', $roleValideur);

        $roleRedacteur = (new Role())
            ->setName('Rédacteur')
            ->addComposite('ROLE_REDACTEUR')
            ->addComposite('ROLE_MANAGE_REDACTION')
        ;
        $manager->persist($roleRedacteur);
        $this->addReference(self::REFERENCE . 'redacteur', $roleRedacteur);

        $roleConsultant = (new Role())
            ->setName('Consultant')
            ->addComposite('ROLE_CONSULTANT')
            ->addComposite('ROLE_MANAGE_CONSULTATION')
        ;
        $manager->persist($roleRedacteur);
        $this->addReference(self::REFERENCE . 'consultant', $roleRedacteur);

        $manager->flush();
    }
}
