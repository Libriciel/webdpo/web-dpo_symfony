<?php

namespace App\DataFixtures;

use App\Entity\Coresponsable;
use App\Entity\Delegue;
use App\Entity\Representant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CoresponsableFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $raisonSocialeExisting = [];
        $emailExisting = [];
        for ($i = 1; $i <= 25; $i++) {
            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $raisonSocialeFaker = $faker->company;
            while (in_array($raisonSocialeFaker, $raisonSocialeExisting)) {
                $raisonSocialeFaker = $faker->company;
            }
            $raisonSocialeExisting[] = $raisonSocialeFaker;

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $representant = (new Representant())
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
                ->setFunction($faker->jobTitle)
            ;

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $delegue = new Delegue();
            $delegue
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
            ;

            $coresponsable = new Coresponsable();
            $coresponsable
                ->setRaisonsociale($raisonSocialeFaker)
                ->setRepresentant($representant)
                ->setDelegue($delegue)
            ;

            $manager->persist($coresponsable);
        }

        $coresponsable = new Coresponsable();
        $coresponsable
            ->setRaisonsociale('EDITION')
        ;
        $manager->persist($coresponsable);

        $coresponsable = new Coresponsable();
        $coresponsable
            ->setRaisonsociale('ASSOCIATION')
        ;
        $manager->persist($coresponsable);

        $countCoresponsable = 0;
        for ($i = 1; $i <= 5; $i++) {
            $countCoresponsable++;

            $coresponsable = new Coresponsable();
            $coresponsable
                ->setRaisonsociale('DELETE' . $countCoresponsable);
            $manager->persist($coresponsable);
        }

        $organisation_libriciel = $this->getReference('organisation_libriciel');
        $coresponsable = new Coresponsable();
        $coresponsable
            ->setRaisonsociale("NOT CAN'T EDIT")
            ->addOrganisation($organisation_libriciel)
        ;
        $manager->persist($coresponsable);

        $countCoresponsable = 0;
        for ($i = 1; $i <= 5; $i++) {
            $countCoresponsable++;

            $coresponsable = new Coresponsable();
            $coresponsable
                ->setRaisonsociale("DISSOCIATION" . $countCoresponsable)
                ->addOrganisation($organisation_libriciel)
            ;
            $manager->persist($coresponsable);
        }

        $manager->flush();
    }
}
