<?php

namespace App\DataFixtures;

use App\Entity\ModelePresentation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ModelePresentationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 20; $i++) {
            $modelePresentation = new ModelePresentation();

            $modelePresentation
                ->setOrganisation($this->getReference('organisation_' . $i))
                ->setFileName("modele_presentation_{$i}")
                ->setPath("/tmp/presentation/{$i}")
            ;

            $manager->persist($modelePresentation);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
        ];
    }
}
