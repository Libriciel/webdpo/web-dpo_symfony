<?php

namespace App\DataFixtures;

use App\Entity\Organisation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\fr_FR\Company;
use Faker\Provider\fr_FR\PhoneNumber;

class OrganisationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new Company($faker));
        $faker->addProvider(new PhoneNumber($faker));

        $organisation = (new Organisation())
            ->setRaisonsociale('Libriciel SCOP')
            ->setPhone('0467659644')
            ->setAddress('836 Rue du Mas de Verchant, 34000 Montpellier')
            ->setEmail('contact@libriciel.coop')
            ->setSiret('49101169800025')
            ->setApe('6202B')
        ;
        $manager->persist($organisation);
        $this->addReference('organisation_libriciel', $organisation);

        $raisonSocialeExisting = [];
        for ($i = 1; $i <= 20; $i++) {
            $raisonSocialeFaker = $faker->company;
            while (in_array($raisonSocialeFaker, $raisonSocialeExisting)) {
                $raisonSocialeFaker = $faker->company;
            }

            $raisonSocialeExisting[] = $raisonSocialeFaker;

            $organisation = (new Organisation())
                ->setRaisonsociale($raisonSocialeFaker)
                ->setPhone('04' . $faker->phoneNumber08())
                ->setAddress($faker->address)
                ->setEmail($faker->companyEmail)
                ->setSiret($faker->siret(false))
                ->setApe('8411Z')
            ;

            $manager->persist($organisation);

            $this->addReference('organisation_' . $i, $organisation);
        }

        $manager->flush();
    }
}
