<?php

namespace App\DataFixtures;

use App\Entity\Organisation;
use App\Entity\Typage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TypageFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $nameExisting = [];

        $typage = new Typage();
        $typage
            ->setName("Sous-traitance")
            ->setCreatedByOrganisation(null)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName("Co-responsabilité")
            ->setCreatedByOrganisation(null)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName("AIPD")
            ->setCreatedByOrganisation(null)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName("Autre")
            ->setCreatedByOrganisation(null)
        ;
        $manager->persist($typage);

        for ($i = 1; $i <= 20; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $typage = new Typage();

            $organisation = $this->getReference('organisation_' . rand(1, 20));

            $typage
                ->setName($intitulerFaker)
                ->setCreatedByOrganisation($this->getReference('organisation_' . rand(1, 20)))
                ->addOrganisation($organisation)
            ;

            $manager->persist($typage);
        }

        /** @var Organisation $organisation_1 */
        $organisation_1 = $this->getReference('organisation_1');
        /** @var Organisation $organisation_2 */
        $organisation_2 = $this->getReference('organisation_2');

        for ($i = 1; $i <= 10; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $typage = new Typage();

            $typage
                ->setName($intitulerFaker)
                ->setCreatedByOrganisation($organisation_1)
                ->addOrganisation($organisation_1)
                ->addOrganisation($organisation_2)
            ;

            $manager->persist($typage);
        }

        $typage = new Typage();
        $typage
            ->setName('Annexe confidentialité')
            ->setCreatedByOrganisation($organisation_1)
        ;
        $manager->persist($typage);

        $organisation_libriciel = $this->getReference('organisation_libriciel');
        $typage = new Typage();
        $typage
            ->setName('Contrat d\'affaire')
            ->setCreatedByOrganisation($organisation_libriciel)
            ->addOrganisation($organisation_libriciel)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName("NOT CAN'T EDIT")
            ->setCreatedByOrganisation($organisation_libriciel)
            ->addOrganisation($organisation_libriciel)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName('Annexe financière')
            ->setCreatedByOrganisation($organisation_libriciel)
            ->addOrganisation($organisation_libriciel)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName('CGU')
            ->setCreatedByOrganisation($organisation_libriciel)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName('CGV')
            ->setCreatedByOrganisation($organisation_libriciel)
        ;
        $manager->persist($typage);

        $typage = new Typage();
        $typage
            ->setName("EDIT")
            ->setCreatedByOrganisation($organisation_libriciel)
        ;
        $manager->persist($typage);

        for ($i = 1; $i <= 5; $i++) {
            $typage = new Typage();
            $typage
                ->setName("DELETE_" . $i)
                ->setCreatedByOrganisation($organisation_libriciel);
            $manager->persist($typage);

            $typage = new Typage();
            $typage
                ->setName("DISSOCIATION_" . $i)
                ->setCreatedByOrganisation($organisation_libriciel)
                ->addOrganisation($organisation_libriciel)
            ;
            $manager->persist($typage);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
        ];
    }
}
