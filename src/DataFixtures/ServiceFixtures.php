<?php

namespace App\DataFixtures;

use App\Entity\Organisation;
use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ServiceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $nameExisting = [];

        for ($i = 1; $i <= 20; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $service = new Service();

            $organisation = $this->getReference('organisation_' . rand(1, 20));

            $service
                ->setOrganisation($organisation)
                ->setName($intitulerFaker)
            ;

            $manager->persist($service);
        }

        /** @var Organisation $organisation_1 */
        $organisation_1 = $this->getReference('organisation_1');
        for ($i = 1; $i <= 5; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $service = new Service();

            $service
                ->setOrganisation($organisation_1)
                ->setName($intitulerFaker)
            ;

            $manager->persist($service);
        }

        $organisation_libriciel = $this->getReference('organisation_libriciel');
        $service = new Service();
        $service
            ->setOrganisation($organisation_libriciel)
            ->setName("EDIT")
        ;
        $manager->persist($service);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
        ];
    }
}
