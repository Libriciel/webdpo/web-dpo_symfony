<?php

namespace App\DataFixtures;

use App\Entity\Norme;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class NormeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $norme = new Norme();
        $norme
            ->setNorme("AU")
            ->setNumero("001")
            ->setName("Système d'information géographique - SIG")
            ->setDescription($faker->paragraphs(1, true))
        ;
        $manager->persist($norme);

        $norme = new Norme();
        $norme
            ->setNorme("DI")
            ->setNumero("000")
            ->setName("Comptabilité générale")
            ->setDescription($faker->paragraphs(1, true))
        ;
        $manager->persist($norme);

        $norme = new Norme();
        $norme
            ->setNorme("MR")
            ->setNumero("001")
            ->setName("Recherches dans le domaine de la santé avec recueil du consentement")
            ->setDescription($faker->paragraphs(1, true))
        ;
        $manager->persist($norme);

        $norme = new Norme();
        $norme
            ->setNorme("NS")
            ->setNumero("008")
            ->setName("Facturation des consommations d'énergie")
            ->setDescription($faker->paragraphs(1, true))
        ;
        $manager->persist($norme);

        $norme = new Norme();
        $norme
            ->setNorme("RS")
            ->setNumero("001")
            ->setName("Gestion des vigilances sanitaires")
            ->setDescription($faker->paragraphs(1, true))
        ;
        $manager->persist($norme);

        $norme = new Norme();
        $norme
            ->setNorme("RU")
            ->setNumero("001")
            ->setName("Attestations d'accueil des étrangers")
            ->setDescription($faker->paragraphs(1, true))
        ;
        $manager->persist($norme);

        $manager->flush();
    }
}
