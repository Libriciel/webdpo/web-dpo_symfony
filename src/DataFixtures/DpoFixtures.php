<?php

namespace App\DataFixtures;

use App\Entity\Dpo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class DpoFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $emailExisting = [];

        for ($i = 1; $i <= 5; $i++) {
            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $dpo = new Dpo();

            $organisation = $this->getReference('organisation_' . $i);
            $user = $this->getReference('user_dpo_' . $i);

            $dpo
                ->setOrganisation($organisation)
                ->setUser($user)
                ->setNumber('DPO-' . $faker->randomNumber(7))
                ->setEmail($emailFaker)
            ;

            $manager->persist($dpo);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
            UserFixtures::class,
        ];
    }
}
