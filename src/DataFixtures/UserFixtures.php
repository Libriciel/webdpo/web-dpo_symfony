<?php

namespace App\DataFixtures;

use App\Entity\Organisation;
use App\Entity\OrganisationUserRole;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        /** @var Role  $roleSuperAdmin */
        $roleSuperAdmin = $this->getReference(RoleFixtures::REFERENCE . 'superadmin');
        $roleDpo = $this->getReference(RoleFixtures::REFERENCE . 'dpo');

        /** @var Role $roleAdmin */
        $roleAdmin = $this->getReference(RoleFixtures::REFERENCE . 'admin');

        //        $roleValideur = $this->getReference(RoleFixtures::REFERENCE . 'valideur');
        //        $roleRedacteur = $this->getReference(RoleFixtures::REFERENCE . 'redacteur');
        //        $roleConsultant = $this->getReference(RoleFixtures::REFERENCE . 'consultant');

        // --------------------------------------------------------
        /** @var Organisation $organisation_1 */
        $organisation_libriciel = $this->getReference('organisation_libriciel');
        $user = new User();
        $user
            ->setUsername('admin_libriciel')
            ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
            ->setCivility('M.')
            ->setFirstName('Jean')
            ->setLastName('ADMIN')
            ->setEmail('j.admin@libriciel.test')
            ->setNotification(false)
            ->setCurrentOrganisation($organisation_libriciel)
        ;
        $manager->persist($user);

        $organisationUserRole = new OrganisationUserRole();
        $organisationUserRole
            ->setUser($user)
            ->setOrganisation($organisation_libriciel)
            ->setRole($roleAdmin)
        ;
        $manager->persist($organisationUserRole);
        // --------------------------------------------------------

        $faker = Factory::create('fr_FR');

        $emailExisting = [];

        /** @var Organisation $organisation_1 */
        $organisation_1 = $this->getReference('organisation_1');
        /** @var Organisation $organisation_2 */
        $organisation_2 = $this->getReference('organisation_2');

        for ($i = 1; $i <= 5; $i++) {
            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $user = new User();
            $user
                ->setUsername('dpo' . $i)
                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
                ->setNotification($faker->boolean)
                ->setCurrentOrganisation($organisation_1)
            ;
            $manager->persist($user);

            $this->addReference('user_dpo_' . $i, $user);

            $organisationUserRole = new OrganisationUserRole();
            $organisationUserRole
                ->setUser($user)
                ->setOrganisation($organisation_1)
                ->setRole($roleAdmin)
            ;
            $manager->persist($organisationUserRole);
        }

        $countAdmin = 0;
        for ($i = 1; $i <= 5; $i++) {
            $countAdmin++;

            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $user = new User();
            $user
                ->setUsername('admin' . $countAdmin)
                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
                ->setNotification($faker->boolean)
                ->setCurrentOrganisation($organisation_1)
            ;
            $manager->persist($user);

            $this->addReference('user_admin_' . $countAdmin, $user);

            $organisationUserRole = new OrganisationUserRole();
            $organisationUserRole
                ->setUser($user)
                ->setOrganisation($organisation_1)
                ->setRole($roleAdmin)
            ;
            $manager->persist($organisationUserRole);
        }

        for ($i = 1; $i <= 5; $i++) {
            $countAdmin++;

            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $user = new User();
            $user
                ->setUsername('admin' . $countAdmin)
                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
                ->setNotification($faker->boolean)
                ->setCurrentOrganisation($organisation_2)
            ;
            $manager->persist($user);

            $this->addReference('user_admin_' . $countAdmin, $user);

            $organisationUserRole = new OrganisationUserRole();
            $organisationUserRole
                ->setUser($user)
                ->setOrganisation($organisation_2)
                ->setRole($roleAdmin)
            ;
            $manager->persist($organisationUserRole);
        }

        for ($i = 1; $i <= 5; $i++) {
            $countAdmin++;

            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $user = new User();
            $user
                ->setUsername('admin' . $countAdmin)
                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
                ->setNotification($faker->boolean)
                ->setCurrentOrganisation($organisation_1)
            ;
            $manager->persist($user);

            $this->addReference('user_admin_' . $countAdmin, $user);

            $organisationUserRole = new OrganisationUserRole();
            $organisationUserRole
                ->setUser($user)
                ->setOrganisation($organisation_1)
                ->setRole($roleAdmin)
            ;
            $manager->persist($organisationUserRole);

            $organisationUserRole = new OrganisationUserRole();
            $organisationUserRole
                ->setUser($user)
                ->setOrganisation($organisation_2)
                ->setRole($roleAdmin)
            ;
            $manager->persist($organisationUserRole);
        }

        /** @var Organisation $organisation_3 */
        $organisation_3 = $this->getReference('organisation_3');
        for ($i = 1; $i <= 5; $i++) {
            $countAdmin++;

            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $user = new User();
            $user
                ->setUsername('admin' . $countAdmin)
                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
                ->setNotification($faker->boolean)
                ->setCurrentOrganisation($organisation_3)
            ;
            $manager->persist($user);

            $this->addReference('user_admin_' . $countAdmin, $user);

            $organisationUserRole = new OrganisationUserRole();
            $organisationUserRole
                ->setUser($user)
                ->setOrganisation($organisation_3)
                ->setRole($roleAdmin)
            ;
            $manager->persist($organisationUserRole);
        }

        //        for ($i = 1; $i <= 5; $i++){
        //            $civility =  ['M.', 'Mme.'];
        //            $key = array_rand($civility);
        //
        //            $emailFaker = $faker->companyEmail;
        //            while (in_array($emailFaker, $emailExisting)) {
        //                $emailFaker = $faker->companyEmail;
        //            }
        //            $emailExisting[] = $emailFaker;
        //
        //            $user = new User();
        //
        //            $user
        //                ->setUsername('valideur'.$i)
        //                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
        //                ->setCivility($civility[$key])
        //                ->setFirstName($faker->firstName)
        //                ->setLastName($faker->lastName)
        //                ->setEmail($emailFaker)
        //                ->setNotification($faker->boolean)
        //                ->setRole($roleValideur)
        //                ->addAuthorizedOrganisation($organisation_1)
        //                ->addAuthorizedOrganisation($organisation_2)
        //            ;
        //
        //            $manager->persist($user);
        //
        //            $this->addReference('user_valideur_'.$i, $user);
        //        }
        //
        //        for ($i = 1; $i <= 5; $i++){
        //            $civility =  ['M.', 'Mme.'];
        //            $key = array_rand($civility);
        //
        //            $emailFaker = $faker->companyEmail;
        //            while (in_array($emailFaker, $emailExisting)) {
        //                $emailFaker = $faker->companyEmail;
        //            }
        //            $emailExisting[] = $emailFaker;
        //
        //            $user = new User();
        //
        //            $user
        //                ->setUsername('redacteur'.$i)
        //                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
        //                ->setCivility($civility[$key])
        //                ->setFirstName($faker->firstName)
        //                ->setLastName($faker->lastName)
        //                ->setEmail($emailFaker)
        //                ->setNotification($faker->boolean)
        //                ->setRole($roleRedacteur)
        //                ->addAuthorizedOrganisation($organisation_1)
        //                ->addAuthorizedOrganisation($organisation_2)
        //            ;
        //
        //            $manager->persist($user);
        //
        //            $this->addReference('user_redacteur_'.$i, $user);
        //        }
        //
        //        for ($i = 1; $i <= 5; $i++){
        //            $civility =  ['M.', 'Mme.'];
        //            $key = array_rand($civility);
        //
        //            $emailFaker = $faker->companyEmail;
        //            while (in_array($emailFaker, $emailExisting)) {
        //                $emailFaker = $faker->companyEmail;
        //            }
        //            $emailExisting[] = $emailFaker;
        //
        //            $user = new User();
        //
        //            $user
        //                ->setUsername('consultant'.$i)
        //                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
        //                ->setCivility($civility[$key])
        //                ->setFirstName($faker->firstName)
        //                ->setLastName($faker->lastName)
        //                ->setEmail($emailFaker)
        //                ->setNotification($faker->boolean)
        //                ->setRole($roleConsultant)
        //                ->addAuthorizedOrganisation($organisation_1)
        //                ->addAuthorizedOrganisation($organisation_2)
        //            ;
        //
        //            $manager->persist($user);
        //
        //            $this->addReference('user_consultant_'.$i, $user);
        //        }

        $user = new User();
        $user
            ->setUsername('superadmin')
            ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
            ->setCivility('M.')
            ->setFirstName('Super')
            ->setLastName('ADMIN')
            ->setEmail("superadmin@webdpo.test")
            ->setNotification(false)
            ->setIsSuperadmin(true)
        ;
        $manager->persist($user);

        $organisationUserRole = new OrganisationUserRole();
        $organisationUserRole
            ->setUser($user)
            ->setRole($roleSuperAdmin)
        ;
        $manager->persist($organisationUserRole);

        $user = new User();
        $user
            ->setUsername('superadmin1')
            ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
            ->setCivility('M.')
            ->setFirstName('Super')
            ->setLastName('ADMIN')
            ->setEmail("superadmin1@webdpo.test")
            ->setNotification(false)
            ->setIsSuperadmin(true)
        ;
        $manager->persist($user);

        $organisationUserRole = new OrganisationUserRole();
        $organisationUserRole
            ->setUser($user)
            ->setRole($roleSuperAdmin)
        ;
        $manager->persist($organisationUserRole);

        $countUserDELETE = 0;
        for ($i = 1; $i <= 5; $i++) {
            $countUserDELETE++;

            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $user = new User();
            $user
                ->setUsername('delete' . $countUserDELETE)
                ->setPassword($this->passwordHasher->hashPassword($user, 'admin'))
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($emailFaker)
                ->setNotification($faker->boolean)
                ->setIsActive(false)
            ;
            $manager->persist($user);

            $organisationUserRole = new OrganisationUserRole();
            $organisationUserRole
                ->setUser($user)
                ->setRole($roleAdmin)
            ;
            $manager->persist($organisationUserRole);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
            RoleFixtures::class,
        ];
    }
}
