<?php

namespace App\DataFixtures;

use App\Entity\Dirigeant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class DirigeantFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 1; $i <= 20; $i++) {
            $civility = ['M.', 'Mme.'];
            $key = array_rand($civility);

            $dirigeant = new Dirigeant();

            $organisation = $this->getReference('organisation_' . $i);

            $dirigeant
                ->setOrganisation($organisation)
                ->setCivility($civility[$key])
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setFunction($faker->jobTitle)
            ;

            $manager->persist($dirigeant);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
        ];
    }
}
