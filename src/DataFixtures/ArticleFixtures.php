<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Organisation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $nameExisting = [];
        for ($i = 1; $i <= 20; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $organisation = $this->getReference('organisation_' . rand(1, 20));

            $article = (new Article())
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(6, true))
                ->setCreatedByOrganisation($this->getReference('organisation_' . rand(1, 20)))
                ->addOrganisation($organisation)
            ;

            $manager->persist($article);
        }

        /** @var Organisation $organisation_1 */
        $organisation_1 = $this->getReference('organisation_1');
        /** @var Organisation $organisation_2 */
        $organisation_2 = $this->getReference('organisation_2');
        for ($i = 1; $i <= 10; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $article = (new Article())
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(6, true))
                ->setCreatedByOrganisation($organisation_1)
                ->addOrganisation($organisation_1)
                ->addOrganisation($organisation_2)
            ;

            $manager->persist($article);
        }

        for ($i = 1; $i <= 5; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $article = (new Article())
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(6, true))
                ->setCreatedByOrganisation($organisation_1)
                ->addOrganisation($organisation_1)
            ;

            $manager->persist($article);
        }

        for ($i = 1; $i <= 5; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $article = (new Article())
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(6, true))
                ->setCreatedByOrganisation($organisation_2)
                ->addOrganisation($organisation_2)
            ;

            $manager->persist($article);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
        ];
    }
}
