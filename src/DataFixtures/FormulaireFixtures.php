<?php

namespace App\DataFixtures;

use App\Entity\Formulaire;
use App\Entity\Option;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class FormulaireFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $nameExisting = [];
        for ($i = 1; $i <= 10; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $options = new Option();
            $formulaire = new Formulaire();
            $formulaire
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(1, true))
                ->setIsActivite(true)
                ->setCreatedByOrganisation($this->getReference('organisation_' . rand(1, 20)))
                ->setOptions($options)
            ;
            $manager->persist($formulaire);
        }

        for ($i = 1; $i <= 10; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $options = new Option();
            $formulaire = new Formulaire();
            $formulaire
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(1, true))
                ->setIsRtExterne(true)
                ->setCreatedByOrganisation($this->getReference('organisation_' . rand(1, 20)))
                ->setOptions($options)
            ;
            $manager->persist($formulaire);
        }

        for ($i = 1; $i <= 10; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $options = new Option();
            $formulaire = new Formulaire();
            $formulaire
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(1, true))
                ->setIsViolation(true)
                ->setCreatedByOrganisation($this->getReference('organisation_' . rand(1, 20)))
                ->setOptions($options)
            ;
            $manager->persist($formulaire);
        }

        for ($i = 1; $i <= 10; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $options = new Option();
            $formulaire = new Formulaire();
            $formulaire
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(1, true))
                ->setIsViolation(true)
                ->setCreatedByOrganisation($this->getReference('organisation_' . rand(1, 20)))
                ->setOptions($options)
                ->setIsArchived(true)
            ;
            $manager->persist($formulaire);
        }

        $organisation_libriciel = $this->getReference('organisation_libriciel');

        for ($i = 1; $i <= 5; $i++) {
            $intitulerFaker = $faker->word;
            while (in_array($intitulerFaker, $nameExisting)) {
                $intitulerFaker = $faker->word;
            }
            $nameExisting[] = $intitulerFaker;

            $options = new Option();
            $formulaire = new Formulaire();
            $formulaire
                ->setName($intitulerFaker)
                ->setDescription($faker->paragraphs(1, true))
                ->setIsActivite(true)
                ->setCreatedByOrganisation($organisation_libriciel)
                ->setOptions($options)
            ;
            $manager->persist($formulaire);
        }

        $options = new Option();
        $formulaire = new Formulaire();
        $formulaire
            ->setName('EDIT')
            ->setDescription($faker->paragraphs(1, true))
            ->setIsActivite(true)
            ->setCreatedByOrganisation($organisation_libriciel)
            ->setOptions($options)
        ;
        $manager->persist($formulaire);

        $options = new Option();
        $formulaire = new Formulaire();
        $formulaire
            ->setName('ARCHIVED')
            ->setDescription($faker->paragraphs(1, true))
            ->setIsActivite(true)
            ->setCreatedByOrganisation($organisation_libriciel)
            ->setOptions($options)
            ->setIsArchived(true)
        ;
        $manager->persist($formulaire);

        $options = new Option();
        $formulaire = new Formulaire();
        $formulaire
            ->setName('NOT ARCHIVED')
            ->setDescription($faker->paragraphs(1, true))
            ->setIsActivite(true)
            ->setCreatedByOrganisation($organisation_libriciel)
            ->setOptions($options)
            ->setIsArchived(false)
        ;
        $manager->persist($formulaire);

        $options = new Option();
        $formulaire = new Formulaire();
        $formulaire
            ->setName('NOT ACTIF')
            ->setDescription($faker->paragraphs(1, true))
            ->setIsActivite(true)
            ->setCreatedByOrganisation($organisation_libriciel)
            ->setOptions($options)
            ->setIsActive(false)
        ;
        $manager->persist($formulaire);

        $options = new Option();
        $formulaire = new Formulaire();
        $formulaire
            ->setName('ACTIF')
            ->setDescription($faker->paragraphs(1, true))
            ->setIsActivite(true)
            ->setCreatedByOrganisation($organisation_libriciel)
            ->setOptions($options)
            ->setIsActive(true)
        ;
        $manager->persist($formulaire);

        $options = new Option();
        $formulaire = new Formulaire();
        $formulaire
            ->setName('FOR NOT CREATIVE ENTITY')
            ->setDescription($faker->paragraphs(1, true))
            ->setIsActivite(true)
            ->setCreatedByOrganisation($this->getReference('organisation_' . rand(1, 20)))
            ->setOptions($options)
        ;
        $manager->persist($formulaire);

        $options = new Option();
        $formulaire = new Formulaire();
        $formulaire
            ->setName('ASSOCIATION')
            ->setDescription($faker->paragraphs(1, true))
            ->setIsActivite(true)
            ->setCreatedByOrganisation($organisation_libriciel)
            ->setOptions($options)
        ;
        $manager->persist($formulaire);

        for ($i = 1; $i <= 5; $i++) {
            $options = new Option();
            $formulaire = new Formulaire();
            $formulaire
                ->setName('DELETE_' . $i)
                ->setDescription($faker->paragraphs(1, true))
                ->setIsViolation(true)
                ->setCreatedByOrganisation($organisation_libriciel)
                ->setOptions($options)
            ;
            $manager->persist($formulaire);
        }

        for ($i = 1; $i <= 5; $i++) {
            $options = new Option();
            $formulaire = new Formulaire();
            $formulaire
                ->setName('DISSOCIATION_' . $i)
                ->setDescription($faker->paragraphs(1, true))
                ->setIsViolation(true)
                ->setCreatedByOrganisation($organisation_libriciel)
                ->setOptions($options)
                ->addOrganisation($organisation_libriciel)
            ;
            $manager->persist($formulaire);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
        ];
    }
}
