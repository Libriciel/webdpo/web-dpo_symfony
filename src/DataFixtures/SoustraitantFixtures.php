<?php

namespace App\DataFixtures;

use App\Entity\Soustraitant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SoustraitantFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $raisonSocialeExisting = [];
        $emailExisting = [];
        for ($i = 1; $i <= 20; $i++) {
            $raisonSocialeFaker = $faker->company;
            while (in_array($raisonSocialeFaker, $raisonSocialeExisting)) {
                $raisonSocialeFaker = $faker->company;
            }
            $raisonSocialeExisting[] = $raisonSocialeFaker;

            $emailFaker = $faker->companyEmail;
            while (in_array($emailFaker, $emailExisting)) {
                $emailFaker = $faker->companyEmail;
            }
            $emailExisting[] = $emailFaker;

            $organisation = $this->getReference('organisation_' . rand(1, 20));

            $soustraitant = new Soustraitant();
            $soustraitant
                ->setRaisonsociale($raisonSocialeFaker)
                ->addOrganisation($organisation)
            ;

            $manager->persist($soustraitant);
        }

        $soustraitant = new Soustraitant();
        $soustraitant
            ->setRaisonsociale("TOTO")
        ;
        $manager->persist($soustraitant);

        $soustraitant = new Soustraitant();
        $soustraitant
            ->setRaisonsociale("DELETE")
        ;
        $manager->persist($soustraitant);

        $soustraitant = new Soustraitant();
        $soustraitant
            ->setRaisonsociale("ASSOCIATION")
        ;
        $manager->persist($soustraitant);

        $organisation_libriciel = $this->getReference('organisation_libriciel');
        $soustraitant = new Soustraitant();
        $soustraitant
            ->setRaisonsociale("DISSOCIATION")
            ->addOrganisation($organisation_libriciel)
        ;
        $manager->persist($soustraitant);

        $soustraitant = new Soustraitant();
        $soustraitant
            ->setRaisonsociale("NOT CAN'T EDIT")
            ->addOrganisation($organisation_libriciel)
        ;
        $manager->persist($soustraitant);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            OrganisationFixtures::class,
        ];
    }
}
