<?php

namespace App\DataFixtures;

use App\Entity\Referentiel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ReferentielFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel pour la gestion des traitements courants des cabinets médicaux et paramédicaux')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel pour la prise en charge médico-sociale des personnes âgées, en situation de handicap ou en difficulté')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel relatif à la désignation des conducteurs ayant commis une infraction au code la route')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel relatif à la gestion des ressources humaines')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel relatif à la gestion locative')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel relatif au dispositif d’alertes professionnelles')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel vigilance sanitaire')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel vise les traitements de données dans le domaine de la santé')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        $referentiel = new Referentiel();
        $referentiel
            ->setName('Référentiel vis les traitements de données mis en œuvre à des fins de recherche, d’étude, et d’évaluation dans le domaine de la santé')
            ->setDescription($faker->paragraphs(6, true))
        ;
        $manager->persist($referentiel);

        //        $nameExisting = [];
        //        for ($i = 1; $i <= 5; $i++) {
        //            $intitulerFaker = $faker->word;
        //            while (in_array($intitulerFaker, $nameExisting)) {
        //                $intitulerFaker = $faker->word;
        //            }
        //            $nameExisting[] = $intitulerFaker;
        //
        //            $referentiel = new Referentiel();
        //            $referentiel
        //                ->setName($intitulerFaker)
        //                ->setDescription($faker->paragraphs(6, true))
        //            ;
        //            $manager->persist($referentiel);
        //        }
        //
        //        for ($i = 1; $i <= 5; $i++) {
        //            $intitulerFaker = $faker->word;
        //            while (in_array($intitulerFaker, $nameExisting)) {
        //                $intitulerFaker = $faker->word;
        //            }
        //            $nameExisting[] = $intitulerFaker;
        //
        //            $referentiel = new Referentiel();
        //            $referentiel
        //                ->setName($intitulerFaker)
        //                ->setDescription($faker->paragraphs(6, true))
        //                ->setAbroger(true)
        //            ;
        //            $manager->persist($referentiel);
        //        }

        $manager->flush();
    }
}
