<?php

namespace App\Sidebar\Twig;

use App\Sidebar\State\SidebarState;
use Symfony\Bundle\SecurityBundle\Security;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SidebarExtension extends AbstractExtension
{
    public function __construct(
        private readonly Environment $twig,
        private readonly SidebarState $sidebarState,
        private readonly Security $security
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('ls_sidebar', [$this, 'getSidebar'], ['is_safe' => ['html']]),
        ];
    }

    public function getSidebar(): string
    {
        if ($this->security->isGranted('ROLE_SUPERADMIN')) {
            $viewSidebar = 'sidebar/sidebar_superadmin.twig.html.twig';
        } else {
            $viewSidebar = 'sidebar/sidebar.twig.html.twig';
        }

        return $this->twig->render($viewSidebar, [
            'activeNavs' => $this->sidebarState->getActiveNavs(),
        ]);
    }
}
