<?php

namespace App\Controller;

use App\Entity\Soustraitant;
use App\Form\MultiOrganisationUserType;
use App\Form\SoustraitantType;
use App\Repository\SoustraitantRepository;
use App\Service\Soustraitant\SoustraitantManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/soustraitant")
 *
 * @Sidebar(active={"soustraitant-nav"})
 * @Breadcrumb("Les sous-traitants de l'application", routeName="app_soustraitant_index")
 */
class SoustraitantController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_soustraitant_index",
     *     methods={"GET"}
     * )
     *
     * @Sidebar(active={"all-soustraitant-index-nav"})
     */
    public function index(Request $request, PaginatorInterface $paginator, SoustraitantRepository $soustraitantRepository): Response
    {
        $soustraitants = $paginator->paginate(
            $soustraitantRepository->findSoustraitantsAndRepresentant(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['s.raisonsociale'],
                'defaultSortDirection' => 'asc'
            ]
        );

        $form = $this->createForm(MultiOrganisationUserType::class);

        return $this->render('soustraitant/index.html.twig', [
            'controller_name' => 'SoustraitantController',
            'soustraitants' => $soustraitants,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/entity",
     *     name="app_soustraitant_entity",
     *     methods={"GET"}
     * )
     *
     * @Sidebar(active={"soustraitant-index-nav"})
     */
    public function entity(Request $request, PaginatorInterface $paginator, SoustraitantRepository $soustraitantRepository): Response
    {
        $soustraitants = $paginator->paginate(
            $soustraitantRepository->getAllSoustraitantsForCurrentOrganisation(
                $this->getUser()->getCurrentOrganisation()
            ),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['s.raisonsociale'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        return $this->render('soustraitant/entity.html.twig', [
            'controller_name' => 'SoustraitantController',
            'soustraitants' => $soustraitants,
        ]);
    }

    /**
     * @Route(
     *     "/add",
     *     name="app_soustraitant_add",
     *     methods={"GET", "POST"}
     * )
     */
    public function add(Request $request, SoustraitantManager $soustraitantManager): Response
    {
        $form = $this->createForm(SoustraitantType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $soustraitantManager->add(
                $form->getdata(),
                $form->get('logo')->getData()
            );

            $this->addFlash('success', 'soustraitant.success_message.save');

            return $this->redirectToRoute('app_soustraitant_index');
        }

        return $this->render('soustraitant/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter un soustraitant'
        ]);
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name="app_soustraitant_edit",
     *      requirements={"id": "%routing.uuid%"}
     * )
     *
     * @Breadcrumb("Modifier le sous-traitant")
     */
    public function edit(Soustraitant $soustraitant, Request $request, SoustraitantManager $soustraitantManager): Response
    {
        if (count($soustraitant->getOrganisations()) !== 0) {
            $this->addFlash('error', 'Impossible de modifier le sous-traitant');

            return $this->redirectToRoute('app_soustraitant_index');
        }

        $form = $this->createForm(SoustraitantType::class, $soustraitant)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $soustraitantManager->save(
                $form->getdata()
            );

            $this->addFlash('success', 'soustraitant.success_message.save');

            return $this->redirectToRoute('app_soustraitant_index');
        }

        return $this->render('soustraitant/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier un soustraitant'
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_soustraitant_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Soustraitant $soustraitant, SoustraitantManager $soustraitantManager, Request $request): Response
    {
        if (count($soustraitant->getOrganisations()) !== 0) {
            $this->addFlash('error', 'Impossible de supprimer le sous-traitant');

            return $this->redirectToRoute('app_soustraitant_index');
        }

        $soustraitantManager->delete($soustraitant);
        $this->addFlash('success', 'Le sous-traitant a bien été supprimé');

        return $this->redirectToRoute('app_soustraitant_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/deletemultiple",
     *     name="app_soustraitant_delete_multiple",
     *     methods={"DELETE"}
     * )
     */
    public function deleteMultiple(Request $request, SoustraitantManager $soustraitantManager): Response
    {
        $soustraitantManager->deleteMultiple(
            $request->request->get('delete')
        );

        $this->addFlash('success', 'soustraitant.success_message.save');

        return $this->redirectToRoute('app_soustraitant_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/association",
     *     name="app_soustraitant_association",
     *     methods={"POST"}
     * )
     */
    public function association(Request $request, SoustraitantManager $soustraitantManager): Response
    {
        $form = $this->createForm(MultiOrganisationUserType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $soustraitantManager->association($form->getdata());

            $this->addFlash('success', 'soustraitant.success_message.save');
        }

        return $this->redirectToRoute('app_soustraitant_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/dissociation/{id}",
     *     name="app_soustraitant_dissociation",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"POST"}
     * )
     */
    public function dissociation(Soustraitant $soustraitant, Request $request, SoustraitantManager $soustraitantManager): Response
    {
        $soustraitantManager->dissociation(
            $soustraitant,
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'soustraitant.success_message.save');

        return $this->redirectToRoute('app_soustraitant_entity', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/dissociationmultiple",
     *     name="app_soustraitant_dissociation_multiple"
     * )
     */
    public function dissociationMultiple(Request $request, SoustraitantManager $soustraitantManager): Response
    {
        $soustraitantManager->dissociationMultiple(
            $request->request->get('dissociation'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'soustraitant.success_message.save');

        return $this->redirectToRoute('app_soustraitant_entity', [
            'page' => $request->get('page')
        ]);
    }
}
