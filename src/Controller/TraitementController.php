<?php

namespace App\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/traitement')]
#[Breadcrumb(title: 'Accueil', routeName: 'app_traitement_index')]
class TraitementController extends AbstractController
{
    #[Route(path: '', name: 'app_traitement_index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('traitement/index.html.twig', [
            'controller_name' => 'TraitementController',
        ]);
    }
}
