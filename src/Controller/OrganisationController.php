<?php

namespace App\Controller;

use App\Entity\Organisation;
use App\Form\ChangeOrganisationType;
use App\Form\OrganisationType;
use App\Repository\OrganisationRepository;
use App\Service\Organisation\OrganisationManager;
use App\Service\User\UserManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/organisation")
 *
 * @Sidebar(active={"organisation-nav"})
 * @Breadcrumb("Les entités de l'application", routeName="app_organisation_index")
 */
class OrganisationController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_organisation_index",
     *     methods={"GET"}
     * )
     *
     * @IsGranted("ROLE_SUPERADMIN")
     * @Sidebar(active={"organisation-index-nav"})
     */
    public function index(Request $request, OrganisationRepository $organisationRepository, PaginatorInterface $paginator): Response
    {
        $organisations = $paginator->paginate(
            $organisationRepository->findAll(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['raisonsociale'],
                'defaultSortDirection' => 'asc'
            ]
        );

        return $this->render('organisation/index.html.twig', [
            'controller_name' => 'OrganisationController',
            'organisations' => $organisations,
        ]);
    }

    /**
     * @Route(
     *      "/add",
     *      name="app_organisation_add"
     * )
     *
     * @IsGranted("ROLE_SUPERADMIN")
     * @Breadcrumb("Créer une entité")
     */
    public function add(Request $request, OrganisationManager $organisationManager): Response
    {
        $form = $this->createForm(OrganisationType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $organisationManager->add(
                $form->getdata(),
                $form->get('logo')->getData()
            );

            $this->addFlash('success', 'organisation.success_message.save');

            return $this->redirectToRoute('app_organisation_index');
        }

        return $this->render('organisation/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name="app_organisation_edit",
     *      requirements={"id": "%routing.uuid%"}
     * )
     *
     * @IsGranted("ROLE_MANAGE_ORGANISATIONS")
     * @Breadcrumb("Modifier l'entité")
     * @Sidebar(active={"organisation-edit-nav"})
     */
    public function edit(Organisation $organisation, Request $request, OrganisationManager $organisationManager): Response
    {
        $form = $this->createForm(OrganisationType::class, $organisation, ['isEditMode' => true])
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $organisationManager->save($form->getdata());

            $this->addFlash('success', 'organisation.success_message.save');

            return $this->redirectToRoute('app_organisation_index');
        }

        return $this->render('organisation/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *      "/show/{id}",
     *      name="app_organisation_show",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"GET"}
     * )
     *
     * @Breadcrumb("Visualiser")
     */
    public function show(Organisation $organisation): Response
    {
        return $this->render('organisation/show.html.twig', [
            'organisation' => $organisation
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_organisation_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     *
     * @IsGranted("ROLE_SUPERADMIN")
     */
    public function delete(Organisation $organisation, OrganisationManager $organisationManager, Request $request): Response
    {
        $organisationManager->delete($organisation);
        $this->addFlash('success', 'organisation.success_message.delete');

        return $this->redirectToRoute('app_organisation_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *      "/change",
     *      name="app_organisation_change",
     * )
     */
    public function change(Request $request, UserManager $userManager): Response
    {
        $form = $this->createForm(ChangeOrganisationType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->addCurrentOrganisationLogin(
                $this->getUser(),
                $form->get('organisation')->getData()
            );

            $this->addFlash('success', 'organisation.success_message.change');

            return $this->redirectToRoute('app_user_index');
        }

        return $this->render('organisation/_formChangeOrganisation.html.twig', [
            'form_change_organisation' => $form->createView()
        ]);
    }
}
