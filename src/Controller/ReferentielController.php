<?php

namespace App\Controller;

use App\Entity\Referentiel;
use App\Form\ReferentielType;
use App\Repository\ReferentielRepository;
use App\Service\Referentiel\ReferentielManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/referentiel")
 *
 * @Breadcrumb("Référentiels", routeName="app_referentiel_index")
 * @Sidebar(active={"referentiel-nav"})
 */
class ReferentielController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_referentiel_index",
     *     methods={"GET"}
     * )
     */
    public function index(Request $request, ReferentielRepository $referentielRepository, PaginatorInterface $paginator): Response
    {
        $referentiels = $paginator->paginate(
            $referentielRepository->getAllReferentiels(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['r.name'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        return $this->render('referentiel/index.html.twig', [
            'controller_name' => 'ReferentielController',
            'referentiels' => $referentiels
        ]);
    }

    /**
     * @Route(
     *     "/add",
     *     name="app_referentiel_add",
     *     methods={"GET", "POST"}
     * )
     */
    public function add(Request $request, ReferentielManager $referentielManager): Response
    {
        $form = $this->createForm(ReferentielType::class)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $referentielManager->add(
                $form->getdata(),
                $form->get('file')->getData()
            );

            $this->addFlash('success', 'referentiel.success_message.save');

            return $this->redirectToRoute('app_referentiel_index');
        }

        return $this->render('referentiel/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter un référentiel'
        ]);
    }

    /**
     * @Route(
     *     "/edit/{id}",
     *     name="app_referentiel_edit",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"GET", "POST"}
     * )
     */
    public function edit(Referentiel $referentiel, Request $request, ReferentielManager $referentielManager): Response
    {
        if ($referentiel->getAbroger() === true) {
            $this->addFlash('error', 'Impossible de modifier le référentiel');

            return $this->redirectToRoute('app_referentiel_index');
        }

        $form = $this->createForm(ReferentielType::class, $referentiel)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $referentielManager->save($form->getdata());

            $this->addFlash('success', 'referentiel.success_message.save');

            return $this->redirectToRoute('app_referentiel_index');
        }

        return $this->render('referentiel/add.html.twig', [
            'form' => $form->createView(),
            'referentiel' => $referentiel,
            'title' => 'Modifier un référentiel'
        ]);
    }

    /**
     * @Route(
     *      "/show/{id}",
     *      name="app_referentiel_show",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"GET"}
     * )
     */
    public function show(Referentiel $referentiel): Response
    {
        return $this->render('referentiel/show.html.twig', [
            'referentiel' => $referentiel
        ]);
    }

    /**
     * @Route(
     *     "/abroger",
     *     name="app_referentiel_abroger",
     *     methods={"POST"}
     * )
     */
    public function abroger(Request $request, ReferentielManager $referentielManager): Response
    {
        $referentielManager->abroger(
            $request->request->get('abroger')
        );

        $this->addFlash('success', 'Le référentiel a bien été modifié');

        return $this->redirectToRoute('app_referentiel_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_referentiel_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Referentiel $referentiel, ReferentielManager $referentielManager, Request $request): Response
    {
        $referentielManager->delete(
            $referentiel
        );
        $this->addFlash('success', 'referentiel.success_message.delete');

        return $this->redirectToRoute('app_referentiel_index', [
            'page' => $request->get('page')
        ]);
    }
}
