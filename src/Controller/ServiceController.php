<?php

namespace App\Controller;

use App\Entity\Service;
use App\Form\ServiceType;
use App\Form\UsersForOrganisationType;
use App\Repository\ServiceRepository;
use App\Service\Service\ServiceManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/service")
 *
 * @IsGranted("ROLE_MANAGE_USERS")
 *
 * @Breadcrumb("Liste des services de l'entité", routeName="app_service_index")
 * @Sidebar(active={"user-nav"})
 * @Sidebar(active={"service-all-nav"})
 */
class ServiceController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_service_index",
     *     methods={"GET"}
     * )
     */
    public function index(Request $request, PaginatorInterface $paginator, ServiceRepository $serviceRepository): Response
    {
        $userCurrentOrganisation = $this->getUser()->getCurrentOrganisation();

        $services = $paginator->paginate(
            $serviceRepository->getAllServicesForCurrentOrganisation($userCurrentOrganisation),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['s.name'],
                'defaultSortDirection' => 'asc'
            ]
        );

        $form = $this->createForm(UsersForOrganisationType::class, null, ['userCurrentOrganisation' => $userCurrentOrganisation]);

        return $this->render('service/index.html.twig', [
            'controller_name' => 'ServiceController',
            'services' => $services,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/add",
     *     name="app_service_add",
     *     methods={"GET", "POST"}
     * )
     *
     * @Breadcrumb("Ajout d'un service")
     */
    public function add(Request $request, ServiceManager $serviceManager): Response
    {
        $form = $this->createForm(ServiceType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceManager->save(
                $form->getdata(),
                $this->getUser()->getCurrentOrganisation()
            );

            $this->addFlash('success', 'service.success_message.save');

            return $this->redirectToRoute('app_service_index');
        }

        return $this->render('service/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter un service'
        ]);
    }

    /**
     * @Route(
     *     "/edit/{id}",
     *     name="app_service_edit",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"GET", "POST"}
     * )
     *
     * @Breadcrumb("Modification d'un service")
     */
    public function edit(Service $service, Request $request, ServiceManager $serviceManager): Response
    {
        if ($service->getOrganisation() !== $this->getUser()->getCurrentOrganisation()) {
            $this->addFlash('error', 'Impossible de modifier le service d\'une autre entité');

            return $this->redirectToRoute('app_service_index');
        }

        if ($service->getTraitements()->count() !== 0) {
            $this->addFlash('error', 'Impossible de supprimer le service');

            return $this->redirectToRoute('app_service_index');
        }

        $form = $this->createForm(ServiceType::class, $service)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceManager->save(
                $form->getdata(),
                $this->getUser()->getCurrentOrganisation()
            );

            $this->addFlash('success', 'service.success_message.save');

            return $this->redirectToRoute('app_service_index');
        }

        return $this->render('service/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier un service'
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_service_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Service $service, ServiceManager $serviceManager, Request $request): Response
    {
        if ($service->getOrganisation() !== $this->getUser()->getCurrentOrganisation()) {
            $this->addFlash('error', 'Impossible de supprimer le service d\'une autre entité');

            return $this->redirectToRoute('app_service_index');
        }

        if ($service->getUser()->count() !== 0 || $service->getTraitements()->count() !== 0) {
            $this->addFlash('error', 'Impossible de supprimer le service');

            return $this->redirectToRoute('app_service_index');
        }

        $serviceManager->delete($service);
        $this->addFlash('success', 'service.success_message.delete');

        return $this->redirectToRoute('app_service_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/deletemultiple",
     *     name="app_service_delete_multiple",
     *     methods={"DELETE"}
     * )
     */
    public function deleteMultiple(Request $request, ServiceManager $serviceManager): Response
    {
        $serviceManager->deleteMultiple(
            $request->request->get('delete')
        );

        $this->addFlash('success', 'service.success_message.delete');

        return $this->redirectToRoute('app_service_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/association",
     *     name="app_service_association",
     *     methods={"POST"}
     * )
     */
    public function association(Request $request, ServiceManager $serviceManager): Response
    {
        $form = $this->createForm(UsersForOrganisationType::class, null, ['userCurrentOrganisation' => $this->getUser()->getCurrentOrganisation()])
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceManager->association($form->getdata());

            $this->addFlash('success', 'service.success_message.save');
        }

        return $this->redirectToRoute('app_service_index', [
            'page' => $request->get('page')
        ]);
    }
}
