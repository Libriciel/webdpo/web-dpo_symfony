<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Security\Password\ResetPassword;
use App\Service\User\UserManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Doctrine\ORM\EntityNotFoundException;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/superadmin")
 *
 * @IsGranted("ROLE_SUPERADMIN")
 * @Sidebar(active={"user-nav"})
 */
class SuperadminController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_superadmin_index",
     *     methods={"GET"}
     * )
     *
     * @Breadcrumb("Liste des superadmin de l'application ", routeName="app_superadmin_index")
     * @Sidebar(active={"user-superadmin-nav"})
     */
    public function index(Request $request, UserRepository $userRepository, PaginatorInterface $paginator): Response
    {
        $users = $paginator->paginate(
            $userRepository->findAllSuperadmin(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['u.lastName'],
                'defaultSortDirection' => 'asc'
            ]
        );

        return $this->render('superadmin/index.html.twig', [
            'controller_name' => 'UserController',
            'users' => $users,
        ]);
    }

    /**
     * @Route(
     *     "/add",
     *     name="app_superadmin_add"
     * )
     *
     * @Breadcrumb("Liste des superadmin de l'application ", routeName="app_superadmin_add")
     * @Breadcrumb("Ajout d'un superadmin")
     * @Sidebar(active={"user-superadmin-nav"})
     */
    public function add(Request $request, UserManager $userManager): Response
    {
        $form = $this->createForm(UserType::class, new User(), ['isAdminMode' => true])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->saveSuperadmin(
                $form->getdata()
            );

            $this->addFlash('success', 'user.success_message.save');

            return $this->redirectToRoute('app_superadmin_index');
        }

        return $this->render('superadmin/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajout d\'un superadmin'
        ]);
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name="app_superadmin_edit",
     *      requirements={"id": "%routing.uuid%"}
     * )
     *
     * @Breadcrumb("Liste des superadmin de l'application ", routeName="app_superadmin_index")
     * @Breadcrumb("Modifier un superadmin")
     * @Sidebar(active={"user-superadmin-nav"})
     */
    public function edit(User $user, Request $request, UserManager $userManager): Response
    {
        $form = $this->createForm(UserType::class, $user, ['isAdminMode' => true, 'isEditMode' => true])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->save(
                $form->getdata()
            );

            $this->addFlash('success', 'user.success_message.save');

            return $this->redirectToRoute('app_superadmin_index');
        }

        return $this->render('superadmin/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modification d\'un superadmin'
        ]);
    }

    /**
     * @Route(
     *      "/show/{id}",
     *      name="app_superadmin_show",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"GET"}
     * )
     *
     * @Breadcrumb("Liste des utilisateurs de l'application ", routeName="app_superadmin_index")
     * @Breadcrumb("Visualisation de l'utilisateur")
     * @Sidebar(active={"user-all-nav"})
     */
    public function show(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user, ['isAdminMode' => true, 'isEditMode' => true, 'userLogged' => $this->getUser()])
            ->handleRequest($request);

        return $this->render('superadmin/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Visualisation de l\'utilisateur'
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_superadmin_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(User $user, UserManager $userManager, Request $request): Response
    {
        if ($this->getUser()->getId() == $user->getId()) {
            $this->addFlash('error', 'Impossible de supprimer son propre utilisateur');

            return $this->redirectToRoute('app_superadmin_index');
        }

        $userManager->delete($user);
        $this->addFlash('success', 'Le superadmin a bien été supprimé');

        return $this->redirectToRoute('app_superadmin_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/toggle",
     *     name="app_superadmin_toggle",
     *     methods={"POST"}
     * )
     */
    public function toggle(Request $request, UserManager $userManager): Response
    {
        if (array_key_exists($this->getUser()->getId(), $request->request->get('toggle'))) {
            $this->addFlash('error', 'Impossible de modifié son propre utilisateur');

            return $this->redirectToRoute('app_superadmin_index');
        }

        $userManager->toggle(
            $request->request->get('toggle')
        );

        $this->addFlash('success', 'Les utilisateurs ont bien été modifiés');

        return $this->redirectToRoute('app_superadmin_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/reaload_password",
     *     name="app_superadmin_reload_password",
     *     methods={"POST"}
     * )
     */
    public function reloadPassword(Request $request, UserRepository $userRepository, ResetPassword $resetPassword): Response
    {
        foreach ($request->request->get('reload') as $userSelected) {
            $user = $userRepository->findOneBy([
                'id' => $userSelected
            ]);

            if (empty($user)) {
                $this->addFlash('error', 'L\'utilisateur n\'a pas été trouvé');

                return $this->redirectToRoute('app_user_index', [
                    'page' => $request->get('page')
                ]);
            }

            $resetPassword->reset(
                $user,
                true
            );
        }

        $this->addFlash(
            'success',
            'Un e-mail de réinitialisation du mot de passe a été envoyé à l\'utilisateur'
        );

        return $this->redirectToRoute('app_superadmin_index', [
            'page' => $request->get('page')
        ]);
    }
}
