<?php

namespace App\Controller;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/check')]
#[IsGranted(data: 'ROLE_SUPERADMIN')]
#[Breadcrumb(title: 'Accueil', routeName: 'app_check_index')]
class CheckController extends AbstractController
{
    #[Route(path: '', name: 'app_check_index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('check/index.html.twig', [
            'controller_name' => 'CheckController',
        ]);
    }
}
