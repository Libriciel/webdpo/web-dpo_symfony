<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Form\MultiOrganisationUserType;
use App\Repository\ArticleRepository;
use App\Service\Article\ArticleManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/article')]
#[Breadcrumb(title: 'FAQ', routeName: 'app_article_index')]
class ArticleController extends AbstractController
{
    #[Route(path: '', name: 'app_article_index', methods: ['GET'])]
    //    #[Sidebar(active: ['faq-nav', 'all-faq-index-nav'])]
    public function index(Request $request, ArticleRepository $articleRepository, PaginatorInterface $paginator): Response
    {
        $articles = $paginator->paginate(
            $articleRepository->findAllArticles(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['a.name'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        $form = $this->createForm(MultiOrganisationUserType::class);

        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
            'articles' => $articles,
            'form' => $form->createView(),
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/entity', name: 'app_article_entity', methods: ['GET'])]
//    #[Sidebar(active: ['faq-nav', 'faq-index-nav'])]
    public function entity(Request $request, PaginatorInterface $paginator, ArticleRepository $articleRepository): Response
    {
        $articles = $paginator->paginate(
            $articleRepository->getAllArticlesForCurrentOrganisation(
                $this->getUser()->getCurrentOrganisation()
            ),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['a.name'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        return $this->render('article/entity.html.twig', [
            'controller_name' => 'ArticleController',
            'articles' => $articles,
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/faq', name: 'app_article_faq', methods: ['GET'])]
    public function faq(Request $request, ArticleRepository $articleRepository, PaginatorInterface $paginator): Response
    {
        $articles = $paginator->paginate(
            $articleRepository->getAllArticlesForCurrentOrganisation(
                $this->getUser()->getCurrentOrganisation()
            ),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['a.name'],
                'defaultSortDirection' => 'asc'
            ]
        );

        return $this->render('article/faq.html.twig', [
            'controller_name' => 'ArticleController',
            'articles' => $articles,
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/add', name: 'app_article_add', methods: ['GET', 'POST'])]
//    #[Sidebar(active: ['faq-nav', 'faq-index-nav'])]
    public function add(Request $request, ArticleManager $articleManager): Response
    {
        $form = $this->createForm(ArticleType::class)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->add(
                $form->getdata(),
                $this->getUser()->getCurrentOrganisation(),
                $form->get('files')->getData()
            );

            $this->addFlash('success', 'article.success_message.save');

            return $this->redirectToRoute('app_article_index');
        }

        return $this->render('article/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_article_edit', requirements: ['id' => '%routing.uuid%'], methods: ['GET', 'POST'])]
//    #[Sidebar(active: ['faq-nav', 'all-faq-index-nav'])]
    public function edit(Article $article, Request $request, ArticleManager $articleManager): Response
    {
        $form = $this->createForm(ArticleType::class, $article)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->save($form->getdata());

            $this->addFlash('success', 'article.success_message.save');

            return $this->redirectToRoute('app_article_index');
        }

        return $this->render('article/edit.html.twig', [
            'form' => $form->createView(),
            'article' => $article
        ]);
    }

    #[Route(path: '/show/{id}', name: 'app_article_show', requirements: ['id' => '%routing.uuid%'], methods: ['GET'])]
    #[Breadcrumb(title: 'Visualiser')]
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', [
            'article' => $article
        ]);
    }

    #[Route(path: '/association', name: 'app_article_association', methods: ['POST'])]
    public function association(Request $request, ArticleManager $articleManager): Response
    {
        $form = $this->createForm(MultiOrganisationUserType::class)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->association($form->getdata());

            $this->addFlash('success', 'article.success_message.save');
        }

        return $this->redirectToRoute('app_article_index', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/dissociation/{id}', name: 'app_article_dissociation', requirements: ['id' => '%routing.uuid%'], methods: ['POST'])]
    public function dissociation(Article $article, Request $request, ArticleManager $articleManager): Response
    {
        $articleManager->dissociation(
            $article,
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'article.success_message.save');

        return $this->redirectToRoute('app_article_entity', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/dissociationmultiple', name: 'app_article_dissociation_multiple')]
    public function dissociationMultiple(Request $request, ArticleManager $articleManager): Response
    {
        $articleManager->dissociationMultiple(
            $request->request->get('dissociation'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'article.success_message.save');

        return $this->redirectToRoute('app_article_entity', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/delete/{id}', name: 'app_article_delete', requirements: ['id' => '%routing.uuid%'], methods: ['DELETE'])]
    public function delete(Article $article, ArticleManager $articleManager, Request $request): Response
    {
        $articleManager->delete(
            $article,
            $this->getUser()->getCurrentOrganisation()
        );
        $this->addFlash('success', 'article.success_message.delete');

        return $this->redirectToRoute('app_article_index', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/deletemultiple', name: 'app_article_delete_multiple', methods: ['DELETE'])]
    public function deleteMultiple(Request $request, ArticleManager $articleManager): Response
    {
        $articleManager->deleteMultiple(
            $request->request->get('delete'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'article.success_message.save');

        return $this->redirectToRoute('app_article_index', [
            'page' => $request->get('page')
        ]);
    }
}
