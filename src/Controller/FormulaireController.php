<?php

namespace App\Controller;

use App\Entity\Formulaire;
use App\Form\FormulaireType;
use App\Form\MultiOrganisationUserType;
use App\Repository\FormulaireRepository;
use App\Service\Formulaire\FormulaireManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/formulaire")
 *
 * @Breadcrumb("Formulaire", routeName="app_formulaire_index")
 * @Sidebar(active={"formulaire-nav"})
 */
class FormulaireController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_formulaire_index",
     *     methods={"GET"}
     * )
     *
     * @Sidebar(active={"all-formulaire-index-nav"})
     */
    public function index(Request $request, FormulaireRepository $formulaireRepository, PaginatorInterface $paginator): Response
    {
        $formulaires = $paginator->paginate(
            $formulaireRepository->findAllFormulaires(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['f.name'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        $form = $this->createForm(MultiOrganisationUserType::class);

        $addFormulaire = $this->createForm(FormulaireType::class);

        return $this->render('formulaire/index.html.twig', [
            'controller_name' => 'FormulaireController',
            'form' => $form->createView(),
            'addFormulaire' => $addFormulaire->createView(),
            'formulaires' => $formulaires,
            'page' => $request->get('page'),
        ]);
    }

    /**
     * @Route(
     *     "/entity",
     *     name="app_formulaire_entity",
     *     methods={"GET"}
     * )
     *
     * @Sidebar(active={"formulaire-index-nav"})
     */
    public function entity(Request $request, PaginatorInterface $paginator, FormulaireRepository $formulaireRepository): Response
    {
        $formulaires = $paginator->paginate(
            $formulaireRepository->getAllFormulairesForCurrentOrganisation(
                $this->getUser()->getCurrentOrganisation()
            ),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['f.name'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        return $this->render('formulaire/entity.html.twig', [
            'controller_name' => 'FormulaireController',
            'formulaires' => $formulaires,
        ]);
    }

    /**
     * @Route(
     *     "/add",
     *     name="app_formulaire_add",
     *     methods={"GET", "POST"}
     * )
     *
     * @Sidebar(active={"all-formulaire-nav"})
     */
    public function add(Request $request, FormulaireManager $formulaireManager): Response
    {
        $form = $this->createForm(FormulaireType::class)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formulaireId = $formulaireManager->add(
                $form->getdata(),
                $this->getUser()->getCurrentOrganisation(),
                $form->get('registre')->getData()
            );

            $this->addFlash('success', 'formulaire.success_message.save');

            return $this->redirectToRoute('app_formulaire_edit', ['id' => $formulaireId]);
            //            return $this->redirectToRoute('app_formulaire_edit_information', ['id' => $formulaireId]);
        }

        return $this->render('formulaire/add.html.twig', [
            'form' => $form->createView(),
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/edit/{id}",
     *     name="app_formulaire_edit",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"GET", "POST"}
     * )
     *
     * @Sidebar(active={"all-formulaire-index-nav"})
     */
    public function edit(Formulaire $formulaire, Request $request, FormulaireManager $formulaireManager): Response
    {
        if ($formulaire->getIsArchived()) {
            throw new InvalidArgumentException('Impossible de modifier le formulaire archivée');
        }

        if ($formulaire->getCreatedByOrganisation() !== $this->getUser()->getCurrentOrganisation()) {
            throw new InvalidArgumentException('Impossible de modifier le formulaire d\'une autre entité');
        }

        $form = $this->createForm(
            FormulaireType::class,
            $formulaire,
            [
                'isEditMode' => true,
                'isActivite' => $formulaire->getIsActivite(),
                'isRtExterne' => $formulaire->getIsRtExterne(),
                'isViolation' => $formulaire->getIsViolation(),
            ]
        )
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formulaireManager->save(
                $form->getdata(),
                $form->get('tests')->getData()
            );

            $this->addFlash('success', 'formulaire.success_message.save');

            return $this->redirectToRoute('app_formulaire_index');
        }

        $viewEdit = null;
        if ($formulaire->getIsActivite() === true) {
            $viewEdit = 'formulaire/edit_activite.html.twig';
        } elseif ($formulaire->getIsRtExterne() === true) {
            $viewEdit = 'formulaire/edit_rt_externe.html.twig';
        } elseif ($formulaire->getIsViolation() === true) {
            $viewEdit = 'formulaire/edit_violation.html.twig';
        }

        return $this->render($viewEdit, [
            'form' => $form->createView(),
            'formulaire' => $formulaire
        ]);
    }

    /**
     * @Route(
     *     "/toggle",
     *     name="app_formulaire_toggle",
     *     methods={"POST"}
     * )
     */
    public function toggle(Request $request, FormulaireManager $formulaireManager): Response
    {
        $formulaireManager->toggle(
            $request->request->get('toggle')
        );

        $this->addFlash('success', 'Les formulaires ont bien été modifiés');

        return $this->redirectToRoute('app_formulaire_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/association",
     *     name="app_formulaire_association",
     *     methods={"POST"}
     * )
     */
    public function association(Request $request, FormulaireManager $formulaireManager): Response
    {
        $form = $this->createForm(MultiOrganisationUserType::class)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formulaireManager->association($form->getdata());

            $this->addFlash('success', 'formulaire.success_message.save');
        }

        return $this->redirectToRoute('app_formulaire_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/dissociation/{id}",
     *     name="app_formulaire_dissociation",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"POST"}
     * )
     */
    public function dissociation(Formulaire $formulaire, Request $request, FormulaireManager $formulaireManager): Response
    {
        $formulaireManager->dissociation(
            $formulaire,
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'formulaire.success_message.save');

        return $this->redirectToRoute('app_formulaire_entity', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/dissociationmultiple",
     *     name="app_formulaire_dissociation_multiple"
     * )
     */
    public function dissociationMultiple(Request $request, FormulaireManager $formulaireManager): Response
    {
        $formulaireManager->dissociationMultiple(
            $request->request->get('dissociation'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'formulaire.success_message.save');

        return $this->redirectToRoute('app_formulaire_entity', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_formulaire_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Formulaire $formulaire, FormulaireManager $formulaireManager, Request $request): Response
    {
        $formulaireManager->delete(
            $formulaire,
            $this->getUser()->getCurrentOrganisation()
        );
        $this->addFlash('success', 'formulaire.success_message.delete');

        return $this->redirectToRoute('app_formulaire_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/deletemultiple",
     *     name="app_formulaire_delete_multiple",
     *     methods={"DELETE"}
     * )
     */
    public function deleteMultiple(Request $request, FormulaireManager $formulaireManager): Response
    {
        $formulaireManager->deleteMultiple(
            $request->request->get('delete'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'formulaire.success_message.save');

        return $this->redirectToRoute('app_formulaire_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/archived",
     *     name="app_formulaire_archived",
     *     methods={"POST"}
     * )
     */
    public function archived(Request $request, FormulaireManager $formulaireManager): Response
    {
        $formulaireManager->archived(
            $request->request->get('archived')
        );

        $this->addFlash('success', 'Les formulaires ont bien été archivés');

        return $this->redirectToRoute('app_formulaire_index', [
            'page' => $request->get('page')
        ]);
    }
}
