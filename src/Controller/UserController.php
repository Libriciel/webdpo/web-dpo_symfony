<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserPreferenceType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Security\Password\ResetPassword;
use App\Service\User\UserManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 *
 * @IsGranted("ROLE_MANAGE_USERS")
 * @Sidebar(active={"user-nav"})
 */
class UserController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_user_index",
     *     methods={"GET"}
     * )
     *
     * @Breadcrumb("Liste des utilisateurs de l'application")
     * @Sidebar(active={"user-all-nav"})
     */
    public function index(Request $request, PaginatorInterface $paginator, UserManager $userManager): Response
    {
        $users = $paginator->paginate(
            $userManager->getUsers($this->getUser()),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['u.firstName', 'u.lastName'],
                'defaultSortDirection' => 'asc'
            ]
        );

        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            'users' => $users,
        ]);
    }

    /**
     * @Route(
     *     "/add",
     *     name="app_user_add",
     *     methods={"GET", "POST"}
     * )
     *
     * @Breadcrumb("Liste des utilisateurs de l'application ", routeName="app_user_index")
     * @Breadcrumb("Ajout d'un utilisateur")
     * @Sidebar(active={"user-all-nav"})
     */
    public function add(Request $request, UserManager $userManager): Response
    {
        $form = $this->createForm(UserType::class, null, ['userLogged' => $this->getUser()])
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->add(
                $form->getdata()
            );

            $this->addFlash('success', 'user.success_message.save');

            return $this->redirectToRoute('app_user_index');
        }

        return $this->render('user/add.html.twig', [
            'form' => $form->createView(),
            'title' => "Ajouter un utilisateur"
        ]);
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name="app_user_edit",
     *      requirements={"id": "%routing.uuid%"}
     * )
     *
     * @Breadcrumb("Liste des utilisateurs de l'application ", routeName="app_user_index")
     * @Breadcrumb("Modifier l'utilisateur")
     * @Sidebar(active={"user-all-nav"})
     */
    public function edit(User $user, Request $request, UserManager $userManager): Response
    {
        $form = $this->createForm(UserType::class, $user, [
            'isEditMode' => true,
            'userLogged' => $this->getUser()
        ])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->save(
                $form->getdata()
            );

            $this->addFlash('success', 'user.success_message.save');

            return $this->redirectToRoute('app_user_index');
        }

        return $this->render('user/add.html.twig', [
            'form' => $form->createView(),
            'title' => "Modifier un utilisateur"
        ]);
    }

    /**
     * @Route(
     *      "/show/{id}",
     *      name="app_user_show",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"GET"}
     * )
     *
     * @Breadcrumb("Liste des utilisateurs de l'application ", routeName="app_user_index")
     * @Breadcrumb("Visualisation de l'utilisateur")
     * @Sidebar(active={"user-all-nav"})
     */
    public function show(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user, ['isEditMode' => true, 'userLogged' => $this->getUser()])
            ->handleRequest($request);

        return $this->render('user/add.html.twig', [
            'form' => $form->createView(),
            'title' => "Visualisation d'un utilisateur"
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_user_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(User $user, UserManager $userManager, Request $request): Response
    {
        if ($this->getUser()->getId() == $user->getId()) {
            $this->addFlash('error', 'Impossible de supprimer son propre utilisateur');

            return $this->redirectToRoute('app_user_index', [
                'page' => $request->get('page')
            ]);
        }

        if ($user->getIsActive() === true) {
            $this->addFlash('error', 'Impossible de supprimer un utilisateur ACTIF');

            return $this->redirectToRoute('app_user_index', [
                'page' => $request->get('page')
            ]);
        }

        $userManager->delete($user);
        $this->addFlash('success', 'L\'utilisateur a bien été supprimé');

        return $this->redirectToRoute('app_user_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/deletemultiple",
     *     name="app_user_delete_multiple",
     *     methods={"DELETE"}
     * )
     */
    public function deleteMultiple(Request $request, UserManager $userManager): Response
    {
        if (in_array($this->getUser()->getId(), $request->request->get('delete'))) {
            $this->addFlash('error', 'Impossible de supprimer son propre utilisateur');

            return $this->redirectToRoute('app_user_index');
        }

        $userManager->deleteMultiple(
            $request->request->get('delete')
        );

        $this->addFlash('success', 'user.success_message.delete');

        return $this->redirectToRoute('app_user_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/preference",
     *     name="app_user_preference",
     *     methods={"GET", "POST"}
     * )
     */
    public function preference(Request $request, UserManager $userManager): Response
    {
        $form = $this->createForm(UserPreferenceType::class, $this->getUser())
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->save($form->getdata());

            $this->addFlash('success', 'Vos informations personnelles, ont bien été enregistré');

            return $this->redirectToRoute('app_entrypoint');
        }

        return $this->render('user/preference.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *     "/toggle",
     *     name="app_user_toggle",
     *     methods={"POST"}
     * )
     */
    public function toggle(Request $request, UserManager $userManager): Response
    {
        if (in_array($this->getUser()->getId(), $request->request->get('toggle'))) {
            $this->addFlash('error', 'Impossible de modifié son propre utilisateur');

            return $this->redirectToRoute('app_user_index');
        }

        $userManager->toggle(
            $request->request->get('toggle')
        );

        $this->addFlash('success', 'Les utilisateurs ont bien été modifiés');

        return $this->redirectToRoute('app_user_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/reaload_password",
     *     name="app_user_reload_password",
     *     methods={"POST"}
     * )
     *
     * @IsGranted("ROLE_MANAGE_PASSWORD")
     */
    public function reloadPassword(Request $request, UserRepository $userRepository, ResetPassword $resetPassword): Response
    {
        foreach ($request->request->get('reload') as $userSelected) {
            $user = $userRepository->findOneBy([
                'id' => $userSelected
            ]);

            if (empty($user)) {
                $this->addFlash('error', 'L\'utilisateur n\'a pas été trouvé');

                return $this->redirectToRoute('app_user_index', [
                    'page' => $request->get('page')
                ]);
            }

            $resetPassword->reset(
                $user,
                true
            );
        }

        $this->addFlash(
            'success',
            'Un e-mail de réinitialisation du mot de passe a été envoyé aux utilisateurs'
        );

        return $this->redirectToRoute('app_user_index', [
            'page' => $request->get('page')
        ]);
    }
}
