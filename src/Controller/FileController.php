<?php

namespace App\Controller;

use App\Entity\File;
use App\Service\File\FileManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/file")
 */
class FileController extends AbstractController
{
    /**
     * @Route(
     *     "/download/{id}",
     *     name="app_file_download",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"GET"}
     * )
     */
    public function download(File $file, FileManager $fileManager): Response
    {
        $path = $fileManager->getRootPath() . $file->getPath();

        $response = new BinaryFileResponse($path);

        return $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $file->getFileName()
        );
    }
}
