<?php

namespace App\Controller;

use App\Entity\Coresponsable;
use App\Form\CoresponsableType;
use App\Form\MultiOrganisationUserType;
use App\Repository\CoresponsableRepository;
use App\Service\Coresponsable\CoresponsableManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/coresponsable')]
#[Breadcrumb(title: 'Les co-responsable de l\'application', routeName: 'app_coresponsable_index')]
class CoresponsableController extends AbstractController
{
    #[Route(path: '', name: 'app_coresponsable_index', methods: ['GET'])]
    //    #[Sidebar(active: ['all-coresponsable-index-nav'])]
    public function index(Request $request, PaginatorInterface $paginator, CoresponsableRepository $coresponsableRepository): Response
    {
        $coresponsables = $paginator->paginate(
            $coresponsableRepository->findCoresponsables(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['c.raisonsociale'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        $form = $this->createForm(MultiOrganisationUserType::class);

        return $this->render('coresponsable/index.html.twig', [
            'controller_name' => 'CoresponsableController',
            'coresponsables' => $coresponsables,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/entity', name: 'app_coresponsable_entity', methods: ['GET'])]
    //    #[Sidebar(active: ['coresponsable-index-nav'])]
    public function entity(Request $request, PaginatorInterface $paginator, CoresponsableRepository $coresponsableRepository): Response
    {
        $coresponsables = $paginator->paginate(
            $coresponsableRepository->getAllCoresponsablesForCurrentOrganisation(
                $this->getUser()->getCurrentOrganisation()
            ),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['c.raisonsociale'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        return $this->render('coresponsable/entity.html.twig', [
            'controller_name' => 'CoresponsableController',
            'coresponsables' => $coresponsables,
        ]);
    }

    #[Route(path: '/add', name: 'app_coresponsable_add', methods: ['GET', 'POST'])]
    public function add(Request $request, CoresponsableManager $coresponsableManager): Response
    {
        $form = $this->createForm(CoresponsableType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $coresponsableManager->add(
                $form->getdata(),
                $form->get('logo')->getData()
            );

            $this->addFlash('success', 'coresponsable.success_message.save');

            return $this->redirectToRoute('app_coresponsable_index');
        }

        return $this->render('coresponsable/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Ajouter un co-responsable'
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_coresponsable_edit', requirements: ['id' => '%routing.uuid%'], methods: ['GET', 'POST'])]
    #[Breadcrumb(title: 'Modifier le co-responsable')]
    public function edit(Coresponsable $coresponsable, Request $request, CoresponsableManager $coresponsableManager): Response
    {
        if (count($coresponsable->getOrganisations()) !== 0) {
            $this->addFlash('error', 'Impossible de modifier le co-responsable');

            return $this->redirectToRoute('app_soustraitant_index');
        }

        $form = $this->createForm(CoresponsableType::class, $coresponsable)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $coresponsableManager->save(
                $form->getdata()
            );

            $this->addFlash('success', 'coresponsable.success_message.save');

            return $this->redirectToRoute('app_coresponsable_index');
        }

        return $this->render('coresponsable/add.html.twig', [
            'form' => $form->createView(),
            'title' => 'Modifier un co-responsable'
        ]);
    }

    #[Route(path: '/delete/{id}', name: 'app_coresponsable_delete', requirements: ['id' => '%routing.uuid%'], methods: ['DELETE'])]
    public function delete(Coresponsable $coresponsable, CoresponsableManager $coresponsableManager, Request $request): Response
    {
        if (count($coresponsable->getOrganisations()) !== 0) {
            $this->addFlash('error', 'Impossible de supprimer le co-responsable');

            return $this->redirectToRoute('app_coresponsable_index');
        }

        $coresponsableManager->delete($coresponsable);
        $this->addFlash('success', 'Le co-responsable a bien été supprimé');

        return $this->redirectToRoute('app_coresponsable_index', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/deletemultiple', name: 'app_coresponsable_delete_multiple', methods: ['DELETE'])]
    public function deleteMultiple(Request $request, CoresponsableManager $coresponsableManager): Response
    {
        $coresponsableManager->deleteMultiple(
            $request->request->get('delete')
        );

        $this->addFlash('success', 'coresponsable.success_message.save');

        return $this->redirectToRoute('app_coresponsable_index', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/association', name: 'app_coresponsable_association', methods: ['POST'])]
    public function association(Request $request, CoresponsableManager $coresponsableManager): Response
    {
        $form = $this->createForm(MultiOrganisationUserType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $coresponsableManager->association($form->getdata());

            $this->addFlash('success', 'coresponsable.success_message.save');
        }

        return $this->redirectToRoute('app_coresponsable_index', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/dissociation/{id}', name: 'app_coresponsable_dissociation', requirements: ['id' => '%routing.uuid%'], methods: ['POST'])]
    public function dissociation(Coresponsable $coresponsable, Request $request, CoresponsableManager $coresponsableManager): Response
    {
        $coresponsableManager->dissociation(
            $coresponsable,
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'coresponsable.success_message.save');

        return $this->redirectToRoute('app_coresponsable_entity', [
            'page' => $request->get('page')
        ]);
    }

    #[Route(path: '/dissociationmultiple/{id}', name: 'app_coresponsable_dissociation_multiple', methods: ['POST'])]
    public function dissociationMultiple(Request $request, CoresponsableManager $coresponsableManager): Response
    {
        $coresponsableManager->dissociationMultiple(
            $request->request->get('dissociation'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'coresponsable.success_message.save');

        return $this->redirectToRoute('app_coresponsable_entity', [
            'page' => $request->get('page')
        ]);
    }
}
