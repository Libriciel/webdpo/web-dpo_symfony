<?php

namespace App\Controller;

use App\Entity\Typage;
use App\Form\MultiOrganisationUserType;
use App\Form\TypageType;
use App\Repository\OrganisationRepository;
use App\Repository\TypageRepository;
use App\Service\Typage\TypageManager;
use App\Sidebar\Annotation\Sidebar;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/typage")
 *
 * @IsGranted("ROLE_MANAGE_ORGANISATIONS")
 *
 * @Sidebar(active={"typage-nav"})
 */
class TypageController extends AbstractController
{
    /**
     * @Route(
     *     "",
     *     name="app_typage_index",
     *     methods={"GET"}
     * )
     *
     * @Sidebar(active={"all-typage-index-nav"})
     */
    public function index(Request $request, PaginatorInterface $paginator, TypageRepository $typageRepository): Response
    {
        $typages = $paginator->paginate(
            $typageRepository->getAllTypages(),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['t.name'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        $form = $this->createForm(MultiOrganisationUserType::class);

        return $this->render('typage/index.html.twig', [
            'controller_name' => 'TypageController',
            'typages' => $typages,
            'form' => $form->createView(),
            'page' => $request->get('page'),
        ]);
    }

    /**
     * @Route(
     *     "/entity",
     *     name="app_typage_entity",
     *     methods={"GET"}
     * )
     *
     * @Sidebar(active={"typage-index-nav"})
     */
    public function entity(Request $request, PaginatorInterface $paginator, TypageRepository $typageRepository): Response
    {
        $typages = $paginator->paginate(
            $typageRepository->getAllTypagesForCurrentOrganisation($this->getUser()->getCurrentOrganisation()),
            $request->query->getInt('page', 1),
            $this->getParameter('limit_line_table'),
            [
                'defaultSortFieldName' => ['t.name'],
                'defaultSortDirection' => 'ASC'
            ]
        );

        return $this->render('typage/entity.html.twig', [
            'controller_name' => 'SoustraitantController',
            'typages' => $typages,
            'page' => $request->get('page'),
        ]);
    }

    /**
     * @Route(
     *     "/add",
     *     name="app_typage_add",
     *     methods={"GET", "POST"}
     * )
     */
    public function add(Request $request, TypageManager $typageManager): Response
    {
        $form = $this->createForm(TypageType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $typageManager->add(
                $form->getdata(),
                $this->getUser()->getCurrentOrganisation()
            );

            $this->addFlash('success', 'typage.success_message.save');

            return $this->redirectToRoute('app_typage_index');
        }

        return $this->render('typage/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *     "/edit/{id}",
     *     name="app_typage_edit",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"GET", "POST"}
     * )
     */
    public function edit(Typage $typage, Request $request, TypageManager $typageManager): Response
    {
        if (count($typage->getOrganisations()) !== 0) {
            $this->addFlash('error', 'Impossible de modifier le type d\'annexe');

            return $this->redirectToRoute('app_soustraitant_index');
        }

        $form = $this->createForm(TypageType::class, $typage)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $typageManager->save($form->getdata());

            $this->addFlash('success', 'typage.success_message.save');

            return $this->redirectToRoute('app_typage_index');
        }

        return $this->render('typage/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="app_typage_delete",
     *      requirements={"id": "%routing.uuid%"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Typage $typage, TypageManager $typageManager, Request $request): Response
    {
        $typageManager->delete(
            $typage,
            $this->getUser()->getCurrentOrganisation()
        );
        $this->addFlash('success', 'typage.success_message.delete');

        return $this->redirectToRoute('app_typage_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/deletemultiple",
     *     name="app_typage_delete_multiple",
     *     methods={"DELETE"}
     * )
     */
    public function deleteMultiple(Request $request, TypageManager $typageManager): Response
    {
        $typageManager->deleteMultiple(
            $request->request->get('delete'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'typage.success_message.save');

        return $this->redirectToRoute('app_typage_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/association",
     *     name="app_typage_association",
     *     methods={"POST"}
     * )
     */
    public function association(Request $request, TypageManager $typageManager): Response
    {
        $form = $this->createForm(MultiOrganisationUserType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $typageManager->association($form->getdata());

            $this->addFlash('success', 'typage.success_message.save');
        }

        return $this->redirectToRoute('app_typage_index', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/dissociation/{id}",
     *     name="app_typage_dissociation",
     *     requirements={"id": "%routing.uuid%"},
     *     methods={"POST"}
     * )
     */
    public function dissociation(Typage $typage, Request $request, TypageManager $typageManager): Response
    {
        $typageManager->dissociation(
            $typage,
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'typage.success_message.save');

        return $this->redirectToRoute('app_typage_entity', [
            'page' => $request->get('page')
        ]);
    }

    /**
     * @Route(
     *     "/dissociationmultiple",
     *     name="app_typage_dissociation_multiple"
     * )
     */
    public function dissociationMultiple(Request $request, TypageManager $typageManager): Response
    {
        $typageManager->dissociationMultiple(
            $request->request->get('dissociation'),
            $this->getUser()->getCurrentOrganisation()
        );

        $this->addFlash('success', 'typage.success_message.save');

        return $this->redirectToRoute('app_typage_entity', [
            'page' => $request->get('page')
        ]);
    }
}
