<?php

namespace App\Controller;

use App\Form\CsvType;
use App\Service\Csv\CsvManager;
use App\Sidebar\Annotation\Sidebar;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Breadcrumb("Liste des services de l'entité", routeName="app_service_index")
 * @Sidebar(active={"user-nav"})
 * @Sidebar(active={"service-all-nav"})
 */
class CsvController extends AbstractController
{
    /**
     * @Route(
     *     "/csv/importServices",
     *     name="app_csv_import",
     * )
     *
     * @Breadcrumb("Importer des servicess via csv")
     */
    public function importServices(Request $request, Session $session, CsvManager $csvManager): Response
    {
        $form = $this->createForm(CsvType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $errors = $csvManager->importServices(
                $form->get('csv')->getData(),
                $this->getUser()->getCurrentOrganisation()
            );

            if (empty($errors)) {
                $this->addFlash('success', 'Fichier csv importé avec succès');

                return $this->redirectToRoute('app_service_index');
            }

            $session->set('errors_csv', $errors);

            return $this->redirectToRoute('app_csv_error');
        }

        return $this->render('csv/importCsv.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/csv/errors",
     *     name="app_csv_error"
     * )
     *
     * @Breadcrumb("Erreurs lors de l'import")
     */
    public function csvError(Session $session): Response
    {
        $errors = $session->get('errors_csv');

        if (empty($errors)) {
            return $this->redirectToRoute('app_service_index');
        }

        $session->remove('errors_csv');

        return $this->render('csv/importCsvErrors.html.twig', [
            'errors' => $errors,
        ]);
    }
}
