<?php

namespace App\Controller;

use App\Form\SelectOrganisationType;
use App\Form\UserPasswordType;
use App\Repository\UserRepository;
use App\Security\Password\ResetPassword;
use App\Security\Password\TimeoutException;
use Doctrine\ORM\EntityNotFoundException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route(path: '/', name: 'app_entrypoint')]
    public function entryPoint(Security $security): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        //        if (Role::NAME_ROLE_ACTOR === $this->getUser()->getRole()->getName()) {
        //            return $this->render('security/noActors.html.twig');
        //        }

        if ($this->isGranted('ROLE_SUPERADMIN')) {
            return $this->redirectToRoute('app_check_index');
        }

        return $this->redirectToRoute('app_traitement_index');
    }

    #[Route(path: '/login', name: 'app_login', methods: ['GET', 'POST'])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        if ($error) {
            $this->addFlash('error', "erreur d\'identification");
        }
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/Login_ls.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    #[Route(path: '/logout', name: 'app_logout', methods: ['GET'])]
    public function logout()
    {
        // Fonction de déconnexion de l'utilisateur
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/reset/{token}', name: 'app_reset', methods: ['GET', 'POST'])]
    public function resetPassword(string $token, ResetPassword $resetPassword, Request $request): Response
    {
        try {
            $user = $resetPassword->getUserFromToken($token);
        } catch (TimeoutException $e) {
            throw new TimeoutException('expired TOKEN', Response::HTTP_BAD_REQUEST);
        } catch (EntityNotFoundException $e) {
            throw new NotFoundHttpException('this token does not exist');
        }

        $form = $this->createForm(UserPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resetPassword->setNewPassword($user, $form->get('plainPassword')->getData());
            $this->addFlash('success', 'Modifiée avec succès');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/reset_ls.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/forget', name: 'app_forget', methods: ['GET', 'POST'])]
    public function forgetPassword(Request $request, UserRepository $userRepository, ResetPassword $resetPassword): Response
    {
        if ($request->isMethod('post')) {
            $user = $userRepository->findOneBy([
                'username' => $request->request->get('username'),
                'email' => $request->request->get('email')
            ]);

            $resetPassword->reset($user);

            $this->addFlash('success', 'Un email vous a été envoyé si un compte lui est associé');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/forget_ls.html.twig');
    }
}
