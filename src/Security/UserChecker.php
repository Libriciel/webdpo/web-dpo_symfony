<?php

namespace App\Security;

use App\Repository\OrganisationRepository;
use App\Service\User\UserManager;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function __construct(
        private readonly OrganisationRepository $organisationRepository,
        private readonly UserManager $userManager
    ) {
    }

    public function checkPreAuth(UserInterface $user)
    {
        if ($user->getIsActive() === false) {
            throw new CustomUserMessageAccountStatusException('Your user account no longer exists.');
        }

        $this->userManager->currentOrganisationLogout($user);

        if ($user->getIsSuperadmin() === false) {
            $userOrganisations = $this->organisationRepository->findOrganisationByUser($user);
            $countNbUserOrganisations = count($userOrganisations);

            if (empty($userOrganisations)) {
                throw new CustomUserMessageAuthenticationException("You're banned !");
            }

            //            if ($countNbUserOrganisations >= 2) {
            //                dd("CHANGE ORGANISATION AFTER LOGIN");
            //            } else {
            //                $this->userManager->addCurrentOrganisationLogin($user, $userOrganisations[0]);
            //            }

            $this->userManager->addCurrentOrganisationLogin($user, $userOrganisations[0]);
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
    }
}
