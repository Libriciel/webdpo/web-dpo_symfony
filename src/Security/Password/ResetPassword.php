<?php

namespace App\Security\Password;

use App\Entity\ForgetToken;
use App\Entity\User;
use App\Repository\ForgetTokenRepository;
use App\Repository\UserRepository;
use App\Service\Email\EmailData;
use App\Service\Email\EmailServiceInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class ResetPassword
{
    private EntityManagerInterface $em;
    private UserRepository $userRepository;
    private ForgetTokenRepository $tokenRepository;
    private RouterInterface $router;
    private UserPasswordHasherInterface $passwordHasher;
    private EmailServiceInterface $email;
    private ParameterBagInterface $bag;

    public function __construct(
        EmailServiceInterface $email,
        EntityManagerInterface $em,
        RouterInterface $router,
        UserRepository $userRepository,
        ForgetTokenRepository $tokenRepository,
        UserPasswordHasherInterface $passwordHasher,
        ParameterBagInterface $bag
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->tokenRepository = $tokenRepository;
        $this->router = $router;
        $this->passwordHasher = $passwordHasher;
        $this->email = $email;
        $this->bag = $bag;
    }

    public function definePassword(User $user)
    {
        $token = $this->createToken($user);

        $resetPasswordUrl = $this->generateResetPasswordUrl($token);

        $subject = 'Initialisation de votre mot de passe';

        $content = "Bonjour, \n
                   Un administrateur de l'application web-DPO viens de vous crée un compte sur l'application.\n
                   Veuillez cliquer sur le lien suivant pour initialiser votre mot de passe : \n\n" . $resetPasswordUrl;

        $emailData = $this->prepareEmail($user, $subject, $content);
        $this->email->sendBatch([$emailData]);
    }

    public function reset(User $user, bool $requestAdmin = false)
    {
        $token = $this->createToken($user);

        $resetPasswordUrl = $this->generateResetPasswordUrl($token);

        $subject = 'Réinitialiser votre mot de passe';

        if ($requestAdmin === false) {
            $content = "Bonjour, \n
                   Une demande de réinitialisation de votre mot de passe a été faite sur l'application web-DPO.\n
                   Si vous n'êtes pas à l'origine de cette demande, veuillez contacter votre administrateur.\n
                   Sinon veuillez cliquer sur le lien suivant pour réinitialiser votre mot de passe : \n\n" . $resetPasswordUrl;
        } else {
            $content = "Bonjour, \n
                   Une demande de réinitialisation de votre mot de passe a été faite par un administrateur de l'application web-DPO.\n
                   Veuillez cliquer sur le lien suivant pour réinitialiser votre mot de passe : \n\n" . $resetPasswordUrl;
        }

        $emailData = $this->prepareEmail($user, $subject, $content);
        $this->email->sendBatch([$emailData]);
    }

    public function prepareEmail(
        User $user,
        string $subject,
        string $content
    ): EmailData {
        if ($subject === null) {
            $subject = 'Réinitialisation votre mot de passe';
        }

        //        $resetPasswordUrl = $this->router->generate('app_reset', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
        //
        //        $format = "Bonjour, \n\n Veuillez cliquer sur le lien suivant pour %s votre mot de passe : \n\n " . $resetPasswordUrl;
        //
        //        $content = sprintf($format, $wordContent);

        $emailData = new EmailData($subject, $content, EmailData::FORMAT_TEXT);
        $emailData->setTo($user->getEmail())
            ->setReplyTo($this->bag->get('email_from'));

        //        $emailData->setTo($user->getEmail())
        //            ->setReplyTo($this->bag->get('email_from'));
        //        if ($user->getStructure() && $user->getStructure()->getReplyTo()) {
        //            $emailData->setReplyTo($user->getStructure()->getReplyTo());
        //        }

        return $emailData;
    }

    private function generateResetPasswordUrl($token)
    {
        $resetPasswordUrl = $this->router->generate(
            'app_reset',
            ['token' => $token],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return $resetPasswordUrl;
    }

    /**
     * @return User
     *
     * @throws EntityNotFoundException
     * @throws TimeoutException
     */
    public function getUserFromToken(string $token)
    {
        $token = $this->tokenRepository->findOneBy(['token' => $token]);

        if (empty($token)) {
            throw new EntityNotFoundException('this token does not exist', 400);
        }

        if (new DateTime() > $token->getExpireAt()) {
            throw new TimeoutException('this token has expired', 400);
        }

        return $token->getUser();
    }

    private function createToken(User $user): string
    {
        $this->removeTokenIfExists($user);

        $token = new ForgetToken($user);

        $this->em->persist($token);
        $this->em->flush();

        return $token->getToken();
    }

    private function removeTokenIfExists(User $user)
    {
        $token = $this->tokenRepository->findOneBy(['user' => $user]);

        if (empty($token)) {
            return;
        }

        $this->em->remove($token);
        $this->em->flush();

        $user->removeToken();
    }

    public function setNewPassword(User $user, string $plainPassword)
    {
        $user->setPassword($this->passwordHasher->hashPassword($user, $plainPassword));
        $this->em->persist($user);
        $this->em->flush();

        $this->removeTokenIfExists($user);
    }
}
