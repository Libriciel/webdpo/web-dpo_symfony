<?php

namespace App\Entity;

use App\Repository\DelegueRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DelegueRepository::class)]
#[ORM\Table(name: '`delegue`')]

#[UniqueEntity(fields: 'email')]
#[UniqueEntity(fields: 'cellphone')]
#[UniqueEntity(fields: 'phone')]
#[UniqueEntity(fields: 'numberCnil')]
class Delegue
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::STRING, length: 4, unique: false, nullable: true)]
    #[Assert\Length(min: 2, max: 4)]
    #[Assert\Choice(['M.', 'Mme.'])]
    private $civility;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Assert\Regex(pattern: "/^[a-zA-Z\\-'ÂÀâàÇçÉÊÈËéêèëÎÏîïÔÖôöÛÙûù ]*\$/i")]
    private $firstName;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Assert\Regex(pattern: "/^[a-zA-Z\\-'ÂÀâàÇçÉÊÈËéêèëÎÏîïÔÖôöÛÙûù ]*\$/i")]
    private $lastName;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Assert\Email]
    private $email;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: true)]
    #[Assert\Length(max: 10)]
    private $cellphone;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: true)]
    #[Assert\Length(max: 10)]
    private $phone;

    #[ORM\Column(type: Types::STRING, length: 50, unique: true, nullable: true)]
    #[Assert\Length(max: 50)]
    private $numberCnil;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(?string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = ucfirst($firstName);

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = strtoupper($lastName);

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = str_replace(' ', '', $cellphone);

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = str_replace(' ', '', $phone);

        return $this;
    }

    public function getNumberCnil(): ?string
    {
        return $this->numberCnil;
    }

    public function setNumberCnil(?string $numberCnil): self
    {
        $this->numberCnil = $numberCnil;

        return $this;
    }

    /**
     * @return string
     */
    public function getNomcomplet(): string
    {
        $nomcomplet = $this->getCivility() . ' ' . $this->getFirstName() . ' ' . $this->getLastName();

        return $nomcomplet;
    }
}
