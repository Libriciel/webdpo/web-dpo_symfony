<?php

namespace App\Entity;

use App\Repository\FileRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FileRepository::class)]
#[ORM\Table(name: '`file`')]

#[UniqueEntity(fields: ['fileName', 'path'], message: 'Le nom du fichier doit être unique', errorPath: 'fileName')]
class File
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: Article::class, inversedBy: 'files')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private $article;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $fileName;

    #[ORM\Column(type: Types::STRING, length: 512, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 512)]
    private $path;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }
}
