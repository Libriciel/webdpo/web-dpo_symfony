<?php

namespace App\Entity;

use App\Repository\ChampRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ChampRepository::class)]
#[ORM\Table(name: '`champ`')]

#[UniqueEntity(fields: ['line', 'colonne'], message: 'Il ne peut pas y avoir deux champs superposer')]
class Champ implements TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: Formulaire::class, inversedBy: 'champs')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull]
    private $formulaire;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Assert\Choice([
        "input",
        "textarea",
        "date",
        "checkboxes",
        "radios",
        "deroulant",
        "multi-select",
        "title",
        "help",
        "texte"
    ])]
    private $type;

    #[ORM\Column(type: Types::INTEGER, unique: false, nullable: false)]
    #[Assert\NotBlank]
    private $line;

    #[ORM\Column(type: Types::INTEGER, unique: false, nullable: false)]
    #[Assert\NotBlank]
    private $colonne;

    #[ORM\Column(type: Types::JSON, options: ['jsonb' => true])]
    #[Assert\NotBlank]
    private $details = [
        'name' => null,
        'placeholder' => null,
        'label' => null,
        'obligatoire' => false,
        'default' => false,
        'conditions' => null
    ];

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $fieldForCoresponsable = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $fieldForSoustraitant = false;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFormulaire(): ?Formulaire
    {
        return $this->formulaire;
    }

    public function setFormulaire(?Formulaire $formulaire): self
    {
        $this->formulaire = $formulaire;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLine(): ?int
    {
        return $this->line;
    }

    public function setLine(int $line): self
    {
        $this->line = $line;

        return $this;
    }

    public function getColonne(): ?int
    {
        return $this->colonne;
    }

    public function setColonne(int $colonne): self
    {
        $this->colonne = $colonne;

        return $this;
    }

    public function getDetails(): ?array
    {
        return $this->details;
    }

    public function setDetails(array $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getFieldForCoresponsable(): ?bool
    {
        return $this->fieldForCoresponsable;
    }

    public function setFieldForCoresponsable(bool $fieldForCoresponsable): self
    {
        $this->fieldForCoresponsable = $fieldForCoresponsable;

        return $this;
    }

    public function getFieldForSoustraitant(): ?bool
    {
        return $this->fieldForSoustraitant;
    }

    public function setFieldForSoustraitant(bool $fieldForSoustraitant): self
    {
        $this->fieldForSoustraitant = $fieldForSoustraitant;

        return $this;
    }
}
