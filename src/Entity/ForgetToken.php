<?php

namespace App\Entity;

use App\Repository\ForgetTokenRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ForgetTokenRepository::class)]
#[ORM\Table(name: '`forget_token`')]

#[UniqueEntity(fields: 'token')]
#[UniqueConstraint(name: 'IDX_FORGETTOKEN_TOKEN', columns: ['token'])]
class ForgetToken
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $token;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private $expireAt;

    #[ORM\OneToOne(targetEntity: User::class, cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->expireAt = new DateTime('+30 minutes');
        $this->token = bin2hex(random_bytes(60));
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getExpireAt(): ?DateTimeInterface
    {
        return $this->expireAt;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setToken(string $newToken)
    {
        $this->token = $newToken;
    }
}
