<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]

#[UniqueEntity(fields: 'username', message: "Ce nom d'utilisateur est déjà utilisé")]
#[UniqueEntity(fields: 'email')]
#[UniqueEntity(fields: 'cellphone')]
#[UniqueEntity(fields: 'phone')]
#[UniqueConstraint(name: 'IDX_USER_USERNAME', columns: ['username'])]
#[UniqueConstraint(name: 'IDX_USER_EMAIL', columns: ['email'])]
#[UniqueConstraint(name: 'IDX_USER_CELLPHONE', columns: ['cellphone'])]
#[UniqueConstraint(name: 'IDX_USER_PHONE', columns: ['phone'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $username;

    #[ORM\Column(type: Types::STRING, unique: false, nullable: false)]
    private $password;

    #[ORM\Column(type: Types::STRING, length: 4, unique: false, nullable: true)]
    #[Assert\Length(min: 2, max: 4)]
    #[Assert\Choice(['M.', 'Mme.'])]
    private $civility;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Assert\Regex(pattern: "/^[a-zA-Z\\-'ÂÀâàÇçÉÊÈËéêèëÎÏîïÔÖôöÛÙûù ]*\$/i")]
    private $firstName;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Assert\Regex(pattern: "/^[a-zA-Z\\-'ÂÀâàÇçÉÊÈËéêèëÎÏîïÔÖôöÛÙûù ]*\$/i")]
    private $lastName;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Assert\Email]
    private $email;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: true)]
    #[Assert\Length(max: 10)]
    private $cellphone;

    #[ORM\Column(type: Types::STRING, length: 10, unique: false, nullable: true)]
    #[Assert\Length(max: 10)]
    private $phone;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $notification = false;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Dpo::class)]
    private $dpos;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: OrganisationUserRole::class, cascade: ['persist'])]
    private $organisationUserRoles;

    #[ORM\ManyToOne(targetEntity: Organisation::class, inversedBy: 'users')]
    private $currentOrganisation;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $isSuperadmin = false;

    #[ORM\ManyToMany(targetEntity: Service::class, mappedBy: 'user')]
    private $services;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    private $isActive = true;

    public function __construct()
    {
        $this->dpos = new ArrayCollection();
        $this->organisationUserRoles = new ArrayCollection();
        $this->services = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = ucfirst($firstName);

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = strtoupper($lastName);

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $cellphone = str_replace(' ', '', $cellphone);

        $this->cellphone = $cellphone;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $phone = str_replace(' ', '', $phone);

        $this->phone = $phone;

        return $this;
    }

    public function getNotification(): ?bool
    {
        return $this->notification;
    }

    public function setNotification(bool $notification): self
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * @return Collection|Dpo[]
     */
    public function getDpos(): Collection
    {
        return $this->dpos;
    }

    public function addDpo(Dpo $dpo): self
    {
        if (!$this->dpos->contains($dpo)) {
            $this->dpos[] = $dpo;
            $dpo->setUser($this);
        }

        return $this;
    }

    public function removeDpo(Dpo $dpo): self
    {
        if ($this->dpos->removeElement($dpo)) {
            // set the owning side to null (unless already changed)
            if ($dpo->getUser() === $this) {
                $dpo->setUser(null);
            }
        }

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->getCurrentRoleOrganisation() ? $this->getCurrentRoleOrganisation()->getComposites() : [];
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_DEFAULT';

        return array_unique($roles);
    }

    public function getCurrentRoleOrganisation(): ?Role
    {
        /** @var iterable<OrganisationUserRole> $organisationUserRoles */
        $organisationUserRoles = $this->organisationUserRoles;

        $currentRole = null;
        foreach ($organisationUserRoles as $organisationUserRole) {
            if ($this->getIsSuperadmin() === false) {
                if ($organisationUserRole->getOrganisation()->getId() === $this->getCurrentOrganisation()->getId()) {
                    $currentRole = $organisationUserRole->getRole();
                }
            } else {
                $currentRole = $organisationUserRole->getRole();
            }
        }

        return $currentRole;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return string
     */
    public function getNomcomplet(): string
    {
        $nomcomplet = $this->getCivility() . ' ' . $this->getFirstName() . ' ' . $this->getLastName();

        return $nomcomplet;
    }

    /**
     * @return Collection|OrganisationUserRole[]
     */
    public function getOrganisationUserRoles(): Collection
    {
        return $this->organisationUserRoles;
    }

    public function addOrganisationUserRole(OrganisationUserRole $organisationUserRole): self
    {
        if (!$this->organisationUserRoles->contains($organisationUserRole)) {
            //            $this->organisationUserRoles[] = $organisationUserRole;
            $this->organisationUserRoles->add($organisationUserRole);
            $organisationUserRole->setUser($this);
        }

        return $this;
    }

    public function removeOrganisationUserRole(OrganisationUserRole $organisationUserRole): self
    {
        if ($this->organisationUserRoles->removeElement($organisationUserRole)) {
            // set the owning side to null (unless already changed)
            if ($organisationUserRole->getUser() === $this) {
                $organisationUserRole->setUser(null);
            }
        }

        return $this;
    }

    public function getCurrentOrganisation(): ?Organisation
    {
        return $this->currentOrganisation;
    }

    public function setCurrentOrganisation(?Organisation $currentOrganisation): self
    {
        $this->currentOrganisation = $currentOrganisation;

        return $this;
    }

    public function getIsSuperadmin(): ?bool
    {
        return $this->isSuperadmin;
    }

    public function setIsSuperadmin(?bool $isSuperadmin): self
    {
        $this->isSuperadmin = $isSuperadmin;

        return $this;
    }

    /**
     * @return Collection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->addUser($this);
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->services->removeElement($service)) {
            $service->removeUser($this);
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
