<?php

namespace App\Entity;

use App\Repository\NormeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: NormeRepository::class)]
#[ORM\Table(name: '`norme`')]

#[UniqueEntity(fields: 'name')]
#[UniqueEntity(fields: ['norme', 'numero'], message: 'Il ne peut pas y avoir deux norme identique', errorPath: 'numero')]
class Norme implements TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::STRING, length: 2, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 2)]
    #[Assert\Choice([
        "AU",
        "DI",
        "MR",
        "NS",
        "RS",
        "RU"
    ])]
    private $norme;

    #[ORM\Column(type: Types::STRING, length: 3, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 3)]
    private $numero;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $name;

    #[ORM\Column(type: Types::TEXT, unique: false, nullable: true)]
    private $description;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $abroger = false;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: true)]
    #[Assert\Length(max: 255)]
    private $fileName;

    #[ORM\Column(type: Types::STRING, length: 512, unique: true, nullable: true)]
    #[Assert\Length(max: 512)]
    private $path;

    #[ORM\OneToMany(mappedBy: 'norme', targetEntity: Traitement::class)]
    private $traitements;

    public function __construct()
    {
        $this->traitements = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNorme(): ?string
    {
        return $this->norme;
    }

    public function setNorme(string $norme): self
    {
        $this->norme = $norme;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAbroger(): ?bool
    {
        return $this->abroger;
    }

    public function setAbroger(bool $abroger): self
    {
        $this->abroger = $abroger;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Collection|Traitement[]
     */
    public function getTraitements(): Collection
    {
        return $this->traitements;
    }

    public function addTraitement(Traitement $traitement): self
    {
        if (!$this->traitements->contains($traitement)) {
            $this->traitements[] = $traitement;
            $traitement->setNorme($this);
        }

        return $this;
    }

    public function removeTraitement(Traitement $traitement): self
    {
        if ($this->traitements->removeElement($traitement)) {
            // set the owning side to null (unless already changed)
            if ($traitement->getNorme() === $this) {
                $traitement->setNorme(null);
            }
        }

        return $this;
    }
}
