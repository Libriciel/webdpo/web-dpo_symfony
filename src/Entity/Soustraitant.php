<?php

namespace App\Entity;

use App\Repository\SoustraitantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SoustraitantRepository::class)]
#[ORM\Table(name: '`soustraitant`')]

#[UniqueEntity(fields: 'raisonsociale', message: 'Ce sous-traitant existe déjà')]
#[UniqueEntity(fields: 'phone')]
#[UniqueEntity(fields: 'fax')]
#[UniqueEntity(fields: 'email')]
#[UniqueEntity(fields: 'siret')]
#[UniqueEntity(fields: 'logo')]
#[UniqueConstraint(name: 'IDX_SOUSTRAITANT_RAISONSOCIALE', columns: ['raisonsociale'])]
#[UniqueConstraint(name: 'IDX_SOUSTRAITANT_PHONE', columns: ['phone'])]
#[UniqueConstraint(name: 'IDX_SOUSTRAITANT_FAX', columns: ['fax'])]
#[UniqueConstraint(name: 'IDX_SOUSTRAITANT_EMAIL', columns: ['email'])]
#[UniqueConstraint(name: 'IDX_SOUSTRAITANT_SIRET', columns: ['siret'])]
#[UniqueConstraint(name: 'IDX_SOUSTRAITANT_LOGO', columns: ['logo'])]
class Soustraitant
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $raisonsociale;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: true)]
    #[Assert\Length(max: 10)]
    private $phone;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: true)]
    #[Assert\Length(max: 10)]
    private $fax;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: true)]
    #[Assert\Length(max: 255)]
    private $address;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: true)]
    #[Assert\Length(max: 255)]
    #[Assert\Email]
    private $email;

    #[ORM\Column(type: Types::STRING, length: 9, unique: true, nullable: true)]
    #[Assert\Length(min: 9, max: 9)]
    private $siren;

    #[ORM\Column(type: Types::STRING, length: 14, unique: true, nullable: true)]
    #[Assert\Length(min: 14, max: 14)]
    private $siret;

    #[ORM\Column(type: Types::STRING, length: 5, unique: false, nullable: true)]
    #[Assert\Length(min: 5, max: 5)]
    private $ape;

    #[ORM\Column(type: Types::TEXT, unique: true, nullable: true)]
    private $logo;

    #[ORM\OneToOne(targetEntity: Representant::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private $representant;

    #[ORM\ManyToMany(targetEntity: Organisation::class, inversedBy: 'soustraitants')]
    private $organisations;

    #[ORM\OneToOne(targetEntity: Delegue::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private $delegue;

    public function __construct()
    {
        $this->organisations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getRaisonsociale(): ?string
    {
        return $this->raisonsociale;
    }

    public function setRaisonsociale(string $raisonsociale): self
    {
        $this->raisonsociale = $raisonsociale;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = str_replace(' ', '', $phone);

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = str_replace(' ', '', $fax);

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(?string $siren): self
    {
        $this->siren = str_replace(' ', '', $siren);

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = str_replace(' ', '', $siret);

        return $this;
    }

    public function getApe(): ?string
    {
        return $this->ape;
    }

    public function setApe(?string $ape): self
    {
        $this->ape = $ape;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

//    public function getCivilityDpo(): ?string
//    {
//        return $this->civilityDpo;
//    }

//    public function setCivilityDpo(?string $civilityDpo): self
//    {
//        $this->civilityDpo = $civilityDpo;
//
//        return $this;
//    }

//    public function getFirstNameDpo(): ?string
//    {
//        return $this->firstNameDpo;
//    }

//    public function setFirstNameDpo(?string $firstNameDpo): self
//    {
//        $this->firstNameDpo = ucfirst($firstNameDpo);
//
//        return $this;
//    }

//    public function getLastNameDpo(): ?string
//    {
//        return $this->lastNameDpo;
//    }

//    public function setLastNameDpo(?string $lastNameDpo): self
//    {
//        $this->lastNameDpo = strtoupper($lastNameDpo);
//
//        return $this;
//    }

//    /**
//     * @return string
//     */
//    public function getNomcompletDpo(): string
//    {
//        $nomcomplet = $this->getCivilityDpo() . ' ' . $this->getFirstNameDpo() . ' ' . $this->getLastNameDpo();
//
//        return $nomcomplet;
//    }
//
//    public function getEmailDpo(): ?string
//    {
//        return $this->emailDpo;
//    }
//
//    public function setEmailDpo(?string $emailDpo): self
//    {
//        $this->emailDpo = $emailDpo;
//
//        return $this;
//    }
//
//    public function getNumberCnilDpo(): ?string
//    {
//        return $this->numberCnilDpo;
//    }
//
//    public function setNumberCnilDpo(?string $numberCnilDpo): self
//    {
//        $this->numberCnilDpo = $numberCnilDpo;
//
//        return $this;
//    }
//
//    public function getCellphoneDpo(): ?string
//    {
//        return $this->cellphoneDpo;
//    }
//
//    public function setCellphoneDpo(?string $cellphoneDpo): self
//    {
//        $this->cellphoneDpo = str_replace(' ', '', $cellphoneDpo);
//
//        return $this;
//    }
//
//    public function getPhoneDpo(): ?string
//    {
//        return $this->phoneDpo;
//    }
//
//    public function setPhoneDpo(?string $phoneDpo): self
//    {
//        $this->phoneDpo = str_replace(' ', '', $phoneDpo);
//
//        return $this;
//    }

    public function getRepresentant(): ?Representant
    {
        return $this->representant;
    }

    public function setRepresentant(?Representant $representant): self
    {
        $this->representant = $representant;

        return $this;
    }

    /**
     * @return Collection|Organisation[]
     */
    public function getOrganisations(): Collection
    {
        return $this->organisations;
    }

    public function addOrganisation(Organisation $organisation): self
    {
        if (!$this->organisations->contains($organisation)) {
            $this->organisations[] = $organisation;
        }

        return $this;
    }

    public function removeOrganisation(Organisation $organisation): self
    {
        $this->organisations->removeElement($organisation);

        return $this;
    }

    public function getDelegue(): ?Delegue
    {
        return $this->delegue;
    }

    public function setDelegue(?Delegue $delegue): self
    {
        $this->delegue = $delegue;

        return $this;
    }
}
