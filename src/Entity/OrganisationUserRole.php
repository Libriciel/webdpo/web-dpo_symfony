<?php

namespace App\Entity;

use App\Repository\OrganisationUserRoleRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OrganisationUserRoleRepository::class)]
#[ORM\Table(name: '`organisation_user_role`')]
class OrganisationUserRole
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: Organisation::class, inversedBy: 'organisationUserRoles')]
    private $organisation;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'organisationUserRoles')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    #[Assert\NotNull]
    private $user;

    #[ORM\ManyToOne(targetEntity: Role::class, inversedBy: 'organisationUserRoles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull]
    private $role;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisation(): ?Organisation
    {
        return $this->organisation;
    }

    public function setOrganisation(?Organisation $organisation): self
    {
        $this->organisation = $organisation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }
}
