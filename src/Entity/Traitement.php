<?php

namespace App\Entity;

use App\Repository\TraitementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TraitementRepository::class)]
#[ORM\Table(name: '`traitement`')]

#[UniqueEntity(fields: 'numero')]
#[UniqueConstraint(name: 'IDX_TRAITEMENT_NUMERO', columns: ['numero'])]
class Traitement implements TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: Formulaire::class, inversedBy: 'traitements')]
    private $formulaire;

    #[ORM\ManyToOne(targetEntity: Organisation::class, inversedBy: 'traitements')]
    private $organisation;

    #[ORM\ManyToOne(targetEntity: Norme::class, inversedBy: 'traitements')]
    private $norme;

    #[ORM\ManyToOne(targetEntity: Service::class, inversedBy: 'traitements')]
    private $service;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: true)]
    #[Assert\Length(max: 255)]
    private $numero;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $coresponsable = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $soustraitance = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true)]
    private $obligationPia;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true)]
    private $realisationPia;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true)]
    private $depotPia;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $transfertHorsUe = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $donneesSensibles = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $rtExterne = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    private $valide = true;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true)]
    private $partage;

    #[ORM\OneToMany(mappedBy: 'traitement', targetEntity: Valeur::class)]
    private $valeurs;

    public function __construct()
    {
        $this->valeurs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFormulaire(): ?Formulaire
    {
        return $this->formulaire;
    }

    public function setFormulaire(?Formulaire $formulaire): self
    {
        $this->formulaire = $formulaire;

        return $this;
    }

    public function getOrganisation(): ?Organisation
    {
        return $this->organisation;
    }

    public function setOrganisation(?Organisation $organisation): self
    {
        $this->organisation = $organisation;

        return $this;
    }

    public function getNorme(): ?Norme
    {
        return $this->norme;
    }

    public function setNorme(?Norme $norme): self
    {
        $this->norme = $norme;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getCoresponsable(): ?bool
    {
        return $this->coresponsable;
    }

    public function setCoresponsable(bool $coresponsable): self
    {
        $this->coresponsable = $coresponsable;

        return $this;
    }

    public function getSoustraitance(): ?bool
    {
        return $this->soustraitance;
    }

    public function setSoustraitance(bool $soustraitance): self
    {
        $this->soustraitance = $soustraitance;

        return $this;
    }

    public function getObligationPia(): ?bool
    {
        return $this->obligationPia;
    }

    public function setObligationPia(?bool $obligationPia): self
    {
        $this->obligationPia = $obligationPia;

        return $this;
    }

    public function getRealisationPia(): ?bool
    {
        return $this->realisationPia;
    }

    public function setRealisationPia(?bool $realisationPia): self
    {
        $this->realisationPia = $realisationPia;

        return $this;
    }

    public function getDepotPia(): ?bool
    {
        return $this->depotPia;
    }

    public function setDepotPia(?bool $depotPia): self
    {
        $this->depotPia = $depotPia;

        return $this;
    }

    public function getTransfertHorsUe(): ?bool
    {
        return $this->transfertHorsUe;
    }

    public function setTransfertHorsUe(bool $transfertHorsUe): self
    {
        $this->transfertHorsUe = $transfertHorsUe;

        return $this;
    }

    public function getDonneesSensibles(): ?bool
    {
        return $this->donneesSensibles;
    }

    public function setDonneesSensibles(bool $donneesSensibles): self
    {
        $this->donneesSensibles = $donneesSensibles;

        return $this;
    }

    public function getRtExterne(): ?bool
    {
        return $this->rtExterne;
    }

    public function setRtExterne(bool $rtExterne): self
    {
        $this->rtExterne = $rtExterne;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

    public function getPartage(): ?bool
    {
        return $this->partage;
    }

    public function setPartage(?bool $partage): self
    {
        $this->partage = $partage;

        return $this;
    }

    /**
     * @return Collection|Valeur[]
     */
    public function getValeurs(): Collection
    {
        return $this->valeurs;
    }

    public function addValeur(Valeur $valeur): self
    {
        if (!$this->valeurs->contains($valeur)) {
            $this->valeurs[] = $valeur;
            $valeur->setTraitement($this);
        }

        return $this;
    }

    public function removeValeur(Valeur $valeur): self
    {
        if ($this->valeurs->removeElement($valeur)) {
            // set the owning side to null (unless already changed)
            if ($valeur->getTraitement() === $this) {
                $valeur->setTraitement(null);
            }
        }

        return $this;
    }
}
