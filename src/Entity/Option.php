<?php

namespace App\Entity;

use App\Repository\OptionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

#[ORM\Entity(repositoryClass: OptionRepository::class)]
#[ORM\Table(name: '`option`')]
class Option
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $useSousFinalite = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $useBaseLegale = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $useDecisionAutomatisee = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $useTransfertHorsUe = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $useDonneesSensible = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $useAllExtensionFiles = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $usePia = false;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUseSousFinalite(): ?bool
    {
        return $this->useSousFinalite;
    }

    public function setUseSousFinalite(bool $useSousFinalite): self
    {
        $this->useSousFinalite = $useSousFinalite;

        return $this;
    }

    public function getUseBaseLegale(): ?bool
    {
        return $this->useBaseLegale;
    }

    public function setUseBaseLegale(bool $useBaseLegale): self
    {
        $this->useBaseLegale = $useBaseLegale;

        return $this;
    }

    public function getUseDecisionAutomatisee(): ?bool
    {
        return $this->useDecisionAutomatisee;
    }

    public function setUseDecisionAutomatisee(bool $useDecisionAutomatisee): self
    {
        $this->useDecisionAutomatisee = $useDecisionAutomatisee;

        return $this;
    }

    public function getUseTransfertHorsUe(): ?bool
    {
        return $this->useTransfertHorsUe;
    }

    public function setUseTransfertHorsUe(bool $useTransfertHorsUe): self
    {
        $this->useTransfertHorsUe = $useTransfertHorsUe;

        return $this;
    }

    public function getUseDonneesSensible(): ?bool
    {
        return $this->useDonneesSensible;
    }

    public function setUseDonneesSensible(bool $useDonneesSensible): self
    {
        $this->useDonneesSensible = $useDonneesSensible;

        return $this;
    }

    public function getUseAllExtensionFiles(): ?bool
    {
        return $this->useAllExtensionFiles;
    }

    public function setUseAllExtensionFiles(bool $useAllExtensionFiles): self
    {
        $this->useAllExtensionFiles = $useAllExtensionFiles;

        return $this;
    }

    public function getUsePia(): ?bool
    {
        return $this->usePia;
    }

    public function setUsePia(bool $usePia): self
    {
        $this->usePia = $usePia;

        return $this;
    }
}
