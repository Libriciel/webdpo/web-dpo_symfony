<?php

namespace App\Entity;

use App\Repository\FormulaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FormulaireRepository::class)]
#[ORM\Table(name: '`formulaire`')]

#[UniqueEntity(fields: 'name')]
#[UniqueConstraint(name: 'IDX_FORMULAIRE_NAME', columns: ['name'])]
class Formulaire implements TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $isActive = false;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $name;

    #[ORM\Column(type: Types::TEXT, unique: false, nullable: true)]
    private $description;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $isViolation = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $isActivite = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $isRtExterne = false;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $isArchived = false;

    #[ORM\ManyToOne(targetEntity: Organisation::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull]
    private $createdByOrganisation;

    #[ORM\OneToMany(mappedBy: 'formulaire', targetEntity: Champ::class)]
    private $champs;

    #[ORM\OneToMany(mappedBy: 'formulaire', targetEntity: Traitement::class)]
    private $traitements;

    #[ORM\ManyToMany(targetEntity: Organisation::class, inversedBy: 'formulaires')]
    #[ORM\JoinColumn(nullable: true)]
    private $organisations;

    #[ORM\OneToOne(targetEntity: Option::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    private $options;

    public function __construct()
    {
        $this->champs = new ArrayCollection();
        $this->traitements = new ArrayCollection();
        $this->organisations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsViolation(): ?bool
    {
        return $this->isViolation;
    }

    public function setIsViolation(bool $isViolation): self
    {
        $this->isViolation = $isViolation;

        return $this;
    }

    public function getIsActivite(): ?bool
    {
        return $this->isActivite;
    }

    public function setIsActivite(bool $isActivite): self
    {
        $this->isActivite = $isActivite;

        return $this;
    }

    public function getIsRtExterne(): ?bool
    {
        return $this->isRtExterne;
    }

    public function setIsRtExterne(bool $isRtExterne): self
    {
        $this->isRtExterne = $isRtExterne;

        return $this;
    }

    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    public function getCreatedByOrganisation(): ?Organisation
    {
        return $this->createdByOrganisation;
    }

    public function setCreatedByOrganisation(?Organisation $createdByOrganisation): self
    {
        $this->createdByOrganisation = $createdByOrganisation;

        return $this;
    }

    /**
     * @return Collection|Champ[]
     */
    public function getChamps(): Collection
    {
        return $this->champs;
    }

    public function addChamp(Champ $champ): self
    {
        if (!$this->champs->contains($champ)) {
            $this->champs[] = $champ;
            $champ->setFormulaire($this);
        }

        return $this;
    }

    public function removeChamp(Champ $champ): self
    {
        if ($this->champs->removeElement($champ)) {
            // set the owning side to null (unless already changed)
            if ($champ->getFormulaire() === $this) {
                $champ->setFormulaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Traitement[]
     */
    public function getTraitements(): Collection
    {
        return $this->traitements;
    }

    public function addTraitement(Traitement $traitement): self
    {
        if (!$this->traitements->contains($traitement)) {
            $this->traitements[] = $traitement;
            $traitement->setFormulaire($this);
        }

        return $this;
    }

    public function removeTraitement(Traitement $traitement): self
    {
        if ($this->traitements->removeElement($traitement)) {
            // set the owning side to null (unless already changed)
            if ($traitement->getFormulaire() === $this) {
                $traitement->setFormulaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Organisation[]
     */
    public function getOrganisations(): Collection
    {
        return $this->organisations;
    }

    public function addOrganisation(Organisation $organisation): self
    {
        if (!$this->organisations->contains($organisation)) {
            $this->organisations[] = $organisation;
        }

        return $this;
    }

    public function removeOrganisation(Organisation $organisation): self
    {
        $this->organisations->removeElement($organisation);

        return $this;
    }

    public function getOptions(): ?Option
    {
        return $this->options;
    }

    public function setOptions(?Option $options): self
    {
        $this->options = $options;

        return $this;
    }
}
