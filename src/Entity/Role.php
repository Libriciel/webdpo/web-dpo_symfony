<?php

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RoleRepository::class)]
#[ORM\Table(name: '`role`')]

#[UniqueEntity(fields: ['name'])]
class Role
{
    public const CODE_ROLE_SUPERADMIN = 1;
    public const CODE_ROLE_DPO = 2;
    public const CODE_ROLE_ADMIN = 3;
    public const CODE_ROLE_VALIDEUR = 4;
    public const CODE_ROLE_REDACTEUR = 5;
    public const CODE_ROLE_CONSULTANT = 6;

    public const NAME_ROLE_SUPERADMIN = 'Superadmin';
    public const NAME_ROLE_DPO = 'Dpo';
    public const NAME_ROLE_ADMIN = 'Administrateur';
    public const NAME_ROLE_VALIDEUR = 'Valideur';
    public const NAME_ROLE_REDACTEUR = 'Rédacteur';
    public const NAME_ROLE_CONSULTANT = 'Valideur';

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $name;

    #[ORM\Column(type: Types::JSON, options: ['jsonb' => true])]
    private $composites = [];

    #[ORM\OneToMany(mappedBy: 'role', targetEntity: OrganisationUserRole::class)]
    private $organisationUserRoles;

    public function __construct()
    {
        $this->organisationUserRoles = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getComposites(): ?array
    {
        return $this->composites;
    }

    public function setComposites(array $composites): self
    {
        $this->composites = $composites;

        return $this;
    }

    public function addComposite(string $composite): self
    {
        if (!in_array($composite, $this->composites)) {
            $this->composites[] = $composite;
        }

        return $this;
    }

    /**
     * @return Collection|OrganisationUserRole[]
     */
    public function getOrganisationUserRoles(): Collection
    {
        return $this->organisationUserRoles;
    }

    public function addOrganisationUserRole(OrganisationUserRole $organisationUserRole): self
    {
        if (!$this->organisationUserRoles->contains($organisationUserRole)) {
            $this->organisationUserRoles[] = $organisationUserRole;
            $organisationUserRole->setRole($this);
        }

        return $this;
    }

    public function removeOrganisationUserRole(OrganisationUserRole $organisationUserRole): self
    {
        if ($this->organisationUserRoles->removeElement($organisationUserRole)) {
            // set the owning side to null (unless already changed)
            if ($organisationUserRole->getRole() === $this) {
                $organisationUserRole->setRole(null);
            }
        }

        return $this;
    }
}
