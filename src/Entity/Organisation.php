<?php

namespace App\Entity;

use App\Repository\OrganisationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OrganisationRepository::class)]
#[ORM\Table(name: '`organisation`')]

#[UniqueEntity(fields: 'raisonsociale')]
#[UniqueEntity(fields: 'phone')]
#[UniqueEntity(fields: 'fax')]
#[UniqueEntity(fields: 'email')]
#[UniqueEntity(fields: 'sigle')]
#[UniqueEntity(fields: 'siret')]
#[UniqueEntity(fields: 'logo')]
class Organisation implements TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private string $id;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $raisonsociale;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 10)]
    private $phone;

    #[ORM\Column(type: Types::STRING, length: 10, unique: true, nullable: true)]
    #[Assert\Length(max: 10)]
    private $fax;

    #[ORM\Column(type: Types::STRING, length: 255, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private $address;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Assert\Email]
    private $email;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true, nullable: true)]
    #[Assert\Length(max: 255)]
    private $sigle;

    #[ORM\Column(type: Types::STRING, length: 14, unique: true, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 14, max: 14)]
    private $siret;

    #[ORM\Column(type: Types::STRING, length: 5, unique: false, nullable: false)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 5, max: 5)]
    private $ape;

    #[ORM\Column(type: Types::TEXT, unique: true, nullable: true)]
    private $logo;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    private $rgpd = false;

    #[ORM\Column(type: Types::STRING, length: 50, unique: false, nullable: false, options: ['default' => 'DPO-'])]
    #[Assert\NotBlank]
    #[Assert\Length(max: 50)]
    private $registrePrefix = 'DPO-';

    #[ORM\Column(type: Types::INTEGER, nullable: false, options: ['default' => 0])]
    private $registreNumber = 0;

    #[ORM\OneToOne(mappedBy: 'organisation', targetEntity: Dpo::class, cascade: ['persist', 'remove'])]
    private $dpo;

    #[ORM\OneToOne(mappedBy: 'organisation', targetEntity: Dirigeant::class, cascade: ['persist', 'remove'])]
    private $dirigeant;

    #[ORM\OneToMany(mappedBy: 'organisation', targetEntity: Service::class)]
    private $services;

    #[ORM\ManyToMany(targetEntity: Article::class, mappedBy: 'organisations')]
    private $articles;

    #[ORM\ManyToMany(targetEntity: Typage::class, mappedBy: 'organisations')]
    private $typages;

    #[ORM\OneToOne(mappedBy: 'organisation', targetEntity: ModelePresentation::class, cascade: ['persist', 'remove'])]
    private $modelePresentation;

    #[ORM\OneToMany(mappedBy: 'organisation', targetEntity: Traitement::class)]
    private $traitements;

    #[ORM\OneToMany(mappedBy: 'organisation', targetEntity: OrganisationUserRole::class)]
    private $organisationUserRoles;

    #[ORM\OneToMany(mappedBy: 'currentOrganisation', targetEntity: User::class)]
    private $users;

    #[ORM\ManyToMany(targetEntity: Soustraitant::class, mappedBy: 'organisations')]
    private $soustraitants;

    #[ORM\ManyToMany(targetEntity: Coresponsable::class, mappedBy: 'organisations')]
    private $coresponsables;

    #[ORM\ManyToMany(targetEntity: Formulaire::class, mappedBy: 'organisations')]
    private $formulaires;

    public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->typages = new ArrayCollection();
        $this->traitements = new ArrayCollection();
        $this->organisationUserRoles = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->soustraitants = new ArrayCollection();
        $this->coresponsables = new ArrayCollection();
        $this->formulaires = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getRaisonsociale(): ?string
    {
        return $this->raisonsociale;
    }

    public function setRaisonsociale(string $raisonsociale): self
    {
        $this->raisonsociale = $raisonsociale;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = str_replace(' ', '', $phone);

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = str_replace(' ', '', $fax);

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSigle(): ?string
    {
        return $this->sigle;
    }

    public function setSigle(?string $sigle): self
    {
        $this->sigle = $sigle;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = str_replace(' ', '', $siret);

        return $this;
    }

    public function getApe(): ?string
    {
        return $this->ape;
    }

    public function setApe(string $ape): self
    {
        $this->ape = $ape;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getRgpd(): ?bool
    {
        return $this->rgpd;
    }

    public function setRgpd(bool $rgpd): self
    {
        $this->rgpd = $rgpd;

        return $this;
    }

    public function getRegistrePrefix(): ?string
    {
        return $this->registrePrefix;
    }

    public function setRegistrePrefix(string $registrePrefix): self
    {
        $this->registrePrefix = $registrePrefix;

        return $this;
    }

    public function getRegistreNumber(): ?int
    {
        return $this->registreNumber;
    }

    public function setRegistreNumber(int $registreNumber): self
    {
        $this->registreNumber = $registreNumber;

        return $this;
    }

    public function getDpo(): ?Dpo
    {
        return $this->dpo;
    }

    public function setDpo(Dpo $dpo): self
    {
        // set the owning side of the relation if necessary
        if ($dpo->getOrganisation() !== $this) {
            $dpo->setOrganisation($this);
        }

        $this->dpo = $dpo;

        return $this;
    }

    public function getDirigeant(): ?Dirigeant
    {
        return $this->dirigeant;
    }

    public function setDirigeant(Dirigeant $dirigeant): self
    {
        // set the owning side of the relation if necessary
        if ($dirigeant->getOrganisation() !== $this) {
            $dirigeant->setOrganisation($this);
        }

        $this->dirigeant = $dirigeant;

        return $this;
    }

    /**
     * @return Collection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->setOrganisation($this);
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->services->removeElement($service)) {
            // set the owning side to null (unless already changed)
            if ($service->getOrganisation() === $this) {
                $service->setOrganisation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->addOrganisation($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            $article->removeOrganisation($this);
        }

        return $this;
    }

    /**
     * @return Collection|Typage[]
     */
    public function getTypages(): Collection
    {
        return $this->typages;
    }

    public function addTypage(Typage $typage): self
    {
        if (!$this->typages->contains($typage)) {
            $this->typages[] = $typage;
            $typage->addOrganisation($this);
        }

        return $this;
    }

    public function removeTypage(Typage $typage): self
    {
        if ($this->typages->removeElement($typage)) {
            $typage->removeOrganisation($this);
        }

        return $this;
    }

    public function getModelePresentation(): ?ModelePresentation
    {
        return $this->modelePresentation;
    }

    public function setModelePresentation(ModelePresentation $modelePresentation): self
    {
        // set the owning side of the relation if necessary
        if ($modelePresentation->getOrganisation() !== $this) {
            $modelePresentation->setOrganisation($this);
        }

        $this->modelePresentation = $modelePresentation;

        return $this;
    }

    /**
     * @return Collection|Traitement[]
     */
    public function getTraitements(): Collection
    {
        return $this->traitements;
    }

    public function addTraitement(Traitement $traitement): self
    {
        if (!$this->traitements->contains($traitement)) {
            $this->traitements[] = $traitement;
            $traitement->setOrganisation($this);
        }

        return $this;
    }

    public function removeTraitement(Traitement $traitement): self
    {
        if ($this->traitements->removeElement($traitement)) {
            // set the owning side to null (unless already changed)
            if ($traitement->getOrganisation() === $this) {
                $traitement->setOrganisation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrganisationUserRole[]
     */
    public function getOrganisationUserRoles(): Collection
    {
        return $this->organisationUserRoles;
    }

    public function addOrganisationUserRole(OrganisationUserRole $organisationUserRole): self
    {
        if (!$this->organisationUserRoles->contains($organisationUserRole)) {
            $this->organisationUserRoles[] = $organisationUserRole;
            $organisationUserRole->setOrganisation($this);
        }

        return $this;
    }

    public function removeOrganisationUserRole(OrganisationUserRole $organisationUserRole): self
    {
        if ($this->organisationUserRoles->removeElement($organisationUserRole)) {
            // set the owning side to null (unless already changed)
            if ($organisationUserRole->getOrganisation() === $this) {
                $organisationUserRole->setOrganisation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCurrentOrganisation($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCurrentOrganisation() === $this) {
                $user->setCurrentOrganisation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Soustraitant[]
     */
    public function getSoustraitants(): Collection
    {
        return $this->soustraitants;
    }

    public function addSoustraitant(Soustraitant $soustraitant): self
    {
        if (!$this->soustraitants->contains($soustraitant)) {
            $this->soustraitants[] = $soustraitant;
            $soustraitant->addOrganisation($this);
        }

        return $this;
    }

    public function removeSoustraitant(Soustraitant $soustraitant): self
    {
        if ($this->soustraitants->removeElement($soustraitant)) {
            $soustraitant->removeOrganisation($this);
        }

        return $this;
    }

    /**
     * @return Collection|Coresponsable[]
     */
    public function getCoresponsables(): Collection
    {
        return $this->coresponsables;
    }

    public function addCoresponsable(Coresponsable $coresponsable): self
    {
        if (!$this->coresponsables->contains($coresponsable)) {
            $this->coresponsables[] = $coresponsable;
            $coresponsable->addOrganisation($this);
        }

        return $this;
    }

    public function removeCoresponsable(Coresponsable $coresponsable): self
    {
        if ($this->coresponsables->removeElement($coresponsable)) {
            $coresponsable->removeOrganisation($this);
        }

        return $this;
    }

    /**
     * @return Collection|Formulaire[]
     */
    public function getFormulaires(): Collection
    {
        return $this->formulaires;
    }

    public function addFormulaire(Formulaire $formulaire): self
    {
        if (!$this->formulaires->contains($formulaire)) {
            $this->formulaires[] = $formulaire;
            $formulaire->addOrganisation($this);
        }

        return $this;
    }

    public function removeFormulaire(Formulaire $formulaire): self
    {
        if ($this->formulaires->removeElement($formulaire)) {
            $formulaire->removeOrganisation($this);
        }

        return $this;
    }
}
