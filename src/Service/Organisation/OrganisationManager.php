<?php

namespace App\Service\Organisation;

use App\Entity\Organisation;
use App\Service\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class OrganisationManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly FileManager $fileManager,
        private readonly ParameterBagInterface $bag
    ) {
    }

    public function add(Organisation $organisation, UploadedFile $uploadedFile): void
    {
        $folderLogos = $this->bag->get('folder_logos');
        $newFilename = $this->fileManager->saveFileOnDisk($folderLogos, $uploadedFile);

        $organisation->setLogo($newFilename);

        $this->save($organisation);
    }


    public function save(Organisation $organisation): void
    {
        $this->em->persist($organisation);
        $this->em->flush();
    }

    public function delete(Organisation $organisation): void
    {
        $this->em->remove($organisation);
        $this->em->flush();
    }
}
