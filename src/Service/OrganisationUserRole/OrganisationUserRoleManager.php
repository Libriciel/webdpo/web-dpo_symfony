<?php

namespace App\Service\OrganisationUserRole;

use App\Entity\OrganisationUserRole;
use Doctrine\ORM\EntityManagerInterface;

class OrganisationUserRoleManager
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function save(OrganisationUserRole $organisationUserRole): void
    {
        $this->em->persist($organisationUserRole);
        $this->em->flush();
    }
}
