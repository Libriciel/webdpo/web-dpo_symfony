<?php

namespace App\Service\File;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    public function __construct(
        private readonly Filesystem $filesystem,
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $bag
    ) {
    }

    public function getRootPath()
    {
        return $this->bag->get('path_files');
    }

    /**
     * @param string $folder
     * @param UploadedFile $uploadedFile
     * @return string
     */
    public function saveFileOnDisk(string $folder, UploadedFile $uploadedFile): string
    {
        $path = $this->getRootPath() . $folder;

        $this->createFolder($path);

        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $originalFilename
        );

        $newFilename = $safeFilename . '_' . uniqid() . $this->getExtension($uploadedFile);

        $uploadedFile->move(
            $path,
            $newFilename
        );

        return $folder . $newFilename;
    }

    /**
     * @param string $path
     */
    public function deleteFileOnDisk(string $path)
    {
        if (!$path) {
            return;
        }
        $this->filesystem->remove($path->getPath());
        $this->em->remove($path);
    }

    /**
     * @param $folder
     */
    private function createFolder($folder)
    {
        if ($this->filesystem->exists($folder) === false) {
            $this->filesystem->mkdir($folder);
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function getExtension(UploadedFile $file): string
    {
        return $file->getClientOriginalExtension() ? '.' . $file->getClientOriginalExtension() : '';
    }
}
