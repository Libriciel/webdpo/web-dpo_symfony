<?php

namespace App\Service\Csv;

use App\Entity\Organisation;
use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CsvManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @return ConstraintViolationListInterface[]
     */
    public function importServices(UploadedFile $file, Organisation $organisation): array
    {
        $errors = [];

        /** @var Reader $csv */
        $csv = Reader::createFromPath($file->getRealPath(), 'r');
        $records = $csv->getRecords();

        foreach ($records as $record) {
            if ($record[0] !== "Nom du service" && !empty($record[0])) {
                $service = $this->createService($organisation, $record[0]);

                if (0 !== $this->validator->validate($service)->count()) {
                    $errors[] = $this->validator->validate($service);
                    continue;
                }

                $this->em->persist($service);
            }
        }

        $this->em->flush();

        return $errors;
    }

    private function createService(Organisation $organisation, $name): Service
    {
        $service = new Service();
        $service->setName($name)
            ->setOrganisation($organisation);

        return $service;
    }
}
