<?php

namespace App\Service\Soustraitant;

use App\Entity\Organisation;
use App\Entity\Soustraitant;
use App\Entity\Typage;
use App\Repository\SoustraitantRepository;
use App\Repository\TypageRepository;
use App\Service\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SoustraitantManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly FileManager $fileManager,
        private readonly ParameterBagInterface $bag,
        private readonly SoustraitantRepository $soustraitantRepository
    ) {
    }

    public function add(Soustraitant $soustraitant, ?UploadedFile $uploadedFile): void
    {
        if (!empty($uploadedFile)) {
            $folderLogos = $this->bag->get('folder_logos');
            $newFilename = $this->fileManager->saveFileOnDisk($folderLogos, $uploadedFile);

            $soustraitant->setLogo($newFilename);
        }

        $this->save($soustraitant);
    }

    public function save(Soustraitant $soustraitant): void
    {
        $this->em->persist($soustraitant);
        $this->em->flush();
    }

    public function delete(Soustraitant $soustraitant): void
    {
        $this->em->remove($soustraitant);
        $this->em->flush();
    }

    public function deleteMultiple($data)
    {
        foreach ($data as $soustraitant) {
            $soustraitantSelected = $this->soustraitantRepository->findOneBy([
                'id' => $soustraitant
            ]);

            if (!$soustraitantSelected) {
                throw new BadRequestException('Le sous-traitant n\'existe pas');
            }

            if (count($soustraitantSelected->getOrganisations()) !== 0) {
                throw new BadRequestException('Le sous-traitant ne peux pas être supprimer, car il est associer à une entité');
            }

            $this->em->remove($soustraitantSelected);
        }

        $this->em->flush();
    }

    public function association($data)
    {
        foreach ($data['organisation'] as $organisation) {
            foreach ($data['fields'] as $soustraitant) {
                $soustraitantSelected = $this->soustraitantRepository->findOneBy(['id' => $soustraitant]);

                $soustraitantSelected
                    ->addOrganisation($organisation)
                ;

                $this->em->persist($soustraitantSelected);
            }
        }

        $this->em->flush();
    }

    public function dissociation(Soustraitant $soustraitant, Organisation $organisation)
    {
        $soustraitant->removeOrganisation($organisation);

        $this->em->persist($soustraitant);
        $this->em->flush();
    }

    public function dissociationMultiple($data, Organisation $organisation)
    {
        foreach ($data['fields'] as $typage) {
            $soustraitantSelected = $this->soustraitantRepository->findOneBy(['id' => $typage]);

            $soustraitantSelected
                ->removeOrganisation($organisation)
            ;

            $this->em->persist($soustraitantSelected);
        }

        $this->em->flush();
    }
}
