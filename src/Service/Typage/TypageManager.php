<?php

namespace App\Service\Typage;

use App\Entity\Organisation;
use App\Entity\Typage;
use App\Repository\TypageRepository;
use Doctrine\ORM\EntityManagerInterface;

class TypageManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly TypageRepository $typageRepository
    ) {
    }

    public function add(Typage $typage, Organisation $currentOrganisation): void
    {
        $typage->setCreatedByOrganisation($currentOrganisation);

        $this->save($typage);
    }

    public function save(Typage $typage): void
    {
        $this->em->persist($typage);

        $this->em->flush();
    }

    public function association($data)
    {
        foreach ($data['organisation'] as $organisation) {
            foreach ($data['fields'] as $typage) {
                $typageSelected = $this->typageRepository->findOneBy(['id' => $typage]);

                $typageSelected
                    ->addOrganisation($organisation)
                ;

                $this->em->persist($typageSelected);
            }
        }

        $this->em->flush();
    }

    public function dissociation(Typage $typage, Organisation $organisation)
    {
        $typage->removeOrganisation($organisation);

        $this->em->persist($typage);
        $this->em->flush();
    }

    public function dissociationMultiple($data, Organisation $organisation)
    {
        foreach ($data as $typage) {
            $typageSelected = $this->typageRepository->findOneBy(['id' => $typage]);

            $typageSelected
                ->removeOrganisation($organisation)
            ;

            $this->em->persist($typageSelected);
        }

        $this->em->flush();
    }

    public function delete(Typage $typage, Organisation $organisation): void
    {
        if ($typage->getCreatedByOrganisation() === $organisation) {
            $this->em->remove($typage);
            $this->em->flush();
        }
    }

    public function deleteMultiple($data, Organisation $organisation)
    {
        foreach ($data as $typage) {
            $typageSelected = $this->typageRepository->findOneBy([
                'id' => $typage,
                'createdByOrganisation' => $organisation
            ]);

            if ($typageSelected !== null) {
                $this->em->remove($typageSelected);
            }
        }

        $this->em->flush();
    }
}
