<?php

namespace App\Service\Role;

use App\Entity\Role;
use App\Repository\RoleRepository;

class RoleManager
{
    public function __construct(private readonly RoleRepository $roleRepository)
    {
    }

    public function getSuperadminRole(): Role
    {
        return $this->roleRepository->findOneBy(['name' => 'Superadmin']);
    }

    public function getAdminRole(): Role
    {
        return $this->roleRepository->findOneBy(['name' => 'Administrateur']);
    }

    public function getDpoRole(): Role
    {
        return $this->roleRepository->findOneBy(['name' => 'DPO']);
    }

    public function getValideurRole(): Role
    {
        return $this->roleRepository->findOneBy(['name' => 'Valideur']);
    }

    public function getRedacteurRole(): Role
    {
        return $this->roleRepository->findOneBy(['name' => 'Rédacteur']);
    }

    public function getConsultantRole(): Role
    {
        return $this->roleRepository->findOneBy(['name' => 'Consultant']);
    }
}
