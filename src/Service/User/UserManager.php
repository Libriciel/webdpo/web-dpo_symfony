<?php

namespace App\Service\User;

use App\Entity\OrganisationUserRole;
use App\Entity\User;
use App\Repository\OrganisationRepository;
use App\Repository\UserRepository;
use App\Security\Password\ResetPassword;
use App\Service\OrganisationUserRole\OrganisationUserRoleManager;
use App\Service\Role\RoleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly Security $security,
        private readonly UserRepository $userRepository,
        private readonly OrganisationRepository $organisationRepository,
        private readonly RoleManager $roleManager,
        private readonly OrganisationUserRoleManager $organisationUserRoleManager,
        private readonly ResetPassword $resetPassword
    ) {
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function saveSuperadmin(User $user)
    {
        $user->setIsSuperadmin(true);

        $user = $this->setFirstPassword($user);

        $this->em->persist($user);

        $organisationUserRole = new OrganisationUserRole();

        $organisationUserRole->setUser($user);
        $organisationUserRole->setRole($this->roleManager->getSuperAdminRole());

        $this->organisationUserRoleManager->save($organisationUserRole);

        $this->em->flush();
    }

    private function addPassword(User $user, ?string $plainPassword)
    {
        if ($plainPassword) {
            $user->setPassword($this->passwordHasher->hashPassword($user, $plainPassword));
        }

        return $user;
    }

    /**
     * @throws \Exception
     */
    private function setFirstPassword(User $user)
    {
        $user->setPassword($this->passwordHasher->hashPassword($user, random_bytes(40)));
        $this->resetPassword->definePassword($user);

        return $user;
    }

    /**
     * @throws \Exception
     */
    public function add(User $user)
    {
        $user = $this->setFirstPassword($user);

        $this->save($user);
    }

    /**
     * @param User $user
     */
    public function save(User $user): void
    {
        $this->em->persist($user);

        $this->em->flush();
    }

    /**
     * @param User $user
     */
    public function delete(User $user): void
    {
        $this->em->remove($user);
        $this->em->flush();
    }

    /**
     * @param $data
     * @return void
     */
    public function deleteMultiple($data): void
    {
        foreach ($data as $user) {
            $userSelected = $this->userRepository->findOneBy([
                'id' => $user
            ]);

            if ($userSelected !== null) {
                $this->em->remove($userSelected);
            }
        }

        $this->em->flush();
    }

    public function getUsers(User $user)
    {
        if ($this->security->isGranted('ROLE_SUPERADMIN')) {
            return $this->userRepository->findAllUser();
        } else {
            $organisations = $this->organisationRepository->findOrganisationByUser($user);
            return $this->userRepository->findAllUsersForAuthorizedOrganisations($organisations);
        }
    }

    public function currentOrganisationLogout(User $user)
    {
        $user->setCurrentOrganisation(null);

        $this->em->persist($user);

        $this->em->flush();
    }

    /**
     * @param User $user
     * @param $organisation
     * @return void
     */
    public function addCurrentOrganisationLogin(User $user, $organisation): void
    {
        $user->setCurrentOrganisation($organisation);

        $this->em->persist($user);

        $this->em->flush();
    }

    /**
     * @param $data
     * @return void
     */
    public function toggle($data): void
    {
        foreach ($data as $user) {
            $userSelected = $this->userRepository->findOneBy(['id' => $user]);
            $userSelected->setIsActive(!$userSelected->getIsActive());

            $this->em->persist($userSelected);
        }

        $this->em->flush();
    }
}
