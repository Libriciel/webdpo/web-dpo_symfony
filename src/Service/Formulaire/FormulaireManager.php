<?php

namespace App\Service\Formulaire;

use App\Entity\Formulaire;
use App\Entity\Option;
use App\Entity\Organisation;
use App\Repository\FormulaireRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class FormulaireManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly FormulaireRepository $formulaireRepository
    ) {
    }

    /**
     * @param Formulaire $formulaire
     * @param Organisation $currentOrganisation
     * @param $saveRegistreDestination
     * @return string
     */
    public function add(Formulaire $formulaire, Organisation $currentOrganisation, $saveRegistreDestination): string
    {
        $formulaire->setCreatedByOrganisation($currentOrganisation);

        if ($saveRegistreDestination === 'IsActivite') {
            $formulaire->setIsActivite(true);
        } elseif ($saveRegistreDestination === 'IsRtExterne') {
            $formulaire->setIsRtExterne(true);
        } elseif ($saveRegistreDestination === 'IsViolation') {
            $formulaire->setIsViolation(true);
        } else {
            throw new BadRequestException('Type de registre n\'existe pas');
        }

        $formulaire->setOptions(new Option());

        $this->em->persist($formulaire);
        $this->em->flush();

        return $formulaire->getId();
    }

    /**
     * @param Formulaire $formulaire
     * @return void
     */
    public function save(Formulaire $formulaire, $fields): void
    {
        dump($fields);

        $test = json_decode($fields);

        dd($test);

        $this->em->persist($formulaire);
        $this->em->flush();
    }

    /**
     * @param Formulaire $formulaire
     * @param Organisation $organisation
     * @return void
     */
    public function delete(Formulaire $formulaire, Organisation $organisation): void
    {
        if ($formulaire->getCreatedByOrganisation() === $organisation &&
            count($formulaire->getOrganisations()) === 0
        ) {
            $this->em->remove($formulaire);
            $this->em->flush();
        }
    }

    /**
     * @param $data
     * @param Organisation $organisation
     * @return void
     */
    public function deleteMultiple($data, Organisation $organisation): void
    {
        foreach ($data as $formulaire) {
            $formulaireSelected = $this->formulaireRepository->findOneBy([
                'id' => $formulaire,
                'createdByOrganisation' => $organisation
            ]);

            if ($formulaireSelected !== null &&
                $formulaireSelected->getCreatedByOrganisation() === $organisation &&
                count($formulaireSelected->getOrganisations()) === 0
            ) {
                $this->em->remove($formulaireSelected);
            }
        }

        $this->em->flush();
    }

    /**
     * @param $data
     * @return void
     */
    public function toggle($data): void
    {
        foreach ($data as $formulaire) {
            $formulaireSelected = $this->formulaireRepository->findOneBy(['id' => $formulaire]);
            $formulaireSelected->setIsActive(!$formulaireSelected->getIsActive());

            $this->em->persist($formulaireSelected);
        }

        $this->em->flush();
    }

    /**
     * @param $data
     * @return void
     */
    public function archived($data): void
    {
        //TODO : Check if formulaire no organisations

        foreach ($data as $formulaire) {
            $formulaireSelected = $this->formulaireRepository->findOneBy(['id' => $formulaire]);
            $formulaireSelected->setIsArchived(!$formulaireSelected->getIsArchived());

            $this->em->persist($formulaireSelected);
        }

        $this->em->flush();
    }

    /**
     * @param $data
     * @return void
     */
    public function association($data)
    {
        foreach ($data['organisation'] as $organisation) {
            foreach ($data['fields'] as $formulaire) {
                $formulaireSelected = $this->formulaireRepository->findOneBy(['id' => $formulaire]);

                $formulaireSelected
                    ->addOrganisation($organisation)
                ;

                $this->em->persist($formulaireSelected);
            }
        }

        $this->em->flush();
    }

    /**
     * @param Formulaire $formulaire
     * @param Organisation $organisation
     * @return void
     */
    public function dissociation(Formulaire $formulaire, Organisation $organisation): void
    {
        $formulaire->removeOrganisation($organisation);

        $this->em->persist($formulaire);
        $this->em->flush();
    }

    /**
     * @param $data
     * @param Organisation $organisation
     * @return void
     */
    public function dissociationMultiple($data, Organisation $organisation): void
    {
        foreach ($data as $formulaire) {
            $formulaireSelected = $this->formulaireRepository->findOneBy(['id' => $formulaire]);

            $formulaireSelected
                ->removeOrganisation($organisation)
            ;

            $this->em->persist($formulaireSelected);
        }

        $this->em->flush();
    }
}
