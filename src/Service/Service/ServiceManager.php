<?php

namespace App\Service\Service;

use App\Entity\Organisation;
use App\Entity\Service;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;

class ServiceManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ServiceRepository $serviceRepository
    ) {
    }

    public function association($data)
    {
        foreach ($data['user'] as $user) {
            foreach ($data['fields'] as $service) {
                $serviceSelected = $this->serviceRepository->findOneBy(['id' => $service]);

                $serviceSelected
                    ->addUser($user)
                ;

                $this->em->persist($serviceSelected);
            }
        }

        $this->em->flush();
    }

    public function save(Service $service, Organisation $currentOrganisation): void
    {
        $service->setOrganisation($currentOrganisation);

        $this->em->persist($service);

        $this->em->flush();
    }

    public function delete(Service $service): void
    {
        $this->em->remove($service);
        $this->em->flush();
    }

    public function deleteMultiple($data)
    {
        foreach ($data as $service) {
            $serviceSelected = $this->serviceRepository->findOneBy([
                'id' => $service,
            ]);

            if ($serviceSelected !== null) {
                $this->em->remove($serviceSelected);
            }
        }

        $this->em->flush();
    }
}
