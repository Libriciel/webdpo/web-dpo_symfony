<?php

namespace App\Service\Referentiel;

use App\Entity\File;
use App\Entity\Referentiel;
use App\Repository\ReferentielRepository;
use App\Service\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ReferentielManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly FileManager $fileManager,
        private readonly ParameterBagInterface $bag,
        private readonly ReferentielRepository $referentielRepository
    ) {
    }

    public function save(Referentiel $referentiel): void
    {
        $this->em->persist($referentiel);

        $this->em->flush();
    }

    /**
     * @param Referentiel $referentiel
     * @param array|null $uploadedFile
     */
    public function add(Referentiel $referentiel, $uploadedFile = null): void
    {
        if ($uploadedFile) {
            $folderReferentiels = $this->bag->get('folder_referentiels');

            $path = $this->fileManager->saveFileOnDisk($folderReferentiels, $uploadedFile);

            $newFile = $this->createNewReferentielFile(
                $uploadedFile->getClientOriginalName(),
                $path
            );
            $referentiel->setFile($newFile);
        }

        $this->save($referentiel);
    }

    private function createNewReferentielFile(string $fileName, string $path): File
    {
        $referentielFile = new File();
        $referentielFile->setFileName($fileName)
            ->setPath($path)
        ;

        $this->em->persist($referentielFile);

        return $referentielFile;
    }

    public function abroger($data)
    {
        foreach ($data as $referentiel => $abroger) {
            $userSelected = $this->referentielRepository->findOneBy(['id' => $referentiel]);
            $userSelected->setAbroger(!$abroger);

            $this->em->persist($userSelected);
        }

        $this->em->flush();
    }

    public function delete(Referentiel $referentiel): void
    {
        if ($referentiel->getAbroger() === true) {
            $this->em->remove($referentiel);
            $this->em->flush();
        }
    }
}
