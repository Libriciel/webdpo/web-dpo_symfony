<?php

namespace App\Service\Article;

use App\Entity\Article;
use App\Entity\File;
use App\Entity\Organisation;
use App\Repository\ArticleRepository;
use App\Service\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ArticleManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly FileManager $fileManager,
        private readonly ParameterBagInterface $bag,
        private readonly ArticleRepository $articleRepository
    ) {
    }

    /**
     * @param Article $article
     * @param Organisation $currentOrganisation
     * @param array|null $uploadedFiles
     */
    public function add(Article $article, Organisation $currentOrganisation, ?array $uploadedFiles = []): void
    {
        if ($uploadedFiles) {
            $folderArticles = $this->bag->get('folder_articles');

            foreach ($uploadedFiles as $uploadedFile) {
                $path = $this->fileManager->saveFileOnDisk($folderArticles, $uploadedFile);

                $newFile = $this->createNewArticleFile(
                    $uploadedFile->getClientOriginalName(),
                    $path,
                    $article
                );
                $article->addFile($newFile);
            }
        }

        $article->setCreatedByOrganisation($currentOrganisation);

        $this->save($article);
    }

    private function createNewArticleFile(string $fileName, string $path, Article $article): File
    {
        $articleFile = new File();
        $articleFile->setFileName($fileName)
            ->setPath($path)
            ->setArticle($article);

        $this->em->persist($articleFile);

        return $articleFile;
    }

    public function save(Article $article): void
    {
        $this->em->persist($article);

        $this->em->flush();
    }

    public function association($data)
    {
        foreach ($data['organisation'] as $organisation) {
            foreach ($data['fields'] as $typage) {
                $articleSelected = $this->articleRepository->findOneBy(['id' => $typage]);

                $articleSelected
                    ->addOrganisation($organisation)
                ;

                $this->em->persist($articleSelected);
            }
        }

        $this->em->flush();
    }

    public function dissociation(Article $article, Organisation $organisation)
    {
        $article->removeOrganisation($organisation);

        $this->save($article);
    }

    public function dissociationMultiple($data, Organisation $organisation)
    {
        foreach ($data['fields'] as $article) {
            $articleSelected = $this->articleRepository->findOneBy(['id' => $article]);

            $articleSelected
                ->removeOrganisation($organisation)
            ;

            $this->em->persist($articleSelected);
        }

        $this->em->flush();
    }

    public function delete(Article $article, Organisation $organisation): void
    {
        if ($article->getCreatedByOrganisation() === $organisation) {
            $this->em->remove($article);
            $this->em->flush();
        }
    }

    public function deleteMultiple($data, Organisation $organisation)
    {
        foreach ($data as $article) {
            $articleSelected = $this->articleRepository->findOneBy([
                'id' => $article,
                'createdByOrganisation' => $organisation
            ]);

            if ($articleSelected !== null) {
                $this->em->remove($articleSelected);
            }
        }

        $this->em->flush();
    }
}
