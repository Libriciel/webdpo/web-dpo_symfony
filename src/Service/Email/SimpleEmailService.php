<?php

namespace App\Service\Email;

use App\Service\EmailTemplate\HtmlTag;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class SimpleEmailService implements EmailServiceInterface
{
    public function __construct(private readonly MailerInterface $mailer, private readonly ParameterBagInterface $bag)
    {
    }

    /**
     * @param EmailData[] $emailsData
     *
     * @throws EmailNotSendException
     */
    public function sendBatch(array $emailsData): void
    {
        foreach ($emailsData as $emailData) {
            $email = (new Email())
                ->from($this->bag->get('email_from'))
                ->to($emailData->getTo())
                ->subject($emailData->getSubject())
            ;

            if ($emailData->getReplyTo()) {
                $email->replyTo($emailData->getReplyTo());
            }

            $this->setAttachment($email, $emailData);

            try {
                $this->mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                throw new EmailNotSendException($e->getMessage());
            }
        }
    }

    private function setAttachment(Email $email, EmailData $emailData)
    {
        if (!$emailData->isAttachment() || !file_exists($emailData->getAttachment()->getPath())) {
            return;
        }

        if (filesize($emailData->getAttachment()->getPath()) > $this->bag->get('max_email_attachment_file_size')) {
            return;
        }

        $email->attachFromPath($emailData->getAttachment()->getPath(), $emailData->getAttachment()->getFileName());
    }

    private function getFormattedContent(EmailData $email): string
    {
        if (EmailData::FORMAT_TEXT === $email->getFormat()) {
            return $email->getContent();
        }

        return HtmlTag::START_HTML . $email->getContent() . HtmlTag::END_HTML;
    }

    private function selectEmailFormat(string $format): string
    {
        if (EmailData::FORMAT_TEXT === $format) {
            return 'text/plain';
        }

        return 'text/html';
    }
}
