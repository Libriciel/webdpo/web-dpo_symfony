<?php

namespace App\Service\Email;

class EmailServiceFactory
{
    public const MAILJET = 'mailjet';

    //    public function __construct(SimpleEmailService $simpleEmailService, MailjetService $mailjetService)
    public function __construct(private readonly SimpleEmailService $simpleEmailService)
    {
    }

    public function chooseImplementation(): EmailServiceInterface
    {
        if (self::MAILJET === getenv('MAILER_TYPE')) {
            dd('MAILJET');
            //            return $this->mailjetService;
        }

        return $this->simpleEmailService;
    }
}
