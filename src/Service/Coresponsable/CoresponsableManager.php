<?php

namespace App\Service\Coresponsable;

use App\Entity\Coresponsable;
use App\Entity\Organisation;
use App\Repository\CoresponsableRepository;
use App\Service\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CoresponsableManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly FileManager $fileManager,
        private readonly ParameterBagInterface $bag,
        private readonly CoresponsableRepository $coresponsableRepository
    ) {
    }

    public function add(Coresponsable $coresponsable, ?UploadedFile $uploadedFile): void
    {
        if (!empty($uploadedFile)) {
            $folderLogos = $this->bag->get('folder_logos');
            $newFilename = $this->fileManager->saveFileOnDisk($folderLogos, $uploadedFile);

            $coresponsable->setLogo($newFilename);
        }

        $this->save($coresponsable);
    }

    public function save(Coresponsable $coresponsable): void
    {
        $this->em->persist($coresponsable);
        $this->em->flush();
    }

    public function delete(Coresponsable $coresponsable): void
    {
        $this->em->remove($coresponsable);
        $this->em->flush();
    }

    public function deleteMultiple($data)
    {
        foreach ($data as $coresponsable) {
            $coresponsableSelected = $this->coresponsableRepository->findOneBy([
                'id' => $coresponsable
            ]);

            if (!$coresponsableSelected) {
                throw new BadRequestException('Le co-responsable n\'existe pas');
            }

            if (count($coresponsableSelected->getOrganisations()) !== 0) {
                throw new BadRequestException('Le co-responsable ne peux pas être supprimer, car il est associer à une entité');
                //                throw new NotFoundResourceException('Le co-responsable ne peux pas être supprimer, car il est associer à une entité');
            }

            $this->em->remove($coresponsableSelected);
        }

        $this->em->flush();
    }

    public function association($data)
    {
        foreach ($data['organisation'] as $organisation) {
            foreach ($data['fields'] as $coresponsable) {
                $coresponsableSelected = $this->coresponsableRepository->findOneBy(['id' => $coresponsable]);

                $coresponsableSelected
                    ->addOrganisation($organisation)
                ;

                $this->em->persist($coresponsableSelected);
            }
        }

        $this->em->flush();
    }

    public function dissociation(Coresponsable $coresponsable, Organisation $organisation)
    {
        $coresponsable->removeOrganisation($organisation);

        $this->save($coresponsable);
    }

    public function dissociationMultiple($data, Organisation $organisation)
    {
        foreach ($data as $coresponsable) {
            $coresponsableSelected = $this->coresponsableRepository->findOneBy(['id' => $coresponsable]);

            $coresponsableSelected
                ->removeOrganisation($organisation)
            ;

            $this->em->persist($coresponsableSelected);
        }

        $this->em->flush();
    }
}
