<?php

namespace App\Form;

use App\Entity\Formulaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormulaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $valueRegistre = '';
        $disabled = false;
        if ($options['isEditMode'] === true) {
            if ($options['isActivite'] === true) {
                $valueRegistre = 'IsActivite';
            } elseif ($options['isRtExterne'] === true) {
                $valueRegistre = 'IsRtExterne';
            } elseif ($options['isViolation'] === true) {
                $valueRegistre = 'IsViolation';
            }

            $disabled = true;
        }

        $builder
            ->add('name', TextType::class, [
                'label' => 'form.formulaire.name.label',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'form.formulaire.description.label',
                'required' => false,
            ])
            ->add('registre', ChoiceType::class, [
                'label' => 'form.formulaire.registre.label',
                'mapped' => false,
                'required' => true,
                'choices' => [
                    'Registre d\'activité' => 'IsActivite',
                    'Registre de sous-traitance' => 'IsRtExterne',
                    'Registre de violation de données' => 'IsViolation',
                ],
                'data' => $valueRegistre,
                'disabled' => $disabled
            ])
        ;

        if ($options['isEditMode'] === true) {
            $builder
                ->add('options', OptionType::class, [
                    'label' => false,
                ])
                ->add('tests', HiddenType::class, [
                    'mapped' => false
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Formulaire::class,
            'translation_domain' => 'formulaire',
            'isEditMode' => false,
            'isActivite' => false,
            'isRtExterne' => false,
            'isViolation' => false,
        ]);
    }
}
