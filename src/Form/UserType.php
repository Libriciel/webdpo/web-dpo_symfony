<?php

namespace App\Form;

use App\Entity\Organisation;
use App\Entity\User;
use App\Repository\OrganisationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class UserType extends AbstractType
{
    public function __construct(private readonly OrganisationRepository $organisationRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['isAdminMode'] === false) {
            /** @var User|null $user */
            $user = $builder->getData();

            $dataOrganisations = null;
            if (!empty($user->getId())) {
                $dataOrganisations = $this->organisationRepository->findAllStructuresByUser($user);
            }

            $builder
                ->add('organisations', EntityType::class, [
                    'mapped' => false,
                    'class' => Organisation::class,
                    'query_builder' => $this->organisationRepository->findOrganisationByUserQB($user),
                    'data' => $dataOrganisations,
                    'choice_label' => 'raisonsociale',
                    'multiple' => true,
                    'label' => 'form.user.organisations.label',
                    'required' => true,
                    'attr' => [
                        'class' => 'select2-multi',
                        'id' => "organisations",
                    ]
                ])
                ->add('organisationUserRoles', CollectionType::class, [
                    'entry_type' => OrganisationUserRoleCollectionType::class,
                    'allow_delete' => true,
                    'allow_add' => true,
                    'prototype' => true,
                    'by_reference' => false,
                    'label' => false,
                ])
            ;
        }

        $optionsCivility = [
            'Madame' => 'Mme.',
            'Monsieur' => 'M.',
        ];
        $optionsNotification = [
            'Non' => false,
            'Oui' => true,
        ];

        $disabled = false;
        if ($options['isEditMode'] === true) {
            $disabled = true;
        } else {
            $optionEmpty = [
                '' => null
            ];
            $optionsCivility = array_merge($optionEmpty, $optionsCivility);
            $optionsNotification = array_merge($optionEmpty, $optionsNotification);
        }

        $builder
            ->add('username', TextType::class, [
                'label' => 'form.user.username.label',
                'disabled' => $disabled
            ])
            ->add('civility', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsCivility,
                'label' => 'form.user.civility.label',
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'label' => 'form.user.firstName.label',
                'attr' => [
                    'class' => 'formatFirstName'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
                'label' => 'form.user.lastName.label',
                'attr' => [
                    'class' => 'formatLastName'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'form.user.email.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('cellphone', TelType::class, [
                'required' => false,
                'label' => 'form.user.cellphone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[6|7]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone portable doit être de la forme 06xxxxxxxx ou 07xxxxxxxx'
                    ),
                ]
            ])
            ->add('phone', TelType::class, [
                'required' => false,
                'label' => 'form.user.phone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('notification', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsNotification,
                'label' => 'form.user.notification.label',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'user',
            'userLogged' => null,
            'isEditMode' => false,
            'isAdminMode' => false,
        ]);
    }
}
