<?php

namespace App\Form;

use App\Entity\Soustraitant;
use App\Form\Type\LsFileType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Regex;

class SoustraitantType extends AbstractType
{
    public function __construct(private readonly ParameterBagInterface $bag)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('raisonsociale', TextType::class, [
                'label' => 'form.soustraitant.raisonsociale.label',
                'required' => true
            ])
            ->add('phone', TelType::class, [
                'label' => 'form.soustraitant.telephone.label',
                'required' => false,
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('fax', TelType::class, [
                'label' => 'form.soustraitant.fax.label',
                'required' => false,
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('address', TextareaType::class, [
                'label' => 'form.soustraitant.adresse.label',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.soustraitant.email.label',
                'required' => false
            ])
            ->add('siren', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'maskSiren'
                ]
            ])
            ->add('siret', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'maskSiret'
                ]
            ])
            ->add('ape', TextType::class, [
                'label' => '',
                'required' => false,
                'attr' => [
                    'class' => 'maskApe'
                ]
            ])
            ->add('logo', LsFileType::class, [
                'label' => 'Logo du sous-traitant',
                'required' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => $this->bag->get('logo_mime_types_authorized'),
                        'mimeTypesMessage' => "Seuls les fichiers au format '.png', '.jpeg', '.jpg' et '.svg' sont acceptés.",
                        'maxSize' => $this->bag->get('document_max_size'),
                    ])
                ],
                'attr' => [
                    'lang' => 'fr',
                    'accept' => '.png, .PNG, .jpeg, .JPEG, .jpg, .JPG, .svg, .SVG',
                    'placeholder' => 'Selectionnez un fichier'
                ]
            ])
            ->add('representant', RepresentantType::class, [
                'label' => false,
            ])
            ->add('delegue', DelegueType::class, [
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Soustraitant::class,
            'translation_domain' => 'soustraitant',
        ]);
    }
}
