<?php

namespace App\Form;

use App\Entity\Dpo;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DpoType extends AbstractType
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'query_builder' => $this->userRepository->findAllQB(),
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('u')
//                        ->leftJoin('u.dpo', 'd')
//                        ->where('d.user != :user');
//                },

                'choice_label' => 'nomcomplet',
                'multiple' => false,
                'label' => 'form.dpo.user.label',
                'attr' => [
//                    'class' => 'select2',
                    'required' => true
                ]
            ])
            ->add('number', TextType::class, [
                'required' => true,
                'label' => 'form.dpo.number.label',
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'form.dpo.email.label',
            ])
//            ->add('organisation')
//            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dpo::class,
            'translation_domain' => 'dpo',
        ]);
    }
}
