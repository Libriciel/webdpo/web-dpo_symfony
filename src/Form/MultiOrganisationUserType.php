<?php

namespace App\Form;

use App\Entity\Organisation;
use App\Repository\OrganisationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class MultiOrganisationUserType extends AbstractType
{
    public function __construct(
        private readonly Security $security,
        private readonly OrganisationRepository $organisationRepository
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organisation', EntityType::class, [
                'class' => Organisation::class,
                'query_builder' => $this->organisationRepository->findOrganisationByUserQB($this->security->getUser()),
                'choice_label' => 'raisonsociale',
                'multiple' => true,
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ],
                'label' => false,
                'attr' => [
                    'class' => 'select2-multi',
                ]
            ])
            ->add('fields', CollectionType::class, [
                'allow_add' => true,
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
