<?php

namespace App\Form;

use App\Entity\Dirigeant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class DirigeantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civility', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    '' => null,
                    'Madame' => 'Mme.',
                    'Monsieur' => 'M.',
                ],
                'label' => 'form.dirigeant.civility.label',
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'label' => 'form.dirigeant.firstName.label',
                'attr' => [
                    'class' => 'formatFirstName'
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
                'label' => 'form.dirigeant.lastName.label',
                'attr' => [
                    'class' => 'formatLastName'
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'label' => 'form.dirigeant.email.label',
            ])
            ->add('phone', TelType::class, [
                'required' => false,
                'label' => 'form.dirigeant.phone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('function', TextType::class, [
                'required' => true,
                'label' => 'form.dirigeant.function.label',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dirigeant::class,
            'translation_domain' => 'dirigeant',
        ]);
    }
}
