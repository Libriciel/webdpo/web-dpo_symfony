<?php

namespace App\Form;

use App\Entity\Organisation;
use App\Entity\OrganisationUserRole;
use App\Entity\Role;
use App\Repository\OrganisationRepository;
use App\Repository\RoleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrganisationUserRoleCollectionType extends AbstractType
{
    public function __construct(
        private readonly Security $security,
        private readonly RoleRepository $roleRepository,
        private readonly OrganisationRepository $organisationRepository
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organisation', EntityType::class, [
                'class' => Organisation::class,
                'query_builder' => $this->organisationRepository->findOrganisationByUserQB($this->security->getUser()),
                'choice_label' => 'raisonsociale',
                'multiple' => false,
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ],
                'label' => false,
                'attr' => [
                    'class' => 'hidden'
                ]
            ])
            ->add('role', EntityType::class, [
                'class' => Role::class,
                'query_builder' => $this->roleRepository->getRoleForUser(),
                'choice_label' => 'name',
                'label' => '__labelRole__',
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrganisationUserRole::class,
        ]);
    }
}
