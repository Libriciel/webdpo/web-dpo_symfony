<?php

namespace App\Form;

use App\Entity\Option;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $optionsFields = [
            'Non' => false,
            'Oui' => true
        ];

        $builder
            ->add('useSousFinalite', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsFields,
                'label' => 'form.option.useSousFinalite.label',
            ])
            ->add('useBaseLegale', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsFields,
                'label' => 'form.option.useBaseLegale.label',
            ])
            ->add('useDecisionAutomatisee', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsFields,
                'label' => 'form.option.useDecisionAutomatisee.label',
            ])
            ->add('useTransfertHorsUe', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsFields,
                'label' => 'form.option.useTransfertHorsUe.label',
            ])
            ->add('useDonneesSensible', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsFields,
                'label' => 'form.option.useDonneesSensible.label',
            ])
            ->add('useAllExtensionFiles', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsFields,
                'label' => 'form.option.useAllExtensionFiles.label',
            ])
            ->add('usePia', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsFields,
                'label' => 'form.option.usePia.label',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Option::class,
            'translation_domain' => 'option',
        ]);
    }
}
