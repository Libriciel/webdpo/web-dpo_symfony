<?php

namespace App\Form;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UsersForOrganisationType extends AbstractType
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'query_builder' => $this->userRepository->findAllUsersForOrganisation($options['userCurrentOrganisation']),
                'choice_label' => 'nomcomplet',
                'multiple' => true,
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ],
                'label' => false,
                'attr' => [
                    'class' => 'select2-multi',
                ]
            ])
            ->add('fields', CollectionType::class, [
                'allow_add' => true,
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'userCurrentOrganisation' => null,
        ]);
    }
}
