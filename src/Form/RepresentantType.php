<?php

namespace App\Form;

use App\Entity\Representant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class RepresentantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('civility', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Madame' => 'Mme.',
                    'Monsieur' => 'M.',
                ],
                'label' => 'form.representant.civility.label',
            ])
            ->add('firstName', TextType::class, [
                'required' => false,
                'label' => 'form.representant.firstName.label',
                'attr' => [
                    'class' => 'formatFirstName'
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'label' => 'form.representant.lastName.label',
                'attr' => [
                    'class' => 'formatLastName'
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'label' => 'form.representant.email.label',
            ])
            ->add('phone', TelType::class, [
                'required' => false,
                'label' => 'form.representant.phone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('function', TextType::class, [
                'required' => false,
                'label' => 'form.representant.function.label',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Representant::class,
            'translation_domain' => 'representant',
        ]);
    }
}
