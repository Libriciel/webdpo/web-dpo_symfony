<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ArticleType extends AbstractType
{
    public function __construct(private readonly ParameterBagInterface $bag)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $mimeTypes = $this->bag->get('article_mime_types_authorized');
        $maxSize = $this->bag->get('document_max_size');

        $builder
            ->add('name', TextType::class, [
                'label' => 'form.article.name.label',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ]
            ])
//            ->add('files', LsFileType::class, [
//                'label' => 'form.article.files.label',
//                'mapped' => false,
//                'required' => false,
//                'multiple' => true,
//                'constraints' => [
//                    new All([
//                        'constraints' => [
//                            new File([
//                                'mimeTypes' => $mimeTypes,
//                                'mimeTypesMessage' => "Seuls les fichiers au format '.png', '.jpeg', '.jpg', '.doc', '.docx', '.xls', '.xlsx', '.ods', '.odt', '.pdf' sont acceptés ",
//                                'maxSize' => $maxSize,
//                                'maxSizeMessage' => sprintf("La taille du ou des fichiers ne peut pas dépasser %s", $maxSize)
//                            ])
//                        ]
//                    ])
//                ],
//                'attr' => [
//                    'lang' => 'fr',
//                    'accept' => '.png, .PNG, .jpeg, .JPEG, .jpg, .JPG, .svg, .SVG',
//                    'placeholder' => 'form.article.files.placeholder'
//                ]
//            ])
            ->add('files', FileType::class, [
                'label' => 'form.article.files.label',
                'mapped' => false,
                'required' => false,
                'multiple' => true,
                'constraints' => [
                    new All([
                        'constraints' => [
                            new File([
                                'mimeTypes' => $mimeTypes,
                                'mimeTypesMessage' => "Seuls les fichiers au format '.png', '.jpeg', '.jpg', '.doc', '.docx', '.xls', '.xlsx', '.ods', '.odt', '.pdf' sont acceptés ",
                                'maxSize' => $maxSize,
                                'maxSizeMessage' => sprintf("La taille du ou des fichiers ne peut pas dépasser %s", $maxSize)
                            ])
                        ]
                    ])
                ],
                'attr' => [
                    'lang' => 'fr',
                    'accept' => '.png, .PNG, .jpeg, .JPEG, .jpg, .JPG, .svg, .SVG',
                    'placeholder' => 'form.article.files.placeholder'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'form.article.description.label',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ],
                'attr' => [
                    'class' => 'tinymceField'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'translation_domain' => 'article',
        ]);
    }
}
