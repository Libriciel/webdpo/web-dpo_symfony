<?php

namespace App\Form;

use App\Entity\Delegue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class DelegueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('civility', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Madame' => 'Mme.',
                    'Monsieur' => 'M.',
                ],
                'label' => 'form.delegue.civility.label',
            ])
            ->add('firstName', TextType::class, [
                'required' => false,
                'label' => 'form.delegue.firstName.label',
                'attr' => [
                    'class' => 'formatFirstName'
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'label' => 'form.delegue.lastName.label',
                'attr' => [
                    'class' => 'formatLastName'
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => false,
                'label' => 'form.delegue.email.label',
            ])
            ->add('cellphone', TelType::class, [
                'required' => false,
                'label' => 'form.delegue.cellphone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[6|7]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone portable doit être de la forme 06xxxxxxxx ou 07xxxxxxxx'
                    ),
                ]
            ])
            ->add('phone', TelType::class, [
                'required' => false,
                'label' => 'form.delegue.phone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('numberCnil', TextType::class, [
                'required' => false,
                'label' => 'form.delegue.numberCnil.label',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Delegue::class,
            'translation_domain' => 'delegue',
        ]);
    }
}
