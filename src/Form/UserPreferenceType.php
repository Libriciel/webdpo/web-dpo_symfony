<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class UserPreferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $optionsCivility = [
            'Madame' => 'Mme.',
            'Monsieur' => 'M.',
        ];
        $optionsNotification = [
            'Non' => false,
            'Oui' => true,
        ];

        $builder
            ->add('username', TextType::class, [
                'label' => 'form.user.username.label',
                'disabled' => true,
                'attr' => [
                    'readonly' => true,
                ],
            ])
            ->add('civility', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsCivility,
                'label' => 'form.user.civility.label',
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'label' => 'form.user.firstName.label',
                'attr' => [
                    'class' => 'formatFirstName'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => true,
                'label' => 'form.user.lastName.label',
                'attr' => [
                    'class' => 'formatLastName'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'form.user.email.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('cellphone', TelType::class, [
                'required' => false,
                'label' => 'form.user.cellphone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[6|7]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone portable doit être de la forme 06xxxxxxxx ou 07xxxxxxxx'
                    ),
                ]
            ])
            ->add('phone', TelType::class, [
                'required' => false,
                'label' => 'form.user.phone.label',
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('notification', ChoiceType::class, [
                'required' => true,
                'choices' => $optionsNotification,
                'label' => 'form.user.notification.label',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'user'
        ]);
    }
}
