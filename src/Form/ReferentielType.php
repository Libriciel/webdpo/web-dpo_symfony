<?php

namespace App\Form;

use App\Entity\Referentiel;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReferentielType extends AbstractType
{
    public function __construct(private readonly ParameterBagInterface $bag)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $mimeTypes = $this->bag->get('referentiel_mime_types_authorized');
        $maxSize = $this->bag->get('document_max_size');

        $builder
            ->add('name', TextType::class, [
                'label' => 'form.referentiel.name.label',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('file', FileType::class, [
                'label' => 'form.referentiel.file.label',
                'mapped' => false,
                'required' => false,
                'multiple' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => $mimeTypes,
                        'mimeTypesMessage' => "Seuls les fichiers au format '.pdf' sont acceptés ",
                        'maxSize' => $maxSize,
                        'maxSizeMessage' => sprintf("La taille du ou des fichiers ne peut pas dépasser %s", $maxSize)
                    ])
                ],
                'attr' => [
                    'lang' => 'fr',
                    'accept' => '.pdf, .PDF',
                    'placeholder' => 'form.referentiel.file.placeholder'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'form.referentiel.description.label',
                'required' => false,
                'attr' => [
                    'class' => 'tinymceField'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Referentiel::class,
            'translation_domain' => 'referentiel',
        ]);
    }
}
