<?php

namespace App\Form;

use App\Entity\Organisation;
use App\Form\Type\LsFileType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Regex;

class OrganisationType extends AbstractType
{
    public function __construct(private readonly ParameterBagInterface $bag)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('raisonsociale', TextType::class, [
                'label' => 'form.organisation.raisonsociale.label',
                'required' => true
            ])
            ->add('phone', TelType::class, [
                'label' => 'form.organisation.telephone.label',
                'required' => true,
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('fax', TelType::class, [
                'label' => 'form.organisation.fax.label',
                'required' => false,
                'attr' => [
                    'class' => 'maskPhone'
                ],
                'constraints' => [
                    new Regex(
                        '/^0[1|2|3|4|5]([-. ]?[0-9]{2} ){3}([-. ]?[0-9]{2})$/',
                        'Le numéro de téléphone fixe doit être de la forme 01xxxxxxxx ou 02xxxxxxxx ou 03xxxxxxxx ou 04xxxxxxxx ou 05xxxxxxxx'
                    ),
                ]
            ])
            ->add('address', TextareaType::class, [
                'label' => 'form.organisation.adresse.label',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.organisation.email.label',
                'required' => true
            ])
            ->add('sigle', TextType::class, [
                'label' => 'Sigle',
                'required' => false
            ])
            ->add('siret', TextType::class, [
                'label' => '',
                'required' => true,
                'attr' => [
                    'class' => 'maskSiret'
                ]
            ])
            ->add('ape', TextType::class, [
                'label' => '',
                'required' => true,
                'attr' => [
                    'class' => 'maskApe'
                ]
            ])
            ->add('logo', LsFileType::class, [
                'label' => 'Logo de l\'entité',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => $this->bag->get('logo_mime_types_authorized'),
                        'mimeTypesMessage' => "Seuls les fichiers au format '.png', '.jpeg', '.jpg' et '.svg' sont acceptés.",
                        'maxSize' => $this->bag->get('document_max_size'),
                    ])
                ],
                'attr' => [
                    'lang' => 'fr',
                    'accept' => '.png, .PNG, .jpeg, .JPEG, .jpg, .JPG, .svg, .SVG',
                    'placeholder' => 'Selectionnez un fichier'
                ]
            ])
            ->add('registrePrefix', TextType::class, [
                'label' => 'form.organisation.registrePrefix.label',
                'required' => true,
                'data' => 'DPO-'
            ])
            ->add('dirigeant', DirigeantType::class, [
                'label' => false,
            ])
        ;

        if ($options['isEditMode'] === true) {
            $builder
                ->add('dpo', DpoType::class, [
                    'label' => false,
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organisation::class,
            'translation_domain' => 'organisation',
            'isEditMode' => false,
        ]);
    }
}
